#include "ast.hpp"

namespace tiny::cc::ast
{

namespace
{

auto & print_ident_short( brq::string_builder &sb, const ident_t *ident )
{
    assert( ident );
    sb << ident << '[' << ident->name << " : ";
    if ( ident->address_taken )
        sb << "&'ed ";
    return sb << ident->type->name() << ']';
}

}

bool expr::uses_ident( const ident_t *id ) const
{
    if ( ident && ident == id )
        return true;

    for ( auto &sub : subs )
        if ( sub.uses_ident( id ) )
            return true;

    return false;
}

brq::string_builder & expr::print( brq::string_builder &sb, int level ) const
{
    for ( int i = 0; i < level; ++i )
        sb << ". ";

    if ( implicit )
        sb << "implicit ";

    sb << ( kind == identifier ? "identifier"
          : kind == immediate ? "immediate"
          : kind == call ? "call"
          : kind == dot ? "dot"
          : kind == postop ? "postop"
          : kind == uplus ? "uplus"
          : kind == uminus ? "uminus"
          : kind == lnot ? "lnot"
          : kind == bwnot ? "bwnot"
          : kind == cast ? "cast"
          : kind == deref ? "deref"
          : kind == addrof ? "addrof"
          : kind == add ? "add"
          : kind == sub ? "sub"
          : kind == mul ? "mul"
          : kind == div ? "div"
          : kind == mod ? "mod"
          : kind == bwand ? "bwand"
          : kind == bwor ? "bwor"
          : kind == bwxor ? "bwxor"
          : kind == shl ? "shl"
          : kind == shr ? "shr"
          : kind == eq ? "eq"
          : kind == ne ? "ne"
          : kind == lt ? "lt"
          : kind == le ? "le"
          : kind == gt ? "gt"
          : kind == ge ? "ge"
          : kind == land ? "land"
          : kind == lor ? "lor"
          : kind == cond ? "cond"
          : kind == assign ? "assign"
          : kind == comma ? "comma"
          : "UNKNOWN" ) << ' ';

    if ( ident )
        print_ident_short( sb, ident ) << " ";

    if ( kind == immediate || value )
        sb << "(= " << value << ") ";

    sb << ( cat == lvalue ? "lvalue"
          : cat == rvalue ? "rvalue"
          : cat == constant ? "constant"
          : "UNKNOWN" );

    if ( type )
        sb << " of type " << type->name();

    if ( parenthesised )
        sb << " parenthesised";

    sb << '\n';


    for ( auto &sub : subs )
        sub.print( sb, level + 1 );

    return sb;
}

brq::string_builder & stmt::print( brq::string_builder &sb, int level ) const
{
    for ( int i = 0; i < level; ++i )
        sb << ". ";

    sb << ( kind == declaration ? "declaration"
          : kind == expression ? "expression"
          : kind == compound ? "compound"
          : kind == conditional ? "if"
          : kind == for_loop ? "for"
          : kind == while_loop ? "while"
          : kind == do_while_loop ? "do-while"
          : kind == brk ? "break"
          : kind == cont ? "continue"
          : kind == ret ? "return"
          : "UNKNOWN" ) << " stmt";

    if ( decl )
        print_ident_short( sb << ' ', decl );
    if ( should_bzero )
        sb << " bzero'd";

    sb << '\n';

    if ( expr )
        expr->print( sb, level + 1 );
    for ( auto &sub : subs )
        sub.print( sb, level + 1 );

    return sb;
}

}
