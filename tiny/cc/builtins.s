    .section .rodata

    .global tiny_malloc_h_begin
    .type   tiny_malloc_h_begin, @object
    .balign  1
tiny_malloc_h_begin:
    .incbin "cc/malloc.h.builtin"

    .global tiny_malloc_h_end
    .type   tiny_malloc_h_end, @object
tiny_malloc_h_end:

    .global tiny_malloc_c_begin
    .type   tiny_malloc_c_begin, @object
    .balign  1
tiny_malloc_c_begin:
    .incbin "cc/malloc.c.builtin"

    .global tiny_malloc_c_end
    .type   tiny_malloc_c_end, @object
tiny_malloc_c_end:

    .global tiny_malloc_asm_begin
    .type   tiny_malloc_asm_begin, @object
    .balign  1
tiny_malloc_asm_begin:
    .incbin "cc/malloc.asm.builtin"

    .global tiny_malloc_asm_end
    .type   tiny_malloc_asm_end, @object
tiny_malloc_asm_end:
