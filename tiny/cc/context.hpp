#pragma once

#include <deque>

#include "ast.hpp"
#include "type_manager.hpp"
#include "scoped.hpp"
#include "ident.hpp"

namespace tiny::cc
{

struct function
{
    ident_t *ident;
    document::span definition_site;
    std::deque< ident_t > locals;
    std::vector< ident_t * > args;
    ast::stmt body;

    bool has_return = false;

    void dump_ast() const;
};

struct context
{
    type_manager types;
    std::map< std::string, function, std::less<> > funcs;
    std::deque< ident_t > globals;

    scoped_view< ident_t > idents;
    scoped_view< ident_t >::scope_guard _toplevel;
    std::vector< std::string > funcs_ordered;

    context();
    context( const context & ) = delete;
    context( context && ) = delete;

    void dump_types() const;
    void dump_ast() const;
};

}
