#include <fstream>
#include <iostream>
#include <brick-cmd>
#include <brick-trace>

#include "diagnostic.hpp"
#include "common/document_store.hpp"
#include "lexer.hpp"
#include "parser.hpp"
#include "codegen.hpp"
#include "builtins.hpp"
#include "as/parser.hpp"
#include "vm/runner.hpp"
#include "common/cli.hpp"

namespace tiny::cc
{

struct command : brq::cmd_base
{
    std::vector< cmd_file > _files;
    std::string _out_file;
    brq::cmd_flag _dump_types,
                  _dump_ast,
                  _debug,
                  _run_vm,
                  _compile_only,
                  _source_in_asm = true,
                  _locations_in_asm = false,
                  _allocations_in_asm,
                  _triggers_in_asm,
                  _no_prolog,
                  _builtin_malloc,
                  _stdin_as;
    unsigned _week = 13;
    parser_config pconf;
    codegen_config cgconf;
    vm::runner_config runconf;

    void options( brq::cmd_options &c ) override
    {
        brq::cmd_base::options( c );
        c.opt( "-o", _out_file )
            << "name of the output file";
        c.opt( "-S", _compile_only )
            << "produce the assembly, not the machine code";
        c.flag( "--source-in-asm", _source_in_asm )
            << "with -S, print source code of each statement (default: yes)";
        c.flag( "--locations-in-asm", _locations_in_asm )
            << "with -S, print .loc directives";
        c.flag( "--allocations-in-asm", _allocations_in_asm )
            << "with -S, print register and stash allocations";
        c.flag( "--triggers-in-asm", _triggers_in_asm )
            << "with -S, produce .trigger directives";
        c.opt( "--no-prolog", _no_prolog )
            << "do not emit the initial call to 'main'";
        c.opt( "--stdin-assembler", _stdin_as )
            << "treat standard input as .asm instead of .c";

        c.section( "Language features" );
        c.opt ( "--week", _week )
            << "limit syntax specifically for the PB111 course";
        c.opt( "--malloc", _builtin_malloc )
            << "provide built-in memory functions (malloc, free, realloc)";

        c.collect( _files );

        c.section( "VM options" );
        c.opt( "--run", _run_vm )
            << "run the result in the VM";
        runconf.options( c );

        c.section( "Debugging" );
        c.opt( "--debug", _debug );
        c.opt( "--dump-types", _dump_types );
        c.opt( "--dump-ast", _dump_ast );
        c.opt( "--trace-internals", cgconf.trace_internals )
            << "include traces from built-in functions";
    }

    std::string_view help() override
    {
        return "‹tiny cc› is a compiler of a subset of the C99 language";
    }

    void run() override
    {
        exit( run_main() );
    }

    int run_main()
    {
        if ( _debug )
            brq::trace().add_rule( "+", "debug" );

        if ( _files.empty() )
            _files.push_back( { "-" } );

        if ( _run_vm && _compile_only )
            throw brq::error( "--run and -S are mutually exclusive" );

        pconf.has_calls    = _week >= 3;
        pconf.has_pointers = _week >= 4;
        pconf.has_structs  = _week >= 5;
        pconf.has_arrays   = _week >= 4;

        tiny::document_store< cc::document > docstore;

        as::assembly assm;
        context ctx;

        try
        {
            if ( !_no_prolog )
                docstore.add_document( "<prolog>", _builtin_malloc ? builtins::prolog_heap
                                                                   : builtins::prolog_plain )
                        .type = document::file_type::assembler;

            if ( _builtin_malloc )
                docstore.add_document( "malloc.h", builtins::malloc_h )
                        .is_internal = true;

            for ( auto &f : _files )
            {
                auto &doc = docstore.load_from_file( f.name );
            }

            if ( _builtin_malloc )
            {
                docstore.add_document( "malloc.asm", builtins::malloc_asm )
                        .is_internal = true;

                docstore.add_document( "malloc.c", builtins::malloc_c )
                        .is_internal = true;
            }

            bool first_week_c_warned = false;

            for ( auto &docptr : docstore.docs )
            {
                auto &doc = *docptr;

                if ( doc.is_stdin() )
                    doc.type = _stdin_as ? document::file_type::assembler
                                         : document::file_type::c;
                else
                    doc.deduce_type();

                if ( doc.type == document::file_type::c )
                {
                    if ( _week < 2 && !first_week_c_warned && !doc.is_internal )
                    {
                        source_diagnostic w( doc.ref_file(),
                                "there are no C programs in the first week" );
                        print_warning( w );

                        first_week_c_warned = true;
                    }

                    parser_config real_pconf = doc.is_internal
                                             ? parser_config{ .is_internal = true }
                                             : pconf;

                    auto toks = lex( doc );
                    parse( ctx, doc, toks, real_pconf );
                }
                else if ( doc.type == document::file_type::assembler )
                {
                    assm.add_document( doc );
                }
                else
                    throw std::runtime_error( "unable to deduce type of file " + doc.filename );
            }

            if ( _dump_types )
            {
                ctx.dump_types();
                _dump_types = false;
            }
            if ( _dump_ast )
            {
                ctx.dump_ast();
                _dump_ast = false;
            }

            generate( ctx, assm, cgconf );

            if ( _compile_only )
            {
                as::filter_t to_print{
                    .triggers    = _triggers_in_asm,
                    .locations   = _locations_in_asm,
                    .allocations = _allocations_in_asm,
                    .src_lines   = _source_in_asm,
                };

                auto str = assm.print( to_print );

                if ( _out_file.empty() || _out_file == "-" )
                    std::cout << str;
                else
                {
                    std::ofstream out{ _out_file };
                    out << str;
                }

                return 0;
            }

            auto prog = assm.assemble();

            if ( _run_vm )
            {
                vm::runner r( prog, runconf );
                r.run();

                brq::string_builder sb;
                std::cout << r.print_final( sb ).data();

                exit( r.hs != vm::state::halted );
            }

            if ( _out_file.empty() )
                _out_file = "tiny.out";

            std::ofstream outf;
            std::ostream &out = _out_file == "-" ? std::cout
                                                 : ( outf.open( _out_file ), outf );

            prog.write_hex( out );
            out << "; triggers follow\n";
            prog.write_triggers( out );

            return 0;
        }
        catch ( diagnostic &e )
        {
            print_error( e );
        }

        // In case of an exception in parsing
        if ( _dump_types )
            ctx.dump_types();
        if ( _dump_ast )
            ctx.dump_ast();

        return 1;
    }
};

std::string cmd_name( command & ) { return ""; }

}

int main( int argc, char **argv )
{
    return tiny::parse_and_run_single< tiny::cc::command >( argc, argv );
}
