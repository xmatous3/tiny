#pragma once

#include <common/assert.hpp>
#include <cstdint>
#include <string>
#include <string_view>

namespace tiny::cc
{

struct token
{
    // All tokens are assumed to come from the same string and ‹lit› therefore
    // also represents the token's position in the source code
    std::string_view lit;
    enum class category : unsigned char
    {
        unknown = 0,
        keyword,
        punct,
        ident,
        number,
    } cat;
    bool lit_unsigned = false;
    uint16_t val = 0;


    bool is( std::string_view sym ) const { return lit == sym; }
    bool is( category c ) const { return cat == c; }

    explicit operator bool() const { return cat != category::unknown; }
};

inline constexpr std::string_view to_string( token::category c )
{
    using cat = token::category;
    switch ( c )
    {
        case cat::unknown: return "unknown token";
        case cat::keyword: return "keyword";
        case cat::punct: return "punctuation";
        case cat::ident: return "identifier";
        case cat::number: return "number";
    }
    UNREACHABLE( "invalid token category" );
}

}
