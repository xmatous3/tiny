#pragma once

#include <string_view>

extern const char tiny_malloc_h_begin[],
                  tiny_malloc_h_end[],
                  tiny_malloc_c_begin[],
                  tiny_malloc_c_end[],
                  tiny_malloc_asm_begin[],
                  tiny_malloc_asm_end[];

namespace tiny::cc::builtins
{

    inline std::string_view malloc_h( tiny_malloc_h_begin,
                                      tiny_malloc_h_end - tiny_malloc_h_begin );

    inline std::string_view malloc_c( tiny_malloc_c_begin,
                                      tiny_malloc_c_end - tiny_malloc_c_begin );

    inline std::string_view malloc_asm( tiny_malloc_asm_begin,
                                        tiny_malloc_asm_end - tiny_malloc_asm_begin );

    inline std::string_view prolog_heap  = "call __init_heap\ncall main\nhalt\n";
    inline std::string_view prolog_plain = "call main\nhalt\n";
}
