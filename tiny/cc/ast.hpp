#pragma once

#include <vector>
#include <brick-min>

#include "type.hpp"
#include "ident.hpp"

namespace tiny::cc::ast
{

struct no_copy
{
    no_copy() = default;
    no_copy( const no_copy & ) = delete;
    no_copy( no_copy && ) noexcept = default;
    no_copy& operator=( no_copy && ) noexcept = default;
};

struct expr : no_copy
{
    /* Were the copy constructor deleted here, the struct would no longer be
     * an aggregate initialisable with designated initialisers. C++ <3. */
    enum kind_t
    {
        TODO = 0,
        identifier, immediate,
        call,
        dot, postop,
        uplus, uminus, lnot, bwnot,
        cast, deref, addrof, /* align, */
        add, sub, mul, div, mod,
        bwand, bwor, bwxor, shl, shr,
        eq, ne, lt, le, gt, ge,
        cond, land, lor,
        assign,
        comma,
    } kind;

    enum category_t
    {
        lvalue,
        rvalue,
        constant,
    } cat = rvalue;

    const cc::type *type;

    std::string_view lit;

    std::vector< expr > subs;

    ident_t *ident = nullptr;
    int32_t value = 0; /* also used to hold the kind of compound assignmnt and struct member offset */

    bool implicit = false;
    bool parenthesised = false;

    bool is_lvalue() const { return cat == lvalue; }
    bool is_constant() const { return cat == constant; }

    const expr & operator[]( unsigned i ) const
    {
        assert( i < subs.size() );
        return subs[ i ];
    }
    const expr & subexpr() const { assert( subs.size() == 1 ); return operator[]( 0 ); };
    const expr & left() const { return operator[]( 0 ); };
    const expr & right() const { return operator[]( 1 ); };

    bool uses_ident( const ident_t * ) const;
    const expr * try_decayed_array() const
    {
        if ( kind == expr::cast && implicit
                && type->is_pointer() && subexpr().type->is_array()
                && subexpr().type->remove_array() == type->remove_pointer() )
            return &subexpr();
        return nullptr;
    }

    brq::string_builder & print( brq::string_builder &sb, int level ) const;

    friend brq::string_builder & operator<<( brq::string_builder &sb, const expr &e )
    {
        return e.print( sb, 0 );
    }
};

struct stmt : no_copy
{
    enum kind_t
    {
        declaration,
        expression,
        compound,
        conditional,
        for_loop,
        while_loop,
        do_while_loop,
        brk, cont, ret,
        TODO,
    } kind = expression;

    ident_t *decl = nullptr;
    document::span lit;
    std::vector< stmt > subs;
    std::optional< ast::expr > expr;

    bool should_bzero = false; /* declarations with incomplete aggregate initializer */

    brq::string_builder & print( brq::string_builder &sb, int level ) const;

    friend brq::string_builder & operator<<( brq::string_builder &sb, const stmt &s )
    {
        return s.print( sb, 0 );
    }
};

}
