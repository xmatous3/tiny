#include <optional>
#include <vector>
#include <brick-string>

#include "common/limits.hpp"
#include "diagnostic.hpp"
#include "parser.hpp"
#include "ident.hpp"

namespace tiny::cc
{

enum class conversion
{
    implicit,
    cast,
    assignment,
};

namespace
{

using std::literals::operator""sv;

std::string_view litspan( const auto &from, const auto &to )
{
    return std::string_view( from.lit.begin(), to.lit.end() - from.lit.begin() );
}

template< typename T, typename... Ts >
auto movesubs( T && arg, Ts &&... args )
{
    std::vector< std::remove_reference_t< T > > v;
    v.reserve( 1 + sizeof...( args ) );
    v.emplace_back( std::move( arg ) );
    ( v.emplace_back( std::move( args ) ), ... );
    return v;
}

using std::literals::operator""s;

ast::expr::kind_t op2kind( std::string_view op )
{
    using ast::expr;
    return op == "+" ? expr::add
         : op == "[" ? expr::add
         : op == "-" ? expr::sub
         : op == "*" ? expr::mul
         : op == "/" ? expr::div
         : op == "%" ? expr::mod
         : op == "&" ? expr::bwand
         : op == "|" ? expr::bwor
         : op == "^" ? expr::bwxor
         : op == "<<" ? expr::shl
         : op == ">>" ? expr::shr
         : op == "==" ? expr::eq
         : op == "!=" ? expr::ne
         : op == "<" ? expr::lt
         : op == "<=" ? expr::le
         : op == ">" ? expr::gt
         : op == ">=" ? expr::ge
         : op == "&&" ? expr::land
         : op == "||" ? expr::lor
         : throw std::runtime_error( "unknown binary operator" );
}

template< typename T >
int32_t value_of( ast::expr::kind_t kind, T l, T r )
{
    using ast::expr;

    if ( r == 0 && ( kind == expr::div || kind == expr::mod ) )
        throw std::logic_error( "division by zero in constant expression" );

    int32_t v;

    switch ( kind )
    {
        case expr::add:   v = l + r; break;
        case expr::sub:   v = l - r; break;
        case expr::mul:   v = l * r; break;
        case expr::div:   v = l / r; break;
        case expr::mod:   v = l % r; break;
        case expr::bwand: v = l & r; break;
        case expr::bwor:  v = l | r; break;
        case expr::bwxor: v = l ^ r; break;
        case expr::shl:   v = l << r; break;
        case expr::shr:   v = l >> r; break;
        case expr::eq:    v = l == r; break;
        case expr::ne:    v = l != r; break;
        case expr::lt:    v = l < r; break;
        case expr::le:    v = l <= r; break;
        case expr::gt:    v = l > r; break;
        case expr::ge:    v = l >= r; break;
        case expr::land:  v = l && r; break;
        case expr::lor:   v = l || r; break;
        case expr::comma: v = r; break;
        default: UNREACHABLE( "invalid binary operation" );
    }

    return v;
}

template < typename T >
int32_t value_of( ast::expr::kind_t kind, T l )
{
    using ast::expr;

    int32_t v;

    switch ( kind )
    {
        case expr::cast:
        case expr::uplus:  v =  l; break;
        case expr::uminus: v = -l; break;
        case expr::lnot:   v = !l; break;
        case expr::bwnot:  v = ~l; break;
        default: UNREACHABLE( "invalid unary operation" );
    }

    return v;
}

int32_t value_of( ast::expr::kind_t kind, int32_t l, int32_t r, bool sgn )
{
    return sgn ? value_of< int32_t >( kind, l, r )
               : value_of< uint32_t >( kind, l, r );
}

int32_t value_of( ast::expr::kind_t kind, int32_t l, bool sgn )
{
    return sgn ? value_of< int32_t >( kind, l )
               : value_of< uint32_t >( kind, l );
}

auto binop = []( auto &&l, const token &op, auto &&r, auto &p )
{
    using ast::expr;
    auto kind = op2kind( op.lit );
    auto ltype = l.type->remove_const();
    auto rtype = r.type->remove_const();

    bool is_logical = kind == expr::land || kind == expr::lor;
    bool is_cmp = kind == expr::eq || kind == expr::ne
               || kind == expr::lt || kind == expr::le
               || kind == expr::gt || kind == expr::ge;
    bool is_shift = kind == expr::shl || kind == expr::shr;
    bool is_ptrarith = false; // set from within a lambda later

    /* pointer-pointer operations: subtraction and comparison */
    if ( !is_logical && ltype->is_pointer() && rtype->is_pointer() )
    {
        auto *lptype = ltype->remove_pointer()->remove_const(),
             *rptype = rtype->remove_pointer()->remove_const();

        if ( kind == expr::sub )
        {
            if ( lptype != rptype )
                 throw p.mkerror( op.lit, "subtracting pointers of incompatible types ",
                                  l.type->name(), " and ", r.type->name() );

            if ( lptype->is_void() )
                 throw p.mkerror( op.lit, "subtracting pointers to void" );

            // Scale the difference by size of the pointed-to type
            l = p.fold_constants_binary( expr{
                .kind = expr::sub,
                .type = p.ctx.types.get_int(),
                .lit  = litspan( l, r ),
                .subs = movesubs( l, r ),
            } );

            return p.add_scale( std::move( l ), ltype, expr::div );
        }
        else if ( is_cmp && lptype != rptype && !lptype->is_void() && !rptype->is_void() )
            print_warning( p.mkerror( op.lit, "comparing pointers of incompatible types ",
                        l.type->name(), " and ", r.type->name() ) );
        else if ( !is_cmp )
            throw p.mkerror( op.lit, "cannot use operator ", op.lit, " with two pointers" );
    }

    auto scale_or_throw = [&]( ast::expr &&ae, const type *pt, bool ptr_right = false )
    {
        if ( kind == expr::eq || kind == expr::ne )
        {
            if ( !ae.is_constant() || ae.value != 0 )
                throw p.mkerror( op.lit,
                        "comparing a pointer with a number is allowed only with literal zero" );

            return p.add_cast( std::move( ae ), pt, conversion::cast );
        }

        if ( pt->remove_pointer()->is_void() )
            throw p.mkerror( op.lit, "arithmetic on pointers to void" );

        if ( kind != expr::add && kind != expr::sub )
            throw p.mkerror( op.lit, "cannot use operator ", op.lit, " with a pointer" );

        if ( ptr_right && kind == expr::sub )
            throw p.mkerror( op.lit, "cannot subtract a pointer from a number" );

        is_ptrarith = true;

        return p.add_scale( std::move( ae ), pt );
    };

    /* pointer-number operations: addition, subtraction, comparison with zero */
    if ( !is_logical && ltype->is_arithmetic() && rtype->is_pointer() )
        l = scale_or_throw( std::move( l ), rtype, /* ptr right */ true );
    if ( !is_logical && ltype->is_pointer() && rtype->is_arithmetic() )
        r = scale_or_throw( std::move( r ), ltype );

    if ( !ltype->is_scalar() || !rtype->is_scalar() )
    {
        throw p.mkerror( litspan( l, r ),
                "incompatible operand types for operator ", op.lit )
            .append_note( p.doc.ref( l.lit ), "left operand has type "s, l.type->name() )
            .append_note( p.doc.ref( r.lit ), "right operand has type "s, r.type->name() );
    }

    // Bitshifts promote each operand separately, but we want them to be equal,
    // so we just mark the left promoted type as the common one, because the
    // signedness of the right one matters only with very large or negative
    // shift amounts, which is UB anyway.
    auto common_type = is_shift   ? p.ctx.types.promoted( l.type )
                     : is_logical ? p.ctx.types.common_boollike_type( ltype, rtype )
                                  : p.ctx.types.common_arithmetic_type( ltype, rtype );
    auto return_type = is_cmp ? p.ctx.types.get_bool()
                              : common_type;

    if ( !is_ptrarith )
    {
        l = p.add_cast( std::move( l ), common_type );
        r = p.add_cast( std::move( r ), common_type );
    }

    // warn that comparison has higher priority than bitwise operators
    if ( kind == expr::bwand || kind == expr::bwor || kind == expr::bwxor )
    {
        p.check_warn_comparison_in_bitwise( l );
        p.check_warn_comparison_in_bitwise( r );
    }

    // Detect UB in shifts
    if ( is_shift )
        p.check_overshift( r );
    if ( kind == expr::shl && common_type->is_signed() && l.is_constant() && l.value < 0 )
        throw p.mkerror( l.lit, "left-shifting a negative value is undefined behaviour" );

    return p.fold_constants_binary( expr{
        .kind = kind,
        .type = return_type,
        .lit = litspan( l, r ),
        .subs = movesubs( l, r ),
    } );
};

struct parser
{
    using token_stream = std::vector< token >;
    using token_iterator = token_stream::const_iterator;
    context &ctx;
    const document &doc;
    parser_config conf;
    parsing_stacktrace st;

    struct tried_t
    {
        std::string_view what;
        bool squeeze = false; // horrible hack for compact diagnostics in expressions
        bool quoted = false;
    };
    std::vector< tried_t > tried_to_parse;
    bool report_tried = true;

    token_iterator begin;
    token_iterator it;
    token_iterator end;

    function *current_function = nullptr;
    int loop_counter = 0;

    static constexpr int MAX_ARGS = 7; // FIXME so that this is not needed
    static constexpr std::string_view msg_prohibited = " prohibited by the --week option";

    using cat = token::category;
    using expr = ast::expr;
    using stmt = ast::stmt;

    parser( context &ctx, const document &doc, const token_stream &ts, parser_config conf )
        : ctx( ctx ), doc( doc ), conf( conf ),
          begin( ts.begin() ), it( ts.begin() ), end( ts.end() )
    {}

    using parse_error = parse_diagnostic;

    parse_error mkerror( document::span where, auto... what )
    {
        auto err = parse_error( where, what... );
        if ( current_function && current_function->ident )
            err.in_function = current_function->ident->name;
        err.stacktrace( std::move( st ) );
        return err;
    }
    parse_error mkerror( std::string_view sv, auto... what )
    {
        return mkerror( doc.ref( sv ), what... );
    }
    parse_error mkerror( const token &t, auto... what )
    {
        return mkerror( t.lit, what... );
    }
    parse_error mkerror_next( auto... what )
    {
        auto sv = empty() ? std::string_view{ ( end - 1 )->lit.end() - 1, 1 }
                          : it->lit;
        return mkerror( sv, what... );
    }
    parse_error mkerror_unexpected()
    {
        auto sb = brq::format( "expected " );
        for ( auto &i : tried_to_parse )
        {
            if ( &i != &tried_to_parse.front() )
            {
                sb << ( &i == &tried_to_parse.back() ? " or " : ", " );
            }
            auto [ sv, squeeze, quoted ] = i;
            if ( quoted ) sb << "‘";
            sb << sv;
            if ( quoted ) sb << "’";
        }
        sb << " here";
        return mkerror_next( sb.data() );
    }

    bool empty() const { return it == end; }
    int remaining_size() const { return end - it; }
    const token& lookahead( int offset = 0 ) const { return *( it + offset ); }

    std::optional< token > try_shift( cat c, std::string_view desc = "" )
    {
        auto g = trying( desc.empty() ? to_string( c ) : desc );
        if ( !empty() && it->cat == c )
        {
            token t = *it++;
            tried_to_parse.clear();
            report_tried = true;
            return t;
        }
        return {};
    }

    std::optional< token > try_shift( std::string_view sv, std::string_view desc = "" )
    {
        auto g = trying( desc.empty() ? sv : desc, /* squeeze */ false, /* quoted */ true );
        if ( !empty() && it->lit == sv )
        {
            token t = *it++;
            assert( t.cat == cat::keyword || t.cat == cat::punct );
            tried_to_parse.clear();
            report_tried = true;
            return t;
        }
        return {};
    }
    std::optional< token > try_shift_one_of( auto t, auto... ts )
    {
        if ( auto tok = try_shift( t ) )
            return tok;
        if constexpr ( sizeof...( ts ) == 0 )
            return {};
        else
            return try_shift_one_of( ts... );
    }
    std::optional< token > try_shift_one_of( tried_t t, auto... ts )
    {
        auto g = trying( t );
        return try_shift_one_of( ts... );
    }

    auto shift( auto what )
    {
        if ( auto t = try_shift( what ) )
            return *t;
        throw mkerror_unexpected();
    }


    template< typename... Args >
    auto must_if( bool must, auto (parser::*fn)(Args ...), const char *msg = nullptr, auto... args )
        -> decltype( (this->*fn)( args... ) )
    {
        auto g = trying( msg ? msg : "" );
        auto res = (this->*fn)( args... );
        if ( res )
            return res;
        if ( must )
            throw mkerror_unexpected();
        return {};
    }
    template< typename... Args >
    auto must( auto (parser::*fn)( Args... ), const char *msg = nullptr, auto... args )
        -> decltype( (this->*fn)( args... ) )::value_type
    {
        auto res = *must_if( true, fn, msg, args... );
        return res;
    }
    template< typename... Args >
    auto must( auto* (parser::*fn)( Args... ), const char *msg = nullptr, auto... args )
        -> decltype( (this->*fn)( args... ) )
    {
        return must_if( true, fn, msg, args... );
    }
    template< typename... Args >
    void must( bool (parser::*fn)( Args... ), const char *msg = nullptr, auto... args )
    {
        must_if( true, fn, msg, args... );
    }

    auto infixl( auto (parser::*prec)(), auto &&f, auto... operators )
    {
        auto e = (this->*prec)();
        if ( e )
        {
            while ( auto op = try_shift_one_of( { .what = "operator (up to logical or)",
                                                  .squeeze = true },
                                                operators... ) )
            {
                auto e2 = must( prec );
                e = f( std::move( *e ), *op, std::move( e2 ), *this );
            }
        }
        return e;
    }


    const type* simple_type()
    {
        if ( try_shift( "unsigned" ) )
        {
            if ( try_shift( "char" ) )
                return ctx.types.get( "unsigned char" );
            try_shift( "int" );
            return ctx.types.get( "unsigned int" );
        }
        if ( try_shift( "signed" ) )
        {
            if ( try_shift( "char" ) )
                return ctx.types.get( "signed char" );
            try_shift( "int" );
            return ctx.types.get( "int" );
        }
        if ( auto t = try_shift_one_of( "int", "bool", "char", "void" ) )
        {
            return ctx.types.get( t->lit );
        }
        if ( try_shift( "struct" ) )
        {
            auto ident = shift( cat::ident );
            auto name = ident.lit;
            auto t = ctx.types.find_struct( name );
            if ( t )
                return t;
            throw mkerror( ident, "no such struct: "s + std::string( name ) );
        }
        return nullptr;
    }
    const type* qualified_type()
    {
        bool is_const = try_shift( "const" ).has_value();
        auto t = must_if( is_const, &parser::simple_type, "simple type name" );
        if ( t )
        {
            if ( !is_const )
                is_const = try_shift( "const" ).has_value();
            if ( is_const )
                t = ctx.types.add_const( t );
        }
        return t;
    }
    const type* pointer( const type *t )
    {
        while ( auto star = try_shift( "*" ) )
        {
            if ( !conf.has_pointers )
                throw mkerror( *star, "pointers", msg_prohibited );

            t = ctx.types.add_pointer( t );
            if ( try_shift( "const" ) )
                t = ctx.types.add_const( t );
        }
        return t;
    }
    const type* array( const type *t, bool allow_unknown = false )
    {
        if ( auto bracket = try_shift( "[" ) )
        {
            if ( !conf.has_arrays )
                throw mkerror( bracket->lit, "arrays", msg_prohibited );

            if ( !t->is_complete() )
                throw mkerror( bracket->lit, "array of incomplete type '",
                               t->name(), "'" );

            auto ic = must_if( !allow_unknown, &parser::constant_integer_expression,
                               "array length" );
            shift( "]" );

            if ( ic && ic->value <= 0 )
                throw mkerror( ic->lit, "array length must be positive" );

            // no multi-dimensional arrays
            t = ctx.types.array_of( t, ic ? ic->value : 0 );
        }
        return t;
    }
    const type * type_name()
    {
        auto g = start( "type name" );
        auto g_ = trying( "type name" );
        auto t = qualified_type();
        if ( t )
        {
            t = pointer( t );
        }
        return t;
    }

    std::optional< std::tuple< const type *, std::string_view > >
        cast_like( const char *what )
    {
        // we assume that any keyword following '(' is a part of type, except
        // for 'sizeof' and 'alignof'
        if ( remaining_size() < 2 || it->lit != "(" || lookahead( 1 ).cat != cat::keyword
                || lookahead( 1 ).lit == "sizeof" || lookahead( 1 ).lit == "alignof" )
            return {};
        auto g = start( what );
        auto lparen = shift( "(" );
        auto t = must( &parser::type_name );
        auto rparen = shift( ")" );
        return std::tuple{ t, litspan( lparen, rparen ) };
    }

    std::optional< token > identifier()
    {
        auto tok = try_shift( cat::ident, "identifier" );

        if ( tok && !conf.is_internal && is_reserved_identifier( tok->lit ) )
            throw mkerror( *tok, "identifier ‘", tok->lit, "’ is reserved" );

        return tok;
    }

    std::optional< token > integer_constant() { return try_shift( cat::number, "number" ); }

    std::optional< expr > primary_expression()
    {
        if ( auto i = identifier() )
        {
            auto *ident = ctx.idents.find( i->lit );
            if ( !ident )
            {
                auto e = mkerror( *i, "identifier ", std::string{ i->lit }, " has not been declared" );

                if ( std::set{ "malloc"sv, "free"sv, "realloc"sv }.contains( i->lit ) )
                    e.append_note( doc.ref_file(),
                            "built-in memory functions are available with --malloc" );

                throw e;
            }

            return expr{ .kind  = ident->enum_value ? expr::immediate : expr::identifier,
                         .cat   = ident->type->is_function() || ident->enum_value
                                ? expr::constant : expr::lvalue,
                         .type  = ident->type,
                         .lit   = i->lit,
                         .ident = ident,
                         .value = ident->enum_value.value_or( 0 ),
            };
        }
        if ( auto c = integer_constant() )
        {
            auto *t = c->lit == "NULL" ? ctx.types.get_voidptr()
                    : c->lit_unsigned  ? ctx.types.get_unsigned()
                                       : ctx.types.get_int();

            // the check for whether it fits into u16 is done by the lexer and
            // c->val is already u16
            if ( t->is_signed() && !within_i16( c->val )  )
                throw mkerror( c->lit, "value cannot be represented by 'int'" )
                    .append_note( doc.ref_zerowidth( c->lit ),
                            "append 'u' to mark the literal as unsigned" );

            return expr{ .kind  = expr::immediate,
                         .cat   = expr::constant,
                         .type  = t,
                         .lit   = c->lit,
                         .value = c->val };
        }
        if ( auto paren_open = try_shift( "(" ) )
        {
            auto e = must( &parser::expression );
            auto paren_close = shift( ")" );
            e.parenthesised = true;
            e.lit = litspan( *paren_open, paren_close );
            return e;
        }
        return {};
    }

    std::optional< expr > postfix_expression()
    {
        auto e = primary_expression();
        if ( e )
            while ( auto &&e2 = postfix_operator( std::move( *e ) ) )
                e = std::move( e2 );
        return e;
    }

    std::optional< expr > postfix_operator( expr && sub )
    {
        auto g = trying( "postfix-operator", /* squeeze */ true );
        // array-to-pointer conversion (decay)
        if ( auto et = sub.type->try_remove_array() )
            sub = add_cast( std::move( sub ), ctx.types.add_pointer( et ) );

        if ( auto op = try_shift_one_of( "++", "--" ) )
            return add_incdec( std::move( sub ), *op, /* is post */ true );

        if ( auto opening_bracket = try_shift( "[" ) )
        {
            if ( !sub.type->is_pointer() )
                throw mkerror( sub.lit, "operand is not a pointer or array" );

            auto ie = must( &parser::expression );

            auto closing_bracket = shift( "]" );
            auto lit = litspan( sub, closing_bracket );

            if ( !ie.type->is_arithmetic() )
                throw mkerror( ie.lit, "index is not of arithmetic type" );

            return expr{ .kind = expr::deref,
                         .cat  = expr::lvalue,
                         .type = sub.type->remove_pointer(),
                         .lit  = lit,
                         .subs = movesubs( binop( std::move( sub ),
                                                  *opening_bracket,
                                                  std::move( ie ), *this ) ),
            };
        }
        if ( try_shift( "(" ) )
        {
            // TODO function pointers and indirect calls?
            auto ft = sub.type->get_if< type::function_type >();
            if ( !ft )
                throw mkerror( sub.lit, "expression of type '", sub.type->name(), "' is not callable" );

            if ( !ft->is_complete() )
                throw mkerror( sub.lit, "cannot call a function with incomplete types" );

            assert( sub.kind == expr::identifier );
            assert( sub.cat == expr::constant );

            std::vector< expr > subs = movesubs( sub );

            {
                auto g = start( "argument list" );

                if ( auto arg = assignment_expression() )
                {
                    subs.push_back( std::move( *arg ) );
                    while ( try_shift( "," ) )
                        subs.push_back( must( &parser::assignment_expression ) );
                }
            }

            auto token_close = shift( ")" );

            auto sub_name = subs[ 0 ].kind == expr::identifier
                          ? subs[ 0 ].lit : "expression";

            if ( ft->argument_types.size() != subs.size() - 1 )
                throw mkerror( litspan( subs[ 0 ], token_close ),
                        "wrong number of arguments for call to ", sub_name,
                        ": expected ", std::to_string( ft->argument_types.size() ),
                       ", but ", std::to_string( subs.size() - 1 ), " provided" );

            if ( ft->argument_types.size() > MAX_ARGS )
                throw mkerror( litspan( subs[ 0 ], token_close ), "call with more than ",
                        std::to_string( MAX_ARGS ), " arguments" );

            // no need to narrow booleans passed to the built-in asrt instruction
            bool is_assert = sub.ident->name == "assert";

            for ( auto i = 1; i < subs.size(); ++i )
                subs[ i ] = add_cast( std::move( subs[ i ] ),
                                      ft->argument_types[ i - 1 ],
                                      is_assert ? conversion::implicit
                                                : conversion::assignment );

            if ( !conf.has_calls && !is_assert &&
                    current_function->ident->name != "main" )
                throw mkerror( litspan( subs[ 0 ], token_close ),
                        "function call", msg_prohibited );

            return expr{
                .kind = expr::call,
                .type = &ft->return_type,
                .lit = litspan( subs[ 0 ], token_close ),
                .subs = std::move( subs ),
            };
        }
        if ( auto op = try_shift( "." ) )
        {
            auto *st = sub.type->remove_const()->get_if< type::struct_type >();
            if ( !st )
                throw mkerror( sub.lit, "left operand is not of struct type, but ",
                               sub.type->name() );

            auto member = must( &parser::identifier, "member name" );

            auto *field = st->field( member.lit );
            if ( !field )
                throw mkerror( sub.lit, "type ", sub.type->name(),
                               " has no field ", member.lit );

            auto *ft = &field->type;
            if ( sub.type->is_const() )
                ft = ctx.types.add_const( ft );

            return expr{
                .kind  = expr::dot,
                .cat   = sub.cat,
                .type  = ft,
                .lit   = litspan( sub, member ),
                .subs  = movesubs( sub ),
                .value = field->offset,
            };
        }
        if ( auto op = try_shift( "->" ) )
        {
            auto *st = sub.type->remove_pointer()->remove_const()->get_if< type::struct_type >();
            if ( !st || !sub.type->is_pointer() )
                throw mkerror( sub.lit, "left operand is not a pointer to struct, but has type ",
                               sub.type->name() );

            auto member = must( &parser::identifier, "member name" );

            auto *field = st->field( member.lit );
            if ( !field )
                throw mkerror( sub.lit, "type ", sub.type->name(),
                               " has no field ", member.lit );

            auto *ft = &field->type;
            if ( sub.type->remove_pointer()->is_const() )
                ft = ctx.types.add_const( ft );


            sub = expr{ .kind = expr::deref,
                        .cat  = expr::lvalue,
                        .type = sub.type->remove_pointer(),
                        .lit  = sub.lit,
                        .subs = movesubs( sub ),
            };
            return expr{
                .kind  = expr::dot,
                .cat   = expr::lvalue,
                .type  = ft,
                .lit   = litspan( sub, member ),
                .subs  = movesubs( sub ),
                .value = field->offset,
            };
        }
        return {};
    }

    std::optional< expr > unary_expression()
    {
        auto g = trying( "unary-expression" );
        if ( auto op = try_shift( "sizeof" ) )
        {
            // only 'sizeof(type)', no 'sizeof var'
            auto [ t, lit ] = must( &parser::cast_like, "type name in parentheses", "sizeof argument" );
            if ( !t->is_complete() )
                throw mkerror( lit, "querying size of incomplete type" );

            return expr{
                .kind  = expr::immediate,
                .cat   = expr::constant,
                .type  = ctx.types.get_unsigned(),
                .lit   = std::string_view( op->lit.begin(), lit.end() - op->lit.begin() ),
                .value = int( t->size() )
            };
        }

        if ( auto t = try_shift_one_of( "++", "--" ) )
        {
            auto e = must( &parser::unary_expression );

            return add_incdec( std::move( e ), *t, /* is post */ false );
        }

        if ( auto t = try_shift_one_of( "+", "-", "!", "~", "&", "*" ) )
        {
            auto op = t->lit;
            auto e = must( &parser::cast_expression );

            if ( op == "&" )
            {
                if ( !conf.has_pointers )
                    throw mkerror( op, "address-of", msg_prohibited );

                if ( !e.is_lvalue() )
                    throw mkerror( e.lit, "operand is not an lvalue" );

                if ( e.kind == expr::identifier )
                    e.ident->address_taken = true;

                return expr{
                    .kind = expr::addrof,
                    .type = ctx.types.add_pointer( e.type ),
                    .lit = litspan( *t, e ),
                    .subs = movesubs( e )
                };
            }

            if ( op == "*" )
            {
                if ( !e.type->is_pointer() )
                    throw mkerror( e.lit, "dereferencing a non-pointer" );

                if ( e.type->remove_pointer()->is_void() )
                    throw mkerror( e.lit, "dereferencing a pointer to void" );

                return expr{
                    .kind = expr::deref,
                    .cat = expr::lvalue,
                    .type = e.type->remove_pointer(),
                    .lit = litspan( *t, e ),
                    .subs = movesubs( e )
                };
            }

            auto kind = op == "+" ? expr::uplus
                      : op == "-" ? expr::uminus
                      : op == "!" ? expr::lnot
                      : expr::bwnot;

            auto promoted_type = kind == expr::lnot ? ctx.types.get_booleoid()
                                                    : ctx.types.promoted( e.type );

            e = add_cast( std::move( e ), promoted_type );

            auto return_type = kind == expr::lnot ? ctx.types.get_bool()
                                                  : promoted_type;
            return fold_constants_unary( expr{
                .kind = kind,
                .type = return_type,
                .lit = litspan( *t, e ),
                .subs = movesubs( e )
            } );
        }

        return postfix_expression();
    }

    std::optional< expr > cast_expression()
    {
        auto g = trying( "cast-expression" );
        if ( auto cast = cast_like( "type cast" ) )
        {
            auto [ t, lit ] = *cast;
            auto e = must( &parser::cast_expression );

            e = add_cast( std::move( e ), t, conversion::cast );
            e.lit = std::string_view( lit.begin(), e.lit.end() - lit.begin() );
            return e;
        }
        return unary_expression();
    }

    std::optional< expr > multiplicative_expression()
    {
        auto g = trying( "multiplicative-expression" );
        return infixl( &parser::cast_expression, binop, "*", "/", "%" );
    }
    std::optional< expr > additive_expression()
    {
        auto g = trying( "additive-expression" );
        return infixl( &parser::multiplicative_expression, binop, "+", "-" );
    }
    std::optional< expr > shift_expression()
    {
        auto g = trying( "shift-expression" );
        return infixl( &parser::additive_expression, binop, "<<", ">>" );
    }
    std::optional< expr > inequality_expression()
    {
        auto g = trying( "inequality-expression" );
        return infixl( &parser::shift_expression, binop, "<=", ">=", "<", ">" );
    }
    std::optional< expr > equality_expression()
    {
        auto g = trying( "equality-expression" );
        return infixl( &parser::inequality_expression, binop, "==", "!=" );
    }
    std::optional< expr > bitand_expression()
    {
        auto g = trying( "bitand-expression" );
        return infixl( &parser::equality_expression, binop, "&" );
    }
    std::optional< expr > bitxor_expression()
    {
        auto g = trying( "bitxor-expression" );
        return infixl( &parser::bitand_expression, binop, "^" );
    }
    std::optional< expr > bitor_expression()
    {
        auto g = trying( "bitor-expression" );
        return infixl( &parser::bitxor_expression, binop, "|" );
    }
    std::optional< expr > and_expression()
    {
        auto g = trying( "and-expression" );
        return infixl( &parser::bitor_expression, binop, "&&" );
    }
    std::optional< expr > or_expression()
    {
        auto g = trying( "or-expression" );
        return infixl( &parser::and_expression, binop, "||" );
    }

    std::optional< expr > constant_integer_expression()
    {
        auto e = or_expression();
        if ( e )
        {
            if ( !e->type->is_arithmetic() )
                throw mkerror( e->lit, "expression must have integer type" );

            if ( !e->is_constant() )
                throw mkerror( e->lit, "expression must be constant" );
        }
        return e;
    }
    std::optional< expr > conditional_expression()
    {
        auto g = trying( "conditional-expression" );
        auto e = or_expression();
        if ( e )
        {
            auto g = trying( "operator (up to ternary)", /* squeeze */ true );
            if ( auto op = try_shift( "?" ) )
            {
                check_is_condition( *e );

                auto et = must( &parser::expression, "then-expression" );
                shift( ":" );
                auto ef = must( &parser::conditional_expression, "else-expression" );

                auto et_type = et.type->remove_const();
                auto ef_type = ef.type->remove_const();

                const type *t = nullptr;

                if ( et_type == ef_type )
                    t = et_type;
                else if ( et_type->is_arithmetic() && ef_type->is_arithmetic() )
                    t = ctx.types.common_arithmetic_type( et_type, ef_type );
                else if ( et_type->is_pointer() && ef_type->is_pointer() )
                {
                    auto voidptr_or_other = [&]( auto &e, auto *other )
                    {
                        if ( e.kind == expr::immediate && e.value == 0 )
                            return other;
                        if ( other->remove_pointer()->is_const() )
                            return ctx.types.get_cvoidptr();
                        return e.type; // can be still const void *
                    };

                    auto etpt = et_type->remove_pointer(),
                         efpt = ef_type->remove_pointer();

                    if ( etpt->is_void() )
                        t = voidptr_or_other( et, ef_type );
                    else if ( efpt->is_void() )
                        t = voidptr_or_other( ef, et_type );
                    else if ( etpt->remove_const() == efpt->remove_const() )
                        t = etpt->is_const() ? et_type : ef_type;
                }

                if ( !t )
                {
                    throw mkerror( litspan( et, ef ),
                            "incompatible operand types for ternary operator" )
                        .append_note( doc.ref( et.lit ), "true operand has type "s, et.type->name() )
                        .append_note( doc.ref( ef.lit ), "false operand has type "s, ef.type->name() );
                }

                et = add_cast( std::move( et ), t );
                ef = add_cast( std::move( ef ), t );

                e = expr{
                    .kind = expr::cond,
                    .type = t,
                    .lit = litspan( *e, ef ),
                    .subs = movesubs( *e, et, ef ),
                };

                auto &cond = *e;
                if ( cond[ 0 ].is_constant() && cond[ 2 - !!cond[ 0 ].value ].is_constant() )
                {
                    cond.cat = expr::constant;
                    cond.value = cond[ 2 - !!cond[ 0 ].value ].value;
                }

            }
        }
        return e;
    }

    std::optional< expr > assignment_expression()
    {
        auto g = trying( "assignment-expression" );
        auto e = conditional_expression();
        if ( e )
        {
            auto g = trying( "operator (up to assignment)", /* squeeze */ true );
            if ( auto op = try_shift_one_of( "=", "+=", "-=", "*=", "/=", "%=",
                                             ">>=", "<<=", "&=", "|=", "^=" ) )
            {
                if ( !e->is_lvalue() )
                    throw mkerror( e->lit, "left-hand side of assignment is not an lvalue" );

                if ( e->type->is_void() )
                    throw mkerror( e->lit, "invalid use of void value" );

                if ( e->type->is_readonly() )
                    throw mkerror( op->lit, "assignment to a read-only lvalue" );

                auto e2 = must( &parser::assignment_expression );

                int compound_kind = 0;
                if ( op->lit.length() != 1 )
                {
                    // compound operation-assignment
                    // we don't just rewrite it to l = l + r, because l may be
                    // a large lvalue expression whose result we'd like to reuse
                    auto subop = op->lit;
                    subop.remove_suffix( 1 );
                    compound_kind = op2kind( subop );
                    assert( compound_kind );
                }

                if ( compound_kind )
                {
                    if ( !e->type->is_arithmetic() && !( e->type->is_pointer()
                                && ( op->lit == "+=" || op->lit == "-=" ) ) )
                        throw mkerror( op->lit, "cannot use compound assignment to type ",
                                       e->type->name() );
                }

                auto *rhs_type = e->type;
                if ( compound_kind )
                {
                    if ( e->type->is_pointer() )
                        rhs_type = ctx.types.get_int();
                    if ( compound_kind == expr::shl || compound_kind == expr::shr )
                    {
                        rhs_type = ctx.types.promoted( e2.type );
                        check_overshift( e2 );
                    }
                }

                e2 = add_cast( std::move( e2 ), rhs_type, conversion::assignment );

                if ( compound_kind && e->type->is_pointer() )
                    e2 = add_scale( std::move( e2 ), e->type );

                return expr{
                    .kind = expr::assign,
                    .type = e->type,
                    .lit  = litspan( *e, e2 ),
                    .subs = movesubs( *e, e2 ),
                    .value = compound_kind,
                };
            }
        }
        return e;
    }

    auto outer_expression()
    {
        auto g = start( "expression" );
        auto scope_guard = ctx.idents.open_scope();
        return expression();
    }
    std::optional< expr > expression()
    {
        auto g_ex = trying( "expression" );
        auto e = assignment_expression();
        if ( !e )
            return e;

        auto g_op = trying( "operator (up to comma)", /* squeeze */ true );
        if ( try_shift( "," ) )
        {
            auto e2 = must( &parser::expression );
            e = fold_constants_binary( expr{
                      .kind = expr::comma,
                      .type = e2.type,
                      .lit = litspan( *e, e2 ),
                      .subs = movesubs( *e, e2 ),
            } );
        }
        return e;
    }

    std::optional< ast::stmt > compound_statement()
    {
        auto brace_open = try_shift( "{" );
        if ( !brace_open )
            return {};

        auto scope_guard = ctx.idents.open_scope();
        ast::stmt s{ .kind = stmt::compound };

        while ( statement( s.subs ) )
            ;

        auto brace_close = shift( "}" );

        s.lit = doc.ref( *brace_open, brace_close );

        return s;
    }
    std::optional< stmt > jump_statement()
    {
        if ( auto t = try_shift( "break" ) )
        {
            if ( loop_counter == 0 )
                throw mkerror( *t, "cannot use break outside loops" );

            return stmt{ .kind = stmt::brk, .lit = doc.ref( *t ) };
        }
        if ( auto t = try_shift( "continue" ) )
        {
            if ( loop_counter == 0 )
                throw mkerror( *t, "cannot use continue outside loops" );

            return stmt{ .kind = stmt::cont, .lit = doc.ref( *t ) };
        }
        if ( auto t = try_shift( "return" ) )
        {
            auto *rt = fun().ident->type->return_type();
            auto e = outer_expression();

            if ( e )
            {
                if ( rt->is_void() )
                    print_warning( mkerror( t->lit, "returning value from a void function" ) );

                e = add_cast( std::move( *e ), rt, conversion::assignment );
            }
            else if ( !rt->is_void() )
                print_warning( mkerror( t->lit, "returning nothing from a non-void function" ) );

            fun().has_return = true;

            auto lit = e ? litspan( *t, *e ) : t->lit;
            return stmt{ .kind = stmt::ret,
                         .lit  = doc.ref( lit ),
                         .expr = std::move( e ),
            };
        }
        return {};
    }
    expr condition()
    {
        shift( "(" );
        auto e = must( &parser::expression, "condition" );
        check_is_condition( e );
        shift( ")" );
        return e;
    }
    std::optional< stmt > conditional_statement()
    {
        if ( !try_shift( "if" ) )
            return {};
        auto s = stmt{ .kind = stmt::conditional, .expr = condition() };
        s.lit = doc.ref( s.expr->lit );

        check_warn_assignment_in_condition( *s.expr );

        s.subs.emplace_back( must( &parser::nondecl_statement, "then-statement" ) );
        if ( try_shift( "else" ) )
            s.subs.emplace_back( must( &parser::nondecl_statement, "else-statement" ) );
        return s;
    }
    std::optional< stmt > while_statement()
    {
        if ( !try_shift( "while" ) )
            return {};

        auto cond = condition();

        check_warn_assignment_in_condition( cond );

        ++ loop_counter;
        auto s = must( &parser::nondecl_statement, "iterated statement" );
        -- loop_counter;

        return stmt{
            .kind = stmt::while_loop,
            .lit  = doc.ref( cond.lit ),
            .subs = movesubs( s ),
            .expr = std::move( cond ),
        };
    }
    std::optional< stmt > do_while_statement()
    {
        if ( !try_shift( "do" ) )
            return {};

        ++ loop_counter;
        auto s = must( &parser::nondecl_statement, "iterated statement" );
        -- loop_counter;

        shift( "while" );

        auto cond = condition();
        check_warn_assignment_in_condition( cond );

        shift( ";" );
        return stmt{
            .kind = stmt::do_while_loop,
            .lit  = doc.ref( cond.lit ),
            .subs = movesubs( s ),
            .expr = std::move( cond ),
        };
    }
    std::optional< stmt > for_statement()
    {
        auto kw = try_shift( "for" );
        if ( !kw )
            return {};
        auto scope_guard = ctx.idents.open_scope();
        shift( "(" );

        stmt outer_stmt{ .kind = stmt::compound };

        if ( auto pre = outer_expression() )
            outer_stmt.subs.push_back( stmt{
                    .kind = stmt::expression,
                    .lit  = doc.ref( pre->lit ),
                    .expr = std::move( pre ),
                    } );
        else
            declaration( outer_stmt.subs );

        shift( ";" );
        auto cond = outer_expression();

        if ( cond )
        {
            check_is_condition( *cond );
            check_warn_assignment_in_condition( *cond );
        }

        shift( ";" );
        auto post = outer_expression();
        shift( ")" );

        ++ loop_counter;
        auto body = must( &parser::nondecl_statement, "iterated statement" );
        -- loop_counter;

        stmt loop_stmt{
            .kind = stmt::for_loop,
            .lit  = doc.ref( kw->lit ),
            .subs = movesubs( body ),
            .expr = std::move( cond ),
        };

        if ( post )
            loop_stmt.subs.push_back( stmt{
                    .kind = stmt::expression,
                    .lit  = doc.ref( post->lit ),
                    .expr = std::move( *post ),
                    } );

        if ( outer_stmt.subs.empty() )
            return loop_stmt;

        outer_stmt.subs.push_back( std::move( loop_stmt ) );
        outer_stmt.lit = doc.ref( litspan( *kw, body ) );
        return outer_stmt;
    }
    std::optional< stmt > loop_statement()
    {
        std::optional< stmt > s;
        ( s = while_statement() )
            || ( s = do_while_statement() )
            || ( s = for_statement() );
        return s;

    }
    std::optional< stmt > nondecl_statement()
    {
        if ( auto tok = try_shift( ";" ) )
            return stmt{ .kind = stmt::expression, .lit = doc.ref( *tok ) };

        std::optional< stmt > s;
        if ( ( s = compound_statement() )
                || ( s = conditional_statement() )
                || ( s = loop_statement() ) )
            return s;

        // push the diagnostic context only for simple statements
        auto g = start( "statement" );

        if ( ( s = jump_statement() ) && shift( ";" ) )
            return s;

        if ( auto e = outer_expression() )
        {
            shift( ";" );
            return stmt{ .kind = stmt::expression,
                         .lit  = doc.ref( e->lit ),
                         .expr = std::move( *e ),
            };
        }

        return {};
    }
    bool statement( auto &parent_stmt )
    {
        std::optional< stmt > s;
        if ( declaration( parent_stmt ) && shift( ";" ) )
            return true;
        if ( enum_declaration() )
            return true;
        if ( auto s = nondecl_statement() )
        {
            parent_stmt.push_back( std::move( *s ) );
            return true;
        };
        return false;
    }
    bool initializer( ident_t *ident, const type *obj_type, int base,
                      std::vector< stmt > &stmts, bool &should_bzero,
                      int *deduce_length = nullptr )
    {
        auto g = start( "initializer" );

        assert( ident->declaration_site );
        auto e_ident = expr{
            .kind  = expr::identifier,
            .cat   = expr::lvalue,
            .type  = ident->type,
            .lit   = ident->declaration_site.sv,
            .ident = ident,
        };

        if ( auto opening_brace = try_shift( "{" ) )
        {
            obj_type = obj_type->remove_const();

            if ( obj_type->is_scalar() )
                throw mkerror( *opening_brace,
                        "initialization of scalar using braces is not supported" );

            auto *at = obj_type->get_if< type::array_type >();
            auto *st = obj_type->get_if< type::struct_type >();
            assert( at || st );

            int aggregate_length = at ? at->length : st->fields.size();

            enum form_t
            {
                undecided,
                sequential,
                designated,
            } form = undecided;

            int index = 0;
            bool sparse = false;

            auto check_designator_mixing = [&]( auto &des )
            {
                form_t want = des ? designated : sequential;

                if ( form == undecided )
                    form = want;
                else if ( form != want )
                    throw des
                        ? mkerror( *des, "mixing designated and sequential initializers is prohibited" )
                        : mkerror_unexpected();
            };

            auto advance_index = [&]( int ix, auto where )
            {
                /* here, index designates the next element to be initialized */
                if ( ix < index )
                    throw mkerror( where, "out-of-order initialization is prohibited" );
                if ( ix != index )
                {
                    sparse = true;
                    index = ix;
                }
            };

            while ( true )
            {
                if ( index >= aggregate_length && !deduce_length )
                    throw mkerror_next( "excess element in initializer" );

                int offset;
                const type *element_type;
                if ( at )
                {
                    auto des = try_shift( "[" );
                    check_designator_mixing( des );
                    if ( des )
                    {
                        auto new_index = must( &parser::constant_integer_expression,
                                               "array index" );

                        shift( "]" );

                        if ( new_index.value < 0 )
                            throw mkerror( new_index.lit, "negative index" );

                        if ( new_index.value >= aggregate_length && !deduce_length )
                            throw mkerror( new_index.lit, "index too large" );

                        advance_index( new_index.value, new_index.lit );

                        shift( "=" );
                    }

                    offset = index * at->of.size();
                    element_type = &at->of;
                }
                else
                {
                    auto des = try_shift( "." );
                    check_designator_mixing( des );
                    if ( des )
                    {
                        auto member = must( &parser::identifier, "member name" );
                        int field_ix = st->field_index( member.lit );

                        if ( field_ix < 0 )
                            throw mkerror( member, "type ", obj_type->name(),
                                           " has no field ", member.lit );

                        advance_index( field_ix, member );

                        shift( "=" );
                    }

                    assert( index < st->fields.size() );
                    const auto &field = st->fields[ index ];
                    offset = field.offset;
                    element_type = &field.type;
                }

                must( &parser::initializer, "initializer", ident, element_type,
                      base + offset, std::ref( stmts ), std::ref( should_bzero ),
                      nullptr );

                ++ index;

                if ( try_shift( "," ) )
                {
                    if ( try_shift( "}" ) )
                        break;
                }
                else
                {
                    shift( "}" );
                    break;
                }
            }

            // TODO: track maximal index
            if ( deduce_length )
               *deduce_length = index;
            // TODO: count initialized members (and handle overwritten fields)
            else if ( index != aggregate_length || sparse )
                should_bzero = true;

            return true;
        }
        if ( auto e = assignment_expression() )
        {
            e = add_cast( std::move( *e ), obj_type, conversion::assignment );

            auto e_dst = expr{
                .kind = expr::dot,
                .cat  = expr::lvalue,
                .type = obj_type,
                .lit  = e->lit,
                .subs = movesubs( e_ident ),
                .value = base,
                .implicit = true,
            };
            auto e_assignment = expr{
                .kind = expr::assign,
                .type = obj_type,
                .lit  = e->lit,
                .subs = movesubs( e_dst, *e ),
                .implicit = true,
            };
            stmts.push_back( stmt{
                    .kind = stmt::expression,
                    .lit  = doc.ref( e_assignment.lit ),
                    .expr = std::move( e_assignment ),
                    } );
            return true;
        }

        return false;
    }

    ident_t* declarator( const type *t )
    {
        auto g = start( "declarator" );
        t = pointer( t );
        auto n = must_if( t->is_pointer(), &parser::identifier, "identifier" );

        if ( !n )
            return {};

        t = array( t, /* allow unknown */ true );
        return add_decl_to_scope( t, n->lit, /* allow incomplete */ true );
    }
    ident_t* init_declarator( const type *t, std::vector< stmt > &stmts )
    {
        auto *ident = must( &parser::declarator, "declarator", t );

        auto decl_ix = stmts.size();
        stmts.push_back( { .kind = stmt::declaration,
                           .decl = ident,
                           .lit  = ident->declaration_site,
                         } );

        auto *at = ident->type->get_if< type::array_type >();
        bool unknown_length = at && at->length == 0;

        if ( !unknown_length ) // complain as soon as possible
            check_type_is_complete( ident );

        int deduce_length = 0;
        bool should_bzero = false;

        if ( ident && try_shift( "=" ) )
                must( &parser::initializer, "initializer", ident, ident->type,
                      0, std::ref( stmts ), std::ref( should_bzero ),
                      unknown_length ? &deduce_length : nullptr );

        if ( should_bzero )
            stmts[ decl_ix ].should_bzero = true;

        if ( unknown_length && deduce_length > 0 )
            ident->type = ctx.types.array_of( &at->of, deduce_length );

        check_type_is_complete( ident );

        return ident;
    }
    bool declaration( auto &parent_stmt )
    {
        auto g = start( "declaration" );
        auto t = qualified_type();
        if ( !t )
            return false;

        do
        {
            must( &parser::init_declarator, "init-declarator", t, std::ref( parent_stmt ) );
        }
        while ( try_shift( "," ) );
        return true;
    }
    bool member_declaration( type::struct_type &s )
    {
        auto g = start( "member declaration" );
        auto t = qualified_type();
        if ( !t )
            return false;
        do
        {
            auto g = start( "member declarator" );
            auto mt = pointer( t );

            auto n = must( &parser::identifier, "member name" );

            mt = array( mt );

            if ( !mt->is_complete() )
                throw mkerror( n, "member has incomplete type ", mt->name() );

            s.add_field( n.lit, *mt );
        }
        while ( try_shift( "," ) );
        shift( ";" );
        return true;
    }
    bool struct_declaration()
    {
        if ( remaining_size() < 3 || it->lit != "struct" || lookahead( 1 ).cat != cat::ident
                || ( lookahead( 2 ).lit != "{" && lookahead( 2 ).lit != ";" ) )
            return false;
        token kw_struct = shift( "struct" );
        auto n = must( &parser::identifier, "struct name");
        std::string name{ n.lit };

        if ( !conf.has_structs )
            throw mkerror( litspan( kw_struct, n ), "structures", msg_prohibited );

        auto &s = ctx.types.add_struct( name );

        if ( try_shift( ";" ) )
        {
            // forward declaration
            return true;
        }

        if ( s.complete )
            throw mkerror( doc.ref( kw_struct, n ), "redefinition of struct "s, name );

        shift( "{" );
        auto scope_guard = ctx.idents.open_scope();
        while ( member_declaration( s ) )
            ;
        shift( "}" );
        shift( ";" );

        s.finish();
        return true;
    }

    bool enum_declaration()
    {
        auto g = start( "enumeration declaration" );
        auto kw = try_shift( "enum" );
        if ( !kw )
            return false;

        shift( "{" );

        int value = 0;
        while ( auto n = must( &parser::identifier, "enumerator name" ) )
        {
            if ( try_shift( "=" ) )
            {
                auto v = must( &parser::constant_integer_expression, "enumerator value" );
                value = v.value;
            }

            auto *ident = add_decl_to_scope( ctx.types.get_int(), n.lit,
                                             current_function ? fun().locals
                                                              : ctx.globals );

            ident->enum_value = value;
            ++ value;

            if ( try_shift( "," ) )
            {
                if ( try_shift( "}" ) )
                    break;
            }
            else
            {
                shift( "}" );
                break;
            }
        }

        shift( ";" );
        return true;
    }

    void check_type_is_complete( ident_t *ident,
                                 std::string_view who = "variable or field" )
    {
        if ( !ident->type->is_complete() )
        {
            assert( ident->declaration_site );
            throw mkerror( ident->declaration_site, who, " ", ident->name,
                           " has incomplete type '", ident->type->name(), "'" );
        }
    }

    ident_t* add_decl_to_scope( const type *t, std::string_view name,
                                bool allow_incomplete = false )
    {
        auto *ident = add_decl_to_scope( t, name, fun().locals, allow_incomplete );
        ident->is_local = true;
        return ident;
    }
    ident_t* add_decl_to_scope( const type *t, std::string_view name, auto &idents,
                                bool allow_incomplete = false )
    {
        auto &ident = idents.emplace_back( name, t, doc.ref( name ) );
        auto add_res = ctx.idents.add( name, &ident );

        if ( !allow_incomplete )
            check_type_is_complete( &ident );

        if ( add_res.is_redeclaration() )
        {
            auto e = mkerror( name, "redeclaration of ", name );
            if ( add_res.has_previous_site() )
                e.append_note( add_res.previous_site(), "previously declared here" );
            throw e;
        }
        if ( add_res.is_shadowing() )
        {
            auto w = mkerror( name, "shadowing earlier declaration of ", name );
            if ( add_res.has_previous_site() )
                w.append_note( add_res.previous_site(), "previously declared here" );
            print_warning( w );
        }

        return add_res.here;
    }

    ident_t* function_argument()
    {
        auto g = trying( "argument declaration" );
        auto t = type_name();
        if ( !t )
            return {};
        auto n = must( &parser::identifier, "argument name" );

        if ( auto opening_bracket = try_shift( "[" ) )
        {
            throw mkerror( *opening_bracket, "array arguments are actually pointers" )
                .append_note( doc.ref_zerowidth( opening_bracket->lit ),
                              "use '", t->name(), " *", n.lit, "' instead" );
        }

        auto *ident = add_decl_to_scope( t, n.lit, /* allow incomplete */ true );
        ident->is_argument = true;

        return ident;
    }
    auto function_argument_list()
    {
        std::vector< ident_t* > args;

        // f( void ) is equivalent to f() in tiny C
        if ( remaining_size() >= 2 && lookahead( 0 ).is( "void" ) && lookahead( 1 ).is( ")" ) )
            return shift( "void" ), args;

        auto arg = function_argument();
        if ( arg )
        {
            args.push_back( arg );
            while ( try_shift( "," ) )
            {
                auto arg = must( &parser::function_argument );
                args.push_back( arg );
            }
        }
        return args;
    }
    bool function_decl_or_def()
    {
        auto rt = type_name();
        if ( !rt )
            return false;

        auto name = must( &parser::identifier );
        shift( "(" );

        if ( name.lit == "assert" )
            throw mkerror( name, "redeclaring the built-in 'assert' function" );

        // Adding the identifier must precede opening the argument list's scope
        auto *ident = &ctx.globals.emplace_back( name.lit, ctx.types.get_void(), doc.ref( name ) );
        auto add_res = ctx.idents.add( name.lit, ident );

        // needs to exist to store arguments' ident_t
        auto fun_ctx = function{ .definition_site = doc.ref( name ) };
        current_function = &fun_ctx;


        auto scope_guard = ctx.idents.open_scope();
        auto args = function_argument_list();
        shift( ")" );

        if ( args.size() > MAX_ARGS )
            throw mkerror( name, "function with more than ",
                    std::to_string( MAX_ARGS ), " arguments" );


        std::vector< const type * > arg_types;
        args.reserve( args.size() );
        for ( auto &arg : args )
            arg_types.push_back( arg->type );
        auto *t = ctx.types.function( rt, arg_types );

        if ( add_res.is_redeclaration() )
        {
            if ( add_res.previous->type != t )
            {
                throw mkerror( name, "redeclaring ", name.lit, " with a different type" )
                    .append_note( add_res.previous_site(), "previously declared with type '"s,
                                  add_res.previous->type->name(), "'" )
                    .append_note( doc.ref( name ), "redeclared with type '"s, t->name(), "'" );
            }

            ctx.globals.pop_back();
            ident = add_res.previous;
        }
        else
            ident->type = t;

        if ( try_shift( ";" ) )
        {
            // forward declaration
            current_function = nullptr;
            return true;
        }

        fun_ctx.args = std::move( args );
        fun_ctx.ident = ident;

        if ( try_shift( "__override" ) )
            ctx.funcs.erase( std::string( name.lit ) );

        auto [ fn, was_new ] = ctx.funcs.emplace( std::string{ name.lit },
                                                  std::move( fun_ctx ) );

        if ( !was_new )
            throw mkerror( name, "redefining function ", name.lit )
                .append_note( fn->second.definition_site, "first defined here" );

        current_function = &fn->second;
        ctx.funcs_ordered.emplace_back( name.lit );

        auto brace_open = shift( "{" );

        check_type_is_complete( ident, "function" );

        fn->second.body = stmt{ .kind = stmt::compound };
        while ( statement( fn->second.body.subs ) )
            ;

        auto brace_close = shift( "}" );

        fn->second.body.lit = doc.ref( brace_open, brace_close );

        if ( !rt->is_void() && !fun().has_return )
            print_warning( mkerror( name,
                        "no return statement in a non-void function" ) );

        current_function = nullptr;

        return true;
    }
    bool toplevel_declaration()
    {
        return struct_declaration()
            || function_decl_or_def()
            || enum_declaration()
            ;
    }
    void translation_unit()
    {
        while ( !empty() )
            must( &parser::toplevel_declaration, "toplevel declaration" );
    }

    void check_warn_assignment_in_condition( const expr &e )
    {
        if ( e.kind == expr::assign && !e.value && !e.parenthesised )
        {
            auto w = mkerror( e.lit, "assignment in condition, did you mean to use '=='?" );
            w.append_note( doc.ref_zerowidth( e.lit ),
                    "add parentheses around the assignment to silence this warning" );
            print_warning( w );
        }
    }

    void check_warn_comparison_in_bitwise( const expr &e )
    {
        if ( e.parenthesised )
            return;

        if ( e.kind == expr::cast && e.implicit )
            return check_warn_comparison_in_bitwise( e.subexpr() );

        if ( e.kind == expr::eq || e.kind == expr::ne || e.kind == expr::lt ||
                e.kind == expr::le || e.kind == expr::gt || e.kind == expr::ge )
        {
            auto w = mkerror( e.lit, "bitwise operation with result of comparison" );
            w.append_note( doc.ref_zerowidth( e.lit ),
                    "add parentheses around the comparison to silence this warning" );
            print_warning( w );
        }
    }

    void check_is_condition( const expr &e )
    {
        if ( !e.type->is_scalar() )
            throw mkerror( e.lit, "condition is not of scalar type" );
    }

    void check_overflow( const expr &e )
    {
        if ( e.type->is_signed() && !within_i16( e.value ) )
            throw mkerror( e.lit, "value cannot be represented by 'int'" )
                .append_note( doc.ref_zerowidth( e.lit ),
                        "expression evaluates to ", e.value );
    }

    void check_overshift( const expr &e )
    {
        if ( e.value < 0 )
            throw mkerror( e.lit, "shifting by negative amount is undefined behaviour" );
        if ( e.value >= 16 )
            throw mkerror( e.lit, "shifting by more than value width is undefined behaviour" );
    }


    struct constituent_guard
    {
        parser &p;
        unsigned guardee_index;
        constituent_guard( const constituent_guard & ) = delete;
        constituent_guard( parser &p, unsigned ix ) : p( p ), guardee_index( ix ) {}
        ~constituent_guard()
        {
            if ( guardee_index >= p.st.open_constituents.size() )
                return;
            auto &con = p.st.open_constituents[ guardee_index ];
            // if something was read, move to closed constituents and flush all
            // closed subconstituents
            if ( p.it != p.end && con.span.begin() < p.it->lit.begin() )
            {
                auto end = p.it > p.begin ? (p.it - 1)->lit.end() : p.begin->lit.begin();
                con.span = std::string_view( con.span.begin(), end - con.span.begin() );
                while ( !p.st.closed_constituents.empty()
                        && p.st.closed_constituents.back().span.begin() >= con.span.begin()
                        && p.st.closed_constituents.back().span.end() <= end
                      )
                    p.st.closed_constituents.pop_back();
                p.st.closed_constituents.emplace_back( std::move( con ) );
            }
            p.st.open_constituents.resize( guardee_index );
        }
    };

    [[nodiscard]]
    constituent_guard start( std::string constituent_name )
    {
        auto lit = it == end ? ( it - 1 )->lit : it->lit;
        auto &con = st.open_constituents.emplace_back( lit, std::move( constituent_name ) );
        // remove past closed constituents
        while ( !st.closed_constituents.empty()
                && st.closed_constituents.back().span.end() <= con.span.begin() )
            st.closed_constituents.pop_back();
        return constituent_guard{ *this, unsigned( st.open_constituents.size() ) - 1 };
    }

    struct trying_guard
    {
        parser *p = nullptr;
        token_iterator pos;
        bool prev;
        trying_guard( const trying_guard & ) = delete;
        trying_guard() = default;
        trying_guard( parser *p ) : p( p ), pos( p->it ), prev( p->report_tried )
        {
            p->report_tried = false;
        }
        ~trying_guard()
        {
            if ( p && pos == p->it ) // nothing succeeded
                p->report_tried = prev;
        }
    };

    [[nodiscard]]
    trying_guard trying( std::string_view what, bool squeeze = false, bool quoted = false )
    {
        return trying( tried_t{ what, squeeze, quoted } );
    }

    [[nodiscard]]
    trying_guard trying( tried_t t )
    {
        if ( t.what.empty() )
            return trying_guard{};
        if ( report_tried )
        {
            if ( t.squeeze && !tried_to_parse.empty() && tried_to_parse.back().squeeze )
                tried_to_parse.pop_back();
            tried_to_parse.emplace_back( std::move( t ) );
        }
        return trying_guard{ this };
    }

    // current function
    function & fun()
    {
        assert( current_function );
        return *current_function;
    }

    ast::expr add_cast( ast::expr &&e, const type *t, conversion kind = conversion::implicit )
    {
        assert( t );

        if ( e.type == t )
            return std::move( e );

        bool is_nullptr = t->is_pointer() && e.kind == expr::immediate
            && e.type->is_arithmetic() && e.value == 0;

        if ( !( ( kind == conversion::assignment && is_nullptr ) ||
                    types_are_convertible( e.type, t, kind != conversion::cast ) ) )
        {
            auto exc = mkerror( e.lit, "cannot convert from type '", e.type->name(),
                                "' to type '", t->name(), "'" );
            if ( kind != conversion::cast && types_are_convertible( e.type, t, false ) )
                exc << " without a cast";

            throw exc;
        }

        return fold_constants_unary( ast::expr{
            .kind = ast::expr::cast,
            .type = t,
            .lit = e.lit,
            .subs = movesubs( e ),
            .implicit = kind != conversion::cast,
        } );
    }

    ast::expr add_scale( ast::expr &&ae, const type *pt, expr::kind_t kind = expr::mul )
    {
        assert( pt->is_pointer() );

        int size = pt->remove_pointer()->size();
        if ( size == 1 )
            return std::move( ae );

        auto scale = expr{
            .kind  = expr::immediate,
            .cat   = expr::constant,
            .type  = ae.type,
            .lit   = ae.lit,
            .value = size,
        };

        return fold_constants_binary( expr{
            .kind = kind,
            .type = ae.type,
            .lit  = ae.lit,
            .subs = movesubs( ae, scale ),
        } );
    }

    ast::expr add_incdec( ast::expr && e, token op, bool is_post )
    {
        if ( !e.is_lvalue() )
            throw mkerror( e.lit, "operand is not an lvalue" );
        if ( !e.type->is_arithmetic() && !e.type->is_pointer() )
            throw mkerror( e.lit, "operand is not of arithemic or pointer type" );
        if ( e.type->is_const() )
            throw mkerror( e.lit, "operand is of const type" );
        if ( e.type->remove_pointer()->is_void() )
            throw mkerror( e.lit, "operand is pointer to void" );

        bool is_inc = op.lit == "++";
        int scale = e.type->is_pointer()
                  ? e.type->remove_pointer()->size()
                  : 1;

        if ( is_post )
            return expr{ .kind = expr::postop,
                         .type = e.type,
                         .lit  = litspan( e, op ),
                         .subs = movesubs( e ),
                         .value = is_inc ? scale : -scale,
            };

        auto one = expr{
            .kind  = expr::immediate,
            .cat   = expr::constant,
            .type  = ctx.types.get_int( e.type->is_signed() ),
            .lit   = op.lit,
            .value = scale,
            .implicit = 1,
        };

        return expr{
            .kind = expr::assign,
            .type = e.type,
            .lit = litspan( op, e ),
            .subs = movesubs( e, one ),
            .value = is_inc ? expr::add : expr::sub,
        };
    }

    ast::expr fold_constants_binary( ast::expr &&e )
    {
        if ( e.left().is_constant() && e.right().is_constant() )
        {
            e.cat = ast::expr::constant;
            e.value = value_of( e.kind, e.left().value, e.right().value,
                                e.right().type->is_signed() );

            check_overflow( e );

            e.value = narrow( e.value, e.type );
        }
        return e;
    }
    ast::expr fold_constants_unary( ast::expr &&e )
    {
        if ( e.subexpr().cat == ast::expr::constant )
        {
            e.cat = ast::expr::constant;
            e.value = value_of( e.kind, e.subexpr().value,
                                e.subexpr().type->is_signed() );

            if ( e.kind == ast::expr::uminus )
                check_overflow( e );

            e.value = narrow( e.value, e.type );
        }
        return e;
    }
    int32_t narrow( int32_t v, const type *&t )
    {
        if ( t->is_void() )
            return 0;
        if ( t->is_boollike() )
            return t = ctx.types.get_bool(), !!v;
        if ( t->is_i8() )
            return int8_t( v ); // sign extension
        if ( t->is_i16() )
            return int16_t( v ); // sign extension

        assert( !t->is_signed() );
        return v & ( t->size() == 2 ? 0xffff : 0xff );
    }
};
}

void parse( context &ctx, const document &doc, const std::vector< token > &ts,
            parser_config conf )
{
    parser p( ctx, doc, ts, conf );

    try
    {
        p.translation_unit();
    }
    catch ( const type_manager::error &e )
    {
        throw p.mkerror_next( "unhandled type error around here: ", e.what() );
    }
    catch ( const std::exception &e )
    {
        throw p.mkerror_next( "an exception was thrown around here\nwhat(): ", e.what() );
    }
}

}
