#pragma once

#include "common/diagnostic.hpp"

namespace tiny::cc
{

using lex_error = tiny::source_diagnostic;

struct constituent_info
{
    std::string_view span;
    std::string name;
};

struct parsing_stacktrace
{
    using cons_vec = std::vector< constituent_info >;

    std::vector< constituent_info > open_constituents;
    std::vector< constituent_info > closed_constituents;
};

struct parse_diagnostic : noted_diagnostic
{
    parsing_stacktrace st;
    std::string_view in_function;

    using noted_diagnostic::noted_diagnostic;

    brq::string_builder &print( brq::string_builder &, kind_t, config_t ) const override;

    auto &stacktrace( parsing_stacktrace s )
    {
        st = std::move( s );
        return *this;
    }

    template< typename... Ts >
    parse_diagnostic &append_note( Ts &&... args )
    {
        return static_cast< parse_diagnostic & >(
                noted_diagnostic::append_note( std::forward< Ts >( args )... )
                );
    }
};

}
