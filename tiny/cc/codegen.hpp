#pragma once

#include "as/assembly.hpp"

namespace tiny::cc
{

struct context;

struct codegen_config
{
    bool trace_internals = false;
};

void generate( context &, as::assembly &, codegen_config );

}
