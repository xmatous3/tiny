#include <bitset>
#include <memory>
#include <stack>
#include <brick-trace>

#include "codegen.hpp"
#include "context.hpp"
#include "type.hpp"
#include "ident.hpp"
#include "as/instructions.hpp"

namespace tiny::cc
{

namespace
{

struct global_codegen
{
    context &ctx;
    as::assembly &as;
    codegen_config conf;

    unsigned label_stamp = 0;

    static inline constexpr std::string_view builtin_memcpy_label = ".memcpy";
    bool need_memcpy = false;

    static inline constexpr std::string_view builtin_bzero_label = ".bzero";
    bool need_bzero = false;

    auto new_labelset( auto... names )
    {
        unsigned i = label_stamp++;
        return std::tuple{ brq::format( ".", names, ".", i ).str()... };
    }

    void emit_label( std::string_view label )
    {
        // Remove previous jump if its destination is this address
        for ( auto it = as.items.end() - 1; it != as.items.begin() - 1; --it  )
        {
            if ( it->kind == as::assembly::item::instruction )
            {
                auto &last = it->instr;
                if ( last.is_jump() && last.address.off == 0 && last.address.sym == label  )
                {
                    it = as.items.erase( it );
                }
                else
                    break;
            }
        }

        as.add_label( label );
    }

    void emit( tiny::as::instruction && i )
    {
        if ( ( i.opcode == as::opcode::add_ri || i.opcode == as::opcode::add_ir )
                && i.immediate.raw == 0 )
            as.add_instr( as::copy( i.reg1, i.reg2 ) );
        else
            as.add_instr( std::move( i ) );
    }

    void emit_memcpy_implementation()
    {
        emit_label( builtin_memcpy_label );
        auto label_even = std::string( builtin_memcpy_label ) + ".even";

        /* The memcpy builtin has a different calling convention; it uses
         * temporary registers to pass parameters. Does not work for size = 0,
         * but that should be okay for our purposes of copying structs */
        auto src = as::reg::t1(),
             dst = as::reg::t2(),
             count = as::reg::t3(),
             temp = as::reg::t4();

        emit( as::and_ri( temp, count, 1 ) );
        emit( as::jz( temp, label_even ) );

        emit( as::ldb_r( temp, src, 0 ) );
        as.add_trigger( as::trigger::ubsan, "mem" );
        emit( as::stb_r( dst, temp, 0 ) );
        emit( as::add_ri( src, src, 1 ) );
        emit( as::add_ri( dst, dst, 1 ) );
        emit( as::sub_ri( count, count, 1 ) );

        emit_label( label_even );
        emit( as::ld_r( temp, src, 0 ) );
        as.add_trigger( as::trigger::ubsan, "mem" );
        emit( as::st_r( dst, temp, 0 ) );
        emit( as::add_ri( src, src, 2 ) );
        emit( as::add_ri( dst, dst, 2 ) );
        emit( as::sub_ri( count, count, 2 ) );
        emit( as::jnz( count, label_even ) );

        emit( as::ret() );
    }

    void emit_bzero_implementation()
    {
        emit_label( builtin_bzero_label );
        auto label_even = std::string( builtin_bzero_label ) + ".even";

        /* The builtin builtin has a different calling convention; it uses
         * temporary registers to pass parameters. Does not work for size = 0,
         * but that should be okay for our purposes of zeroing-out structs */
        auto dst = as::reg::t1(),
             count = as::reg::t2(),
             zero = as::reg::t3();

        emit( as::and_ri( zero, count, 1 ) );
        emit( as::jz( zero, label_even ) );

        emit( as::put( zero, 0 ) );
        emit( as::stb_r( dst, zero, 0 ) );
        emit( as::add_ri( dst, dst, 1 ) );
        emit( as::sub_ri( count, count, 1 ) );

        emit_label( label_even );
        emit( as::st_r( dst, zero, 0 ) );
        emit( as::add_ri( dst, dst, 2 ) );
        emit( as::sub_ri( count, count, 2 ) );
        emit( as::jnz( count, label_even ) );

        emit( as::ret() );
    }
};

struct function_codegen
{
    using regs_t = std::array< reg_t, 16 >;
    regs_t regs;

    using stashes_t = std::vector< stash_t >;
    stashes_t stashes;

    using regs_mask = std::bitset< 16 >;

    using reg_ix = reg_t::index_t;
    using stash_ix = stash_t::index_t;

    context &ctx;
    global_codegen &cg;
    const function &fun;

    std::string current_return_label;
    std::unique_ptr< ident_t > rv_ident;
    unsigned frame_size = 0;

    struct _temp_t : ident_t
    {
        function_codegen &p;
        template< typename... Args >
        _temp_t( function_codegen &p, Args &&... args )
            : ident_t( std::forward< Args >( args )... ), p( p ) {}
        ~_temp_t() { p.kill_ident( this ); }
    };

    using temp_t = std::unique_ptr< _temp_t >;
    std::vector< temp_t > temp_structs;

    struct snapshot_t
    {
        regs_t regs;
        stashes_t stashes;

        auto live_idents() const
        {
            std::set< ident_t * > live;
            for ( auto &r : regs )
                if ( r.is_live() )
                    live.insert( r.user );

            for ( auto &s : stashes )
                if ( s.is_live() )
                    live.insert( s.user );

            return live;
        }
    };

    struct target_t
    {
        const std::string &label;
        const snapshot_t &snapshot;
    };

    std::stack< target_t > continue_targets;
    std::stack< target_t > break_targets;

    inline static constexpr regs_mask regs_temporary = { 0x3f00 };
    inline static constexpr regs_mask regs_local = { 0x00fe };
    inline static constexpr regs_mask regs_protected = { 0xc000 };

    inline static constexpr regs_mask used( reg_ix ix )
    {
        regs_mask m;
        if ( ix )
            m.set( ix.zero_based() );
        return m;
    }

    auto new_labelset( auto... names ) { return cg.new_labelset( names... ); }

    function_codegen( global_codegen &cg, const function &fun )
        : ctx( cg.ctx ), cg( cg ), fun( fun ),
          rv_ident( new ident_t{ ".rv", fun.ident->type->return_type() } )
    {
        std::tie( current_return_label ) = new_labelset( "leave" );
    }

    as::assembly& as() { return cg.as; }

    bool is_internal() const { return fun.ident->is_reserved(); }
    bool is_silent() const { return is_internal() && !cg.conf.trace_internals; }

    reg_t& reg( reg_ix ix )
    {
        assert( ix.valid() );
        assert( ix.zero_based() < regs.size() );
        return regs[ ix.zero_based() ];
    }

    bool reg_is_protected( reg_ix ix )
    {
        return regs_protected.test( ix.zero_based() );
    }
    bool reg_is_writable( reg_ix ix )
    {
        return !reg( ix ).is_live() && !reg_is_protected( ix );
    }

    stash_t& stash( stash_ix ix )
    {
        assert( ix.valid() );
        assert( ix.zero_based() < stashes.size() );
        return stashes[ ix.zero_based() ];
    }

    void dbg_reg( auto r, ident_t *ident )
    {
        if ( is_silent() )
            return;

        as().add({ .kind = as::assembly::item::register_allocation,
                   .name = ident ? ident->name : "",
                   .placement = r.zero_based(),
                });
    }

    void dbg_stash( auto s, ident_t *ident )
    {
        if ( is_silent() )
            return;

        as().add({ .kind = as::assembly::item::stash_allocation,
                   .name = ident ? ident->name : "",
                   .placement = int16_t( -stash( s ).offset ),
                   .size = uint16_t( stash( s ).size ),
                 });
    }

    void dbg_location( document::span lit )
    {
        if ( is_silent() )
        {
            as().add_source_line( {} );
            return;
        }

        if ( !lit )
            return;
        as().add_source_line( lit );
    }

    void dbg_ubsan( std::string_view type, auto... args )
    {
        if ( is_internal() )
            return;

        as().add_trigger( as::trigger::ubsan, type, args... );
    }

    void dbg_ubsan( ast::expr::kind_t kind, bool sgn )
    {
        if ( is_internal() )
            return;

        if ( sgn && kind == ast::expr::shl )
            as().add_trigger( as::trigger::ubsan, "shl-neg" );
        if ( kind == ast::expr::shl || kind == ast::expr::shr )
            as().add_trigger( as::trigger::ubsan, "overshift" );
        if ( sgn && ( kind == ast::expr::add || kind == ast::expr::sub ||
                    kind == ast::expr::mul || kind == ast::expr::div ||
                    kind == ast::expr::mod || kind == ast::expr::shl ) )
            as().add_trigger( as::trigger::ubsan, "overflow" );
    }

    void dbg_mark( as::memory_mark::kind_t kind, reg_ix rbegin, uint16_t obegin,
                   reg_ix rend, uint16_t oend )
    {
        if ( is_internal() )
            return;

        as().add_trigger( as::memory_mark{ kind, rbegin, obegin, rend, oend } );
    }

    void emit_label( std::string_view label ) { cg.emit_label( label ); }
    void emit( tiny::as::instruction && i ) { cg.emit( std::move( i ) ); }

    temp_t new_temp( const type *t, std::string tmpname )
    {
        static int tmpnum;
        return temp_t{ new _temp_t{ *this, "." + std::move( tmpname ) + "." + std::to_string( tmpnum++ ), t } };
    }

    temp_t temp_from_reg( reg_ix r, const type *t, std::string tmpname = "tmp" )
    {
        assert( t->is_scalar() );

        if ( !r )
            return {};

        // If we need to treat sp/bp value as a temporary, we should have copied
        // it using move_to_cow_register().
        assert( !reg_is_protected( r ) );

        if ( reg( r ).is_live() )
        {
            assert( reg( r ).user->type->is_scalar() );
            return {};
        }

        temp_t ident = new_temp( t, tmpname );
        link_register( r, ident.get() );
        return ident;
    }

    // Returns src if it is not marked as used, or allocates a new register
    reg_ix get_cow_register( reg_ix src, reg_ix hint )
    {
        if ( reg_is_writable( src ) && ( src != as::reg::rv().ix() || src == hint ) )
            return src;

        return new_register( hint, true, used( src ) );
    }

    // As get_cow_register, but also moves the value if necessary
    reg_ix move_to_cow_register( reg_ix src, reg_ix hint )
    {
        auto dest = get_cow_register( src, hint );
        emit_mov( dest, src );
        return dest;
    }

    bool emit_mov( reg_ix dest, reg_ix src )
    {
        if ( dest == src )
            return false;
        emit( as::copy( dest, src ) );
        return true;
    }
    void emit_add_ri_signed( reg_ix dest, reg_ix base, int offset )
    {
        emit( offset >= 0 ? as::add_ri( dest, base, offset )
                          : as::sub_ri( dest, base, -offset ) );
    }

    void emit_memcpy()
    {
        for ( auto r : { as::reg::t1(), as::reg::t2(),
                         as::reg::t3(), as::reg::t4() } )
            assert( !reg( r.ix() ).is_live() );

        emit( as::call( cg.builtin_memcpy_label ) );
        cg.need_memcpy = true;
    }

    reg_ix new_register( reg_ix hint, bool spill = true, regs_mask used = {} )
    {
        // never spill stack pointer or base pointer
        used |= regs_protected;

        // forbidding takes precedence
        if ( hint && used.test( hint.zero_based() ) )
            hint.clear();

        if ( hint && !reg( hint ).is_live() )
            return hint;

        // if 'hint' is not available, try free temporary registers
        if ( auto tmp = new_register( regs_temporary & ~used, /* spill = */ false ) )
            return tmp;

        if ( !spill )
            return {};

        // since we must spill anyway, prefer spilling hint
        if ( hint )
        {
            spill_register( hint );
            return hint;
        }

        return new_register( regs_temporary & ~used );
    }
    reg_ix new_register( regs_mask mask, bool spill = true, bool avoid_pref = false )
    {
        assert( !avoid_pref || !spill );

        reg_ix last;
        for ( int r = 0; r < 16; ++r )
        {
            if ( mask.test( r ) )
            {
                last = reg_ix::from_zero_based( r );
                if( !reg( last ).is_live()
                        && ( !avoid_pref || !reg( last ).preferred_by ) )
                    return last;
            }
        }

        if ( !spill )
            return {};

        spill_register( last );
        return last;
    }

    void link_register( reg_ix ix, ident_t *ident )
    {
        assert( ix );
        assert( reg_is_writable( ix ) );
        assert( !ident->in_reg );

        reg( ix ).user = ident;
        ident->in_reg = ix;

        dbg_reg( ix, ident );
    }

    void kill_register( reg_ix ix )
    {
        assert( ix );
        assert( reg( ix ).is_live() );
        assert( reg( ix ).user->in_reg == ix );

        reg( ix ).user->in_reg.clear();
        reg( ix ).user = nullptr;

        dbg_reg( ix, nullptr );
    }

    void spill_register( reg_ix ix )
    {
        assert( ix );

        if ( reg_is_writable( ix ) )
            return;

        auto *ident = reg( ix ).user;

        if ( !ident->in_stash )
            link_stash( new_stash( ident->type->size() ), ident );

        if ( !ident->permanent_stash || !ident->type->is_const() )
            store_to_stash( ident );

        kill_register( ix );
    }

    void prefer_register( reg_ix ix, ident_t *ident )
    {
        assert( ident );
        assert( ix );
        assert( !ident->pref_reg );
        assert( !reg( ix ).preferred_by );

        reg( ix ).preferred_by = ident;
        ident->pref_reg = ix;
    }

    void disown_register( reg_ix ix )
    {
        assert( ix );
        assert( reg( ix ).preferred_by );
        assert( reg( ix ).preferred_by->pref_reg == ix );

        reg( ix ).preferred_by->pref_reg.clear();
        reg( ix ).preferred_by = nullptr;
    }

    stash_ix new_stash( int size = 2 )
    {
        int i = 0;
        for ( auto &s : stashes )
        {
            if ( !s.is_live() && s.size == size )
                return stash_ix::from_zero_based( i );
            ++i;
        }

        int offset = size + ( stashes.empty() ? 0 : stashes.back().offset );

        assert( offset > 0 );
        frame_size = std::max( frame_size, unsigned( offset ) );

        auto &s = stashes.emplace_back();
        s.offset = offset;
        s.size = size;

        return stash_ix::from_zero_based( i );
    }

    void link_stash( stash_ix ix, ident_t *ident )
    {
        assert( ix );
        assert( !stash( ix ).is_live() );
        assert( !ident->in_stash );

        stash( ix ).user = ident;
        ident->in_stash = ix;

        dbg_stash( ix, ident );
    }

    void kill_stash( stash_ix ix )
    {
        assert( ix );
        assert( stash( ix ).is_live() );

        assert( stash( ix ).user->in_stash == ix );

        stash( ix ).user->in_stash.clear();
        stash( ix ).user = nullptr;

        dbg_stash( ix, nullptr );
    }

    void new_ident( ident_t *ident )
    {
        assert( !ident->type->is_void() );

        if ( !ident->in_reg && ( ident->type->is_scalar() || ident->is_argument ) )
        {
            ident->in_reg = new_register( regs_local, /* spill = */ false, /* avoid_pref */ true );
            if ( ident->in_reg && regs_local.test( ident->in_reg.zero_based() )
                    && !reg( ident->in_reg ).preferred_by )
                prefer_register( ident->in_reg, ident );
        }

        if ( ident->in_reg )
        {
            reg( ident->in_reg ).user = ident;
            dbg_reg( ident->in_reg, ident );
        }
        else if ( ident->is_argument )
            UNREACHABLE( "arguments on stack not yet implemented" );

        if ( !ident->in_stash && ( ident->address_taken || !ident->in_reg ||
                    ( ident->is_argument && !ident->type->is_scalar() ) ) )
            link_stash( new_stash( ident->type->size() ), ident );

        if ( ident->address_taken || ( ident->is_local && !ident->pref_reg ) )
        {
            assert( ident->in_stash );
            ident->permanent_stash = true;
        }

        // For struct arguments, the register contains a pointer. Copy the
        // structure onto stack as a local variable of the function.
        if ( ident->is_argument && !ident->type->is_scalar() )
        {
            assert( ident->in_stash );
            assert( ident->in_reg );

            emit_mov( as::reg::t1().ix(), ident->in_reg );
            emit( as::sub_ri( as::reg::t2(), as::reg::bp(),
                             stash( ident->in_stash ).offset ) );
            emit( as::put( as::reg::t3(), ident->type->size() ) );

            emit_memcpy();

            // killing the register must wait until all arguments are introduced.
        }

        if ( ident->permanent_stash )
        {
            auto &st = stash( ident->in_stash );
            auto obj_end = mark_constant_areas( ident->type, -st.offset );

            assert( obj_end == -st.offset + st.size );
        }
    }

    int mark_constant_areas( const type *t, int offset )
    {
        auto base = offset;
        auto end = offset + t->size();

        if ( !t->is_readonly() )
            return end;

        if ( t->remove_array()->is_const() )
        {
            dbg_mark( as::memory_mark::readonly, as::reg::bp().ix(), base,
                      as::reg::bp().ix(), end );
            return end;
        }

        if ( auto *st = t->get_if< type::struct_type >() )
        {
            for ( auto f : st->fields )
            {
                assert( offset == base + f.offset && "no padding" );
                auto f_end = mark_constant_areas( &f.type, offset );
                offset += f.type.size();
                assert( f_end == offset );
            }
        }
        else if ( auto *at = t->get_if< type::array_type >() )
        {
            for ( int i = 0; i < at->length; ++i )
            {
                auto e_end = mark_constant_areas( &at->of, offset );
                offset += at->of.size();
                assert( e_end == offset );
            }
        }
        else
            UNREACHABLE();

        assert( offset == end );
        return offset;
    }

    void kill_ident( ident_t *ident )
    {
        if ( ident->in_reg )
            kill_register( ident->in_reg );
        if ( ident->in_stash )
            kill_stash( ident->in_stash );
        if ( ident->pref_reg )
            disown_register( ident->pref_reg );
    }

    snapshot_t snapshot() { return { regs, stashes }; }
    void spill_temporaries( regs_mask used = {}, unsigned n = 2 )
    {
        auto mask = regs_temporary & ~used;
        while ( n --> 0 )
        {
            auto r = new_register( mask );
            mask.reset( r.zero_based() );
        }
    }
    auto reconcile( const snapshot_t &s, regs_mask used = {} )
    {
        // Gather what is where and where we want it to be

        struct move_t
        {
            struct side_t
            {
                reg_ix reg = {};
                stash_ix stash = {};

                bool operator==( const side_t & ) const = default;
            } from,
              to;
        };

        std::map< ident_t *, move_t > moves;

        for ( int i = 0; i < s.regs.size(); ++i )
        {
            auto ix = reg_ix::from_zero_based( i );
            auto &rg = regs[ i ];
            auto &srg = s.regs[ i ];

            if ( rg.user )
                moves[ rg.user ].from.reg = ix;
            if ( srg.user )
            {
                assert( !used.test( i ) || srg.user == rg.user );
                moves[ srg.user ].to.reg = ix;
            }
        }

        assert( s.stashes.size() <= stashes.size() );
        for ( int i = 0; i < stashes.size(); ++i )
        {
            auto ix = stash_ix::from_zero_based( i );
            auto &st = stashes[ i ];

            if ( st.user )
                moves[ st.user ].from.stash = ix;
            if ( i < s.stashes.size() )
            {
                auto &sst = s.stashes[ i ];
                if ( sst.user )
                    moves[ sst.user ].to.stash = ix;
            }
        }

        // Perform all the moves in a safe order

        while ( !moves.empty() )
        {
            bool stuck = false;
            bool changed = false;
            reg_ix reg_victim = {};
            stash_ix stash_victim = {};

            // Do all conflict-free moves
            auto it = moves.begin();
            while ( it != moves.end() )
            {
                auto &[ ident, move ] = *it;
                auto &[ from, to ] = move;

                assert( from.reg || from.stash );

                // Move to correct register, if possible
                if ( to.reg && from.reg != to.reg )
                {
                    if ( reg( to.reg ).is_live() )
                    {
                        reg_victim = to.reg;
                    }
                    else
                    {
                        if ( from.reg )
                        {
                            emit_mov( to.reg, from.reg );
                            kill_register( from.reg );
                        }
                        else
                        {
                            assert( from.stash );
                            load_from_stash( ident, to.reg );
                        }

                        link_register( to.reg, ident );
                        from.reg = to.reg;
                        changed = true;
                    }
                }

                // Move to correct stash, if possible
                if ( to.stash && from.stash != to.stash )
                {
                    if ( stash( to.stash ).is_live() )
                    {
                        stash_victim = to.stash;
                    }
                    else
                    {
                        if ( from.reg )
                            store_to_stash( ident, to.stash );
                        else
                        {
                            assert( from.stash );

                            auto temp = new_register( ~regs_protected & ~used,
                                                      /* spill */ false );

                            // FIXME when does it not hold?
                            assert( temp && "no free registers during reconciliation" );

                            load_from_stash( ident, temp );
                            store_to_stash( ident, to.stash, temp );
                        }
                        if ( from.stash )
                            kill_stash( from.stash );

                        link_stash( to.stash, ident );
                        from.stash = to.stash;
                        changed = true;
                    }
                }

                // Kill the doomed stash if the value is in the correct register
                if ( from.stash && !to.stash && ( !to.reg || from.reg == to.reg ) )
                {
                    kill_stash( from.stash );
                    from.stash.clear();
                    changed = true;
                }

                // Kill the doomed register if the value is in the correct stash
                if ( from.reg && !to.reg && ( !to.stash || from.stash == to.stash ) )
                {
                    kill_register( from.reg );
                    from.reg.clear();
                    changed = true;
                }

                // If the identifier died, reap also its register preference
                if ( !to.reg && !to.stash )
                {
                    assert( !from.reg ); // should have been killed above
                    assert( !from.stash ); // should have been killed above
                    kill_ident( ident );
                }

                // Mark as done
                if ( from == to )
                {
                    it = moves.erase( it );
                }
                else
                    ++ it;
            }

            // Break a dependency cycle by moving something out of the way
            if ( changed )
                stuck = false;
            else if ( !moves.empty() )
            {
                assert( !stuck );
                stuck = true;
                NOT_IMPLEMENTED();
                // TODO
            }
        }

        // Retract the stack

        int shrink = 0;
        for ( int i = s.stashes.size(); i < stashes.size(); ++i )
        {
            auto ix = stash_ix::from_zero_based( i );
            assert( !stash( ix ).is_live() );
            shrink += stash( ix ).size;
        }

        if ( shrink > 0 )
        {
            stashes.resize( s.stashes.size() );
        }

        assert( regs == s.regs );
        assert( stashes == s.stashes );
    }

    auto force_restore( const snapshot_t &snap )
    {
        // Clear the state
        for ( auto &r : regs )
            if ( r.is_live() )
                kill_ident( r.user );
        for ( auto &s : stashes )
            if ( s.is_live() )
                kill_ident( s.user );

        // Force one direction of the mapping
        regs = snap.regs;
        stashes = snap.stashes;

        // Restore the reverse mapping
        for ( int i = 0; i < regs.size(); ++i )
        {
            auto &r = regs[ i ];
            auto ix = reg_ix::from_zero_based( i );

            if ( auto ident = r.user )
                r.user = nullptr, link_register( ix, ident );
            if ( auto ident = r.preferred_by )
                r.preferred_by = nullptr, prefer_register( ix, ident );
        }
        for ( int i = 0; i < snap.stashes.size(); ++i )
        {
            auto &s = stashes[ i ];
            auto ix = stash_ix::from_zero_based( i );

            if ( auto ident = s.user )
                s.user = nullptr, link_stash( ix, ident );
        }
    }

    void dbg_assign( const ast::expr &expr, reg_ix dest )
    {
        if ( is_silent() )
            return;

        /* Emit a no-op to hook the trigger onto when the actual assignment
         * happened inside a branch (ternary, short circuiting). */
        for ( auto it = as().items.end() - 1; it != as().items.begin() - 1; --it  )
        {
            if ( it->kind == as::assembly::item::instruction )
                break;
            if ( it->kind == as::assembly::item::label )
            {
                emit( as::copy( dest, dest ) );
                break;
            }
        }

        brq::string_builder msg;

        if ( expr.implicit )
        {
            assert( expr.left().kind == ast::expr::dot );
            msg << expr.left().subexpr().lit << " = " << expr.right().lit;
        }
        else
            msg << expr.lit;

        auto op_type = ast::expr::kind_t( expr.value );

        if ( expr.type->is_scalar() )
            msg << "; 0x$" << std::hex << dest.zero_based();

        as().add_trigger( as::trigger::trace, msg.data() );
    }

    void dbg_enter( const function &fun )
    {
        if ( is_silent() )
            return;

        brq::string_builder b;
        b << fun.ident->name;
        int i = 0;

        for ( auto a : fun.args )
        {
            b << ( i++ == 0 ? "( " : ", " );
            b << "0x$" << i;
        }

        b << ( i == 0 ? "()" : " )" );
        as().add_trigger( as::trigger::trace, b.data() );
        as().add_trigger( as::trigger::inc, "_indent_" );
    }

    void dbg_leave( const function &fun )
    {
        if ( is_silent() )
            return;

        if ( !fun.ident->type->return_type()->is_void() )
            as().add_trigger( as::trigger::trace, "return 0x$0" );
        as().add_trigger( as::trigger::dec, "_indent_" );
    }

    void generate()
    {
        dbg_location( fun.definition_site );
        emit_label( fun.ident->name );
        dbg_location( fun.body.lit );

        emit( as::push_r( as::reg::bp() ) );
        dbg_enter( fun );
        emit( as::copy( as::reg::bp(), as::reg::sp() ) );
        emit( as::sub_ri( as::reg::sp(), as::reg::sp(), 0xbeef ) ); // to backpatch
        auto frame_size_backpatch_ix = as().items.size() - 1;

        dbg_mark( as::memory_mark::readwrite, as::reg::sp().ix(), 0,
                  as::reg::bp().ix(), 0 );
        dbg_ubsan( "stack-overflow" );

        // Make sure the return-value pointer is preserved
        if ( rv_ident->type->is_struct() )
        {
            rv_ident->type = ctx.types.add_pointer( rv_ident->type );
            link_register( as::reg::rv().ix(), rv_ident.get() );
        }

        for ( auto ident : fun.args )
        {
            new_ident( ident );
            if ( ident->address_taken && ident->in_reg )
                store_to_stash( ident );
        }

        // Kill registers used to pass pointers to structures
        for ( auto ident : fun.args )
        {
            if ( ident->type->is_struct() && ident->in_reg )
                kill_register( ident->in_reg );
        }

        assert( fun.body.kind == ast::stmt::compound );
        gen_compound( fun.body.subs, fun.body.lit.last() );

        // backpatch stack frame size
        auto &instr = as().items[ frame_size_backpatch_ix ];
        assert( instr.kind == as::assembly::item::instruction );
        assert( instr.instr.opcode == as::opcode::sub_ri );
        if ( frame_size > 0 )
            instr.instr.immediate = frame_size;
        else
            as().items.erase( as().items.begin() + frame_size_backpatch_ix );

        for ( auto ident : fun.args )
            kill_ident( ident );

        // Detect reaching end of non-void function without a return statement
        if ( !rv_ident->type->is_void() &&
                ( fun.body.subs.empty() || fun.body.subs.back().kind != ast::stmt::ret ) )
        {
            if ( fun.ident->name == "main" )
            {
                emit( as::put( as::reg::rv(), 0 ) );
            }
            else
            {
                emit( as::put( as::reg::rv(), 0xdead ) );
                dbg_ubsan( "no-return" );
            }
        }

        emit_label( current_return_label );
        emit( as::copy( as::reg::sp(), as::reg::bp() ) );
        dbg_mark( as::memory_mark::inaccessible, as::reg::bp().ix(),
                  -frame_size_backpatch_ix, as::reg::bp().ix(), 0 );
        emit( as::pop( as::reg::bp() ) );

        kill_ident( rv_ident.get() );

        emit( as::ret() );
        dbg_leave( fun );

        for ( auto &ident : fun.locals )
            assert( !ident.is_live() );
    }

    void gen_stmt_or_decl( const ast::stmt &s, std::vector< ident_t * > &locals )
    {
        if ( s.kind == ast::stmt::declaration )
        {
            assert( s.decl );
            locals.push_back( s.decl );
            new_ident( s.decl );

            if ( s.should_bzero )
            {
                assert( !s.decl->type->is_scalar() );

                cg.need_bzero = true;

                auto t1 = as::reg::t1().ix(),
                     t2 = as::reg::t2().ix();

                auto st = s.decl->in_stash;
                assert( st );

                spill_register( t1 );
                spill_register( t2 );
                spill_register( as::reg::t3().ix() );
                emit( as::sub_ri( t1, as::reg::bp(), stash( st ).offset ) );
                emit( as::put( t2, stash( st ).size ) );
                emit( as::call( cg.builtin_bzero_label ) );
            }
        }
        else
            gen_stmt( s );

        assert( temp_structs.empty() );
    }
    void gen_stmt( const ast::stmt &s )
    {
        assert( s.kind != ast::stmt::declaration );

        dbg_location( s.lit );

        switch ( s.kind )
        {
            case ast::stmt::compound:
                gen_compound( s.subs, s.lit.last() );
                break;

            case ast::stmt::expression:
                if( s.expr )
                    gen_expression( *s.expr, {}, nullptr, /* result unused */ true );
                break;

            case ast::stmt::conditional:
                assert( s.expr );
                gen_conditional( *s.expr, s.subs );
                break;

            case ast::stmt::ret:
            {
                if ( s.expr && !rv_ident->type->is_void() )
                {
                    assert( rv_ident );

                    auto rv_ix = as::reg::rv().ix();
                    lvalue_ref ref{ .type = rv_ident->type };

                    if ( !s.expr->type->is_struct() )
                    {
                        ref.ident = rv_ident.get();  /* the object lives in rv */
                        if ( !rv_ident->pref_reg )
                            prefer_register( rv_ix, rv_ident.get() );

                        assert( rv_ident->pref_reg == rv_ix );
                    }
                    else
                    {
                        ref.obj_ident = rv_ident.get(); /* the object's address was in rv */
                        ref.type = ref.type->remove_pointer();
                    }

                    auto dest = gen_assignment_to_ref( ref, *s.expr, {}, rv_ix,
                                                       /* result unused */ false,
                                                       /* is initializer */ true );

                    assert( dest == rv_ix );
                }
                emit( as::jmp( current_return_label ) );
                break;
            }

            case ast::stmt::for_loop:
                if ( s.subs.size() == 2 )
                {
                    assert( s.subs[ 1 ].kind == ast::stmt::expression );
                    gen_loop( s.expr, s.subs[ 1 ].expr, s.subs[ 0 ], true );
                    break;
                }
                [[fallthrough]];
            case ast::stmt::while_loop:
                gen_loop( s.expr, {}, s.subs[ 0 ], true );
                break;

            case ast::stmt::do_while_loop:
                gen_loop( s.expr, {}, s.subs[ 0 ], false );
                break;

            case ast::stmt::brk:
            case ast::stmt::cont:
            {
                auto &target = ( s.kind == ast::stmt::brk ? break_targets
                                                          : continue_targets ).top();

                // We need to emit the correct reconcilliation code, but not
                // actually change the state, because code generation after
                // the jump must proceed as if nothing happened
                auto snap = snapshot();
                reconcile( target.snapshot );
                emit( as::jmp( target.label ) );
                force_restore( snap );

                break;
            }

            default:
                UNREACHABLE( "invalid statement kind" );
        }

        // TODO: most temporary structures can actually be killed much sooner
        temp_structs.clear();
    }

    void gen_compound( const std::vector< ast::stmt > &subs, auto brace_close )
    {
        std::vector< ident_t * > locals;

        for ( auto &stmt : subs )
            gen_stmt_or_decl( stmt, locals );

        dbg_location( brace_close );

        for ( auto ident : locals )
            kill_ident( ident );
    }

    void gen_conditional( const ast::expr &expr, const std::vector< ast::stmt > &subs )
    {
        assert( subs.size() == 1 || subs.size() == 2 );
        auto [ l_else, l_fi ] = new_labelset( "else", "fi" );
        auto dest = gen_expression( expr, {} );
        emit( as::jz( dest, l_else ) );

        spill_temporaries( used( dest ) );
        auto snap = snapshot();

        gen_stmt( subs[ 0 ] );
        reconcile( snap );

        if ( subs.size() == 2 )
        {
            emit( as::jmp( l_fi ) );
            emit_label( l_else );
            gen_stmt( subs[ 1 ] );
            reconcile( snap );
        }
        else
        {
            emit_label( l_else );
        }

        emit_label( l_fi );
    }

    void gen_loop( const std::optional< ast::expr > &cond, const std::optional< ast::expr > &post,
                   const ast::stmt &body, bool cond_first )
    {
        auto [ l_cond, l_post, l_done, l_loop ] = new_labelset( "cond", "post", "done", "loop" );

        spill_temporaries( {}, 3 ); // two for reconciliation, one for the loop
                                    // condition result
        auto snap = snapshot();

        continue_targets.emplace( l_post, snap );
        break_targets.emplace( l_done, snap );

        if ( cond_first )
            emit( as::jmp( l_cond ) );

        emit_label( l_loop );
        gen_stmt( body );

        reconcile( snap );

        emit_label( l_post );
        if ( post )
        {
            gen_expression( *post, {}, nullptr, /* result unused */ true );
            reconcile( snap );
        }

        emit_label( l_cond );
        if ( cond )
        {
            // Find a suitable register for the condition result
            reg_ix dest;
            for ( int i = 0; i < 16; ++i )
            {
                if ( regs_temporary.test( i ) && !snap.regs[ i ].is_live()
                        && !regs[ i ].is_live() )
                    dest = dest.from_zero_based( i );
            }
            assert( dest );

            dest = gen_expression( *cond, dest );
            reconcile( snap, used( dest ) );
            emit( as::jnz( dest, l_loop ) );
        }
        else
            emit( as::jmp( l_loop ) );

        emit_label( l_done );

        continue_targets.pop();
        break_targets.pop();
    }

    /* Possible combinations of fields in lvalue_ref:
     * ident: scalar (in register or stashed) or stashed struct
     * ident + offset: accessing a stashed struct's field
     * ident + off_reg: indexing a stashed array element
     * ident + off_reg + offset: field of a stashed array element
     * obj_reg: deref'd pointer
     * obj_reg + offset: result of the arrow operator
     * obj_reg + off_reg: indexing a generic pointer
     * obj_reg + off_reg + offset: field of a result of indexing a generic pointer
     * ... and the same with obj_ident instead of obj_reg for potentially
     * stashed away references.
     *
     * - If the index is a constant, offset can be used instead of off_reg.
     * - If obj_reg or off_reg is temporary, they can be addded together.
     * - In fact, they must be added sooner or later, because we don't have
     *   load/store instructions that could use both (and off_reg will usually
     *   be a temporary anyway, because we need to scale indices by element size).
     */
    struct lvalue_ref
    {
        const cc::type *type;
        ident_t *ident = nullptr;
        ident_t *obj_ident = nullptr; // mutually exclusive with ident and obj_reg
        int offset = 0; // constexpr offset for member access
        reg_ix obj_reg = {}; // mutually exclusive with ident and obj_ident
        reg_ix off_reg = {}; // runtime offset for indexing arrays

        bool is_scalar_ident() const
        {
            return ident && offset == 0 && !off_reg
                && ident->type == type && type->is_scalar();
        }
    };

    lvalue_ref gen_lvalue( const ast::expr& expr )
    {
        auto unarray = [ &expr ]( auto lref )
        {
            assert( lref.type->is_array() );
            assert( lref.type->remove_array() == expr.type );
            lref.type = expr.type;
            return lref;
        };

        switch ( expr.kind )
        {
            case ast::expr::identifier:
                assert( expr.ident );
                return { .type = expr.type, .ident = expr.ident };

            case ast::expr::deref:
            {
                assert( expr.value == 0 );

                auto &sub = expr.subexpr();

                if ( auto *ea = sub.try_decayed_array() )
                    return unarray( gen_lvalue( *ea ) );

                lvalue_ref lref{ .type = expr.type };

                if ( sub.kind == ast::expr::identifier )
                {
                    lref.obj_ident = sub.ident;
                    return lref;
                }

                if ( ( sub.kind == ast::expr::sub && sub.left().type->is_pointer() )
                        || sub.kind == ast::expr::add )
                {
                    auto &ep = sub.left().type->is_pointer() ? sub.left()
                                                             : sub.right();
                    auto &ei = sub.left().type->is_pointer() ? sub.right()
                                                             : sub.left();

                    assert( ep.type->is_pointer() );
                    assert( ei.type->is_arithmetic() );

                    if ( ep.kind == ast::expr::identifier )
                        lref.obj_ident = ep.ident;
                    else if ( auto *ea = ep.try_decayed_array() )
                        lref = unarray( gen_lvalue( *ea ) );
                    else
                        lref.obj_reg = gen_expression( ep, {} );

                    if ( ei.is_constant() )
                    {
                        auto sgn = sub.kind == ast::expr::add ? 1 : -1;
                        lref.offset += sgn * ei.value;
                    }
                    else
                    {
                        auto obj_tmp = preserve_lvalue_ref( lref, {} );
                        lref.off_reg = gen_expression( ei, {} );

                        if ( auto *obj_ident = lref.obj_ident )
                        {
                            lref.obj_reg = load_from_ident( obj_ident, {}, used( lref.off_reg ) );
                            lref.obj_ident = {};
                        }
                    }

                    return lref;
                }

                auto dest = gen_expression( expr.subexpr(), {} );
                lref.obj_reg = dest;
                return lref;
            }
            case ast::expr::dot:
            {
                auto lref = gen_lvalue( expr.subexpr() );
                lref.type = expr.type;
                lref.offset += expr.value;
                return lref;
            }

            case ast::expr::call:
            case ast::expr::comma:
            case ast::expr::assign:
            case ast::expr::cond:
            {
                // not technically an lvalue, but useful to treat as one
                assert( expr.type->is_struct() );
                auto obj_reg = gen_expression( expr, {} );
                return { .type = expr.type, .obj_reg = obj_reg };
            }

            default:
                UNREACHABLE( "expression cannot possibly be lvalue or designate a struct" );
        }
    }

    std::pair< reg_ix, int > gen_obj_address( lvalue_ref lref, reg_ix hint,
                                              regs_mask nospill = {} )
    {
        assert( !!lref.ident + !!lref.obj_reg + !!lref.obj_ident == 1 );

        // replace ident with its offset from the base pointer
        if ( auto *ident = lref.ident )
        {
            assert( ident->in_stash );

            // This would fire when accessing outside a known array with
            // a constexpr index and perhaps during some shenanigans with
            // overshooting pointers to struct fields.
            // TODO: print a warning? Is this the place?
            // assert( lref.offset < stash( ident->in_stash ).size );

            auto stash_offset = stash( ident->in_stash ).offset;

            lref.obj_reg = as::reg::bp().ix();
            lref.offset -= stash_offset;
        }
        else if ( auto *obj_ident = lref.obj_ident )
        {
            assert( obj_ident->type->is_pointer() );
            lref.obj_reg = load_from_ident( obj_ident, {}, nospill );
        }

        assert( lref.obj_reg );

        auto dest = lref.obj_reg;
        if ( lref.off_reg )
        {
            if ( reg( lref.obj_reg ).is_live() )
                nospill |= used( lref.obj_reg );
            if ( reg( lref.off_reg ).is_live() )
                nospill |= used( lref.off_reg );

            dest = new_register( hint, true, nospill );
            emit( as::add_rr( dest, lref.obj_reg, lref.off_reg ) );
        }

        return { dest, lref.offset };
    }

    // For dereferenced pointers, create a temporary ident to ensure
    // survival of the address
    [[nodiscard]]
    temp_t preserve_lvalue_ref( lvalue_ref &lref, reg_ix hint )
    {
        temp_t obj_tmp;

        if ( lref.obj_reg || lref.off_reg )
        {
            assert( !lref.obj_reg || !lref.obj_ident );

            auto [ obj_reg, offset ] = gen_obj_address( lref, hint );

            assert( !reg_is_protected( obj_reg ) || !lref.off_reg );

            lref = lvalue_ref{
                .type = lref.type,
                .offset = offset,
                .obj_reg = obj_reg,
            };

            if ( !reg_is_protected( obj_reg ) )
            {
                obj_tmp = temp_from_reg( obj_reg, ctx.types.add_pointer( lref.type ) );
            }

            if ( auto *obj_ident = reg( obj_reg ).user )
            {
                lref.obj_ident = obj_ident;
                lref.obj_reg.clear();
            }
        }

        return std::move( obj_tmp );
    }

    /* 'victim' is a poor (and lazy) man's substitute for a proper
     * define-use analysis. It is an ident that will no longer be needed
     * after this expression, because it is being assigned to. It is assumed
     * that 'hint' is it's current register, if any. */
    void kill_victim( ident_t *victim )
    {
        if ( victim && victim->in_reg )
            kill_register( victim->in_reg );
    }

    reg_ix gen_expression( const ast::expr &expr, reg_ix hint,
                           ident_t *victim = nullptr, bool unused = false )
    {
        assert( !expr.type->is_void() || unused );

        auto dest = _gen_expression( expr, hint, victim, unused );
        kill_victim( victim );
        return dest;
    }
    reg_ix _gen_expression( const ast::expr &expr, reg_ix hint, ident_t *victim,
                            bool unused = false )
    {
        // There are non-lvalue structs (results of calls, assignment,
        // commas and ternary operators) and though it is not an lvalue, we can
        // treat it as such in codegen if we make sure it lives long enough.
        if ( expr.is_lvalue() || expr.kind == ast::expr::dot )
        {
            // "lvalue conversion" (loading the value) or address of a struct
            assert( !expr.type->is_void() );
            auto lref = gen_lvalue( expr );
            return expr.type->is_scalar() ? gen_load( lref, hint )
                                          : gen_addrof( lref, hint );
        }

        if ( expr.is_constant() )
        {
            kill_victim( victim );

            if ( unused )
                return {};

            auto dest = new_register( hint );
            emit( as::put( dest, expr.value ) );
            return dest;
        }

        switch ( expr.kind )
        {
            case ast::expr::call:
                return gen_call( expr.subs );

            case ast::expr::cast:
                return gen_cast( expr.subexpr(), expr.type,
                                 hint, victim );

            case ast::expr::addrof:
                return gen_addrof( expr.subexpr(), hint );

            case ast::expr::uplus:
            case ast::expr::uminus:
            case ast::expr::lnot:
            case ast::expr::bwnot:
            {
                auto dest = gen_expression( expr.left(), hint );

                hint = get_cow_register( dest, hint );
                if ( expr.kind == ast::expr::uminus )
                    emit( as::mul_ri( hint, dest, -1 ) ); /* XXX signed */
                else if ( expr.kind == ast::expr::lnot )
                    emit( as::eq_ri( hint, dest, 0 ) );
                else if ( expr.kind == ast::expr::bwnot )
                    emit( as::xor_ri( hint, dest, 0xFFFF ) );
                else
                    emit_mov( hint, dest );

                return hint;
            }

            case ast::expr::postop:
                return gen_postop( expr.subexpr(), expr.value, hint, unused );

            case ast::expr::add:
            case ast::expr::sub:
            case ast::expr::mul:
            case ast::expr::div:
            case ast::expr::mod:
            case ast::expr::bwand:
            case ast::expr::bwor:
            case ast::expr::bwxor:
            case ast::expr::shl:
            case ast::expr::shr:
            case ast::expr::eq:
            case ast::expr::ne:
            case ast::expr::lt:
            case ast::expr::le:
            case ast::expr::gt:
            case ast::expr::ge:
                return gen_arithexpr( expr.kind, expr.left(), expr.right(),
                                      hint, victim );

            case ast::expr::land:
            case ast::expr::lor:
                return gen_boolexpr( expr.kind, expr.left(), expr.right(),
                                     hint );

            case ast::expr::cond:
                assert( expr.subs.size() == 3 );

                if ( expr[ 0 ].is_constant() )
                    return gen_expression( expr[ 2 - !!expr[ 0 ].value ], hint, victim, unused );

                return gen_condexpr( expr[ 0 ], expr[ 1 ], expr[ 2 ], hint, unused );

            case ast::expr::assign:
            {
                auto dest = gen_assignment( expr.left(), expr.right(),
                                            ast::expr::kind_t( expr.value ), hint,
                                            unused, expr.implicit );
                dbg_assign( expr, dest );
                return dest;
            }

            case ast::expr::comma:
            {
                auto dest = hint;
                for ( int i = 0; i < expr.subs.size(); ++i )
                {
                    bool result_unused = unused || i != expr.subs.size() - 1;
                    dest = gen_expression( expr[ i ], hint, nullptr, result_unused );
                }
                return dest;
            }
        }

        UNREACHABLE( "invalid expression kind" );
    }

    auto gen_arg( const ast::expr &expr, reg_ix hint = {} )
    {
        assert( !expr.type->is_void() );

        auto *realtype = expr.type;
        if ( !realtype->is_scalar() )
            realtype = ctx.types.add_pointer( realtype );

        return std::pair{ gen_expression( expr, hint ), realtype };
    }

    reg_ix gen_call( const std::vector< ast::expr > &subs,
                     std::optional< lvalue_ref > dest_ref = {} )
    {
        assert( subs.size() >= 1 );
        auto &callable = subs[ 0 ];

        // TODO: function pointers and indirect calls?
        auto ft = callable.type->get_if< type::function_type >();
        assert( ft );
        assert( callable.kind == ast::expr::identifier );
        assert( callable.cat == ast::expr::constant );
        const auto &symbol = callable.ident->name;
        const bool is_assert = symbol == "assert";

        constexpr auto rv_ix = as::reg::rv().ix();

        // TODO: pass extra args on stack
        assert( subs.size() <= 8 && "not impl" );

        // FIXME: this assumes what is the reg's constructor doing with reg_ix
        static_assert( as::reg::mkreg< 1 >().to_string() == "l1" );
        static_assert( as::reg::mkreg< 7 >().to_string() == "l7" );

        // compute arguments
        std::vector< temp_t > tmps;
        std::vector< ident_t* > args;
        for ( int i = 1; i < subs.size(); ++i )
        {
            auto &arg = subs[ i ];
            auto [ dest, realtype ]
                = gen_arg( arg, is_assert ? reg_ix{} : reg_ix::from_zero_based( i ) );

            tmps.push_back( temp_from_reg( dest, realtype, "arg" ) );
            args.push_back( reg( dest ).user );
        }

        if ( is_assert )
        {
            auto dest = load_from_ident( args[ 0 ], rv_ix );
            emit( as::asrt( dest ) );

            if ( !is_silent() )
            {
                auto msg = brq::format( "assert( ", subs[ 1 ].lit, " ); 0x$",
                                         std::hex, dest.zero_based() );
                as().add_trigger( as::trigger::trace, msg.data() );
            }

            return rv_ix;
        }

        // put arguments to correct registers
        for ( int i = 0; i < args.size(); ++i )
        {
            auto *ident = args[ i ];
            auto dest = reg_ix::from_zero_based( i + 1 );

            if ( reg( dest ).is_live() && reg( dest ).user != ident )
                spill_register( dest );

            if ( ident->in_reg )
                emit_mov( dest, ident->in_reg );
            else
                load_from_stash( ident, dest );
        }
        //
        // if rv already contains the correct result pointer,
        // do not spill it, because it would get re-loaded immediatelly
        if ( !dest_ref || !( dest_ref->obj_reg == rv_ix ||
                    ( dest_ref->obj_ident && dest_ref->obj_ident->in_reg == rv_ix ) ) )
            spill_register( rv_ix );

        // when the return value is a struct, allocate a stash for it and pass
        // its address in rv
        if ( ft->return_type.is_struct() )
        {
            if ( !dest_ref )
            {
                temp_structs.push_back( new_temp( &ft->return_type, "ret" ) );
                auto dest = new_stash( ft->return_type.size() );
                link_stash( dest, temp_structs.back().get() );
                dest_ref = lvalue_ref{
                    .type = &ft->return_type,
                    .ident = temp_structs.back().get(),
                };
            }

            assert( dest_ref );
            auto r_addr = gen_addrof( *dest_ref, rv_ix );

            emit_mov( rv_ix, r_addr );
        }

        for ( int i = 0; i < args.size(); ++i )
        {
            // kill the temporary to free its stash and prevent saving it in the
            // next step
            tmps[ i ].reset();
        }

        // save any remaining live registers
        static_assert( rv_ix.zero_based() == 0 );
        static_assert( as::reg::sp().ix().zero_based() == 15 );
        static_assert( as::reg::bp().ix().zero_based() == 14 );
        for ( int ix = 1; ix < 14; ++ix )
            spill_register( reg_ix::from_zero_based( ix ) );

        emit( as::call( symbol ) );

        return rv_ix;
    }

    reg_ix gen_cast( const ast::expr &sub, const type *to_type, reg_ix hint,
                     ident_t *victim = nullptr )
    {
        assert( sub.type != to_type ); /* Sanity check of the parser, actually */

        auto dest = gen_expression( sub, hint, victim );
        return _gen_cast( dest, sub.type, to_type, hint );
    }

    reg_ix _gen_cast( reg_ix src, const type *from_type, const type *to_type,
                      reg_ix hint )
    {
        if ( from_type->remove_const() == to_type->remove_const() )
            return src;

        if ( to_type->is_bool() || from_type->is_booleoid() )
        {
            auto dest = get_cow_register( src, hint );
            emit( as::ne_ri( dest, src, 0 ) );
            return dest;
        }
        if ( to_type->is_i8() )
        {
            auto dest = get_cow_register( src, hint );
            emit( as::sext( dest, src ) );
            return dest;
        }
        if ( to_type->is_u8() )
        {
            auto dest = get_cow_register( src, hint );
            emit( as::and_ri( dest, src, 0xFF ) );
            return dest;
        }

        return src;
    }

    reg_ix gen_addrof( const ast::expr &sub, reg_ix hint )
    {
        return gen_addrof( gen_lvalue( sub ), hint );
    }
    reg_ix gen_addrof( lvalue_ref lref, reg_ix hint )
    {
        auto [ obj_reg, offset ] = gen_obj_address( std::move( lref ), hint );

        auto dest = obj_reg;
        if ( offset )
        {
            dest = get_cow_register( obj_reg, hint );
            emit_add_ri_signed( dest, obj_reg, offset );
        }
        else if ( reg_is_protected( dest ) )
            dest = move_to_cow_register( dest, hint );

        return dest;
    }

    bool operand_is_neutral( int value, ast::expr::kind_t kind, bool right )
    {
        using ast::expr;

        switch ( kind )
        {
            case expr::sub:
            case expr::shl:
            case expr::shr:
                if ( !right ) return false; [[fallthrough]];
            case expr::add:
            case expr::bwor:
            case expr::bwxor:
                return value == 0;

            case expr::div:
                if ( !right ) return false; [[fallthrough]];
            case expr::mul:
                return value == 1;

            case expr::bwand:
                return ( value & 0xffff ) == 0xffff;
        }

        return false;
    }

    reg_ix gen_arithexpr( ast::expr::kind_t kind, const ast::expr &left,
                          const ast::expr &right, reg_ix hint,
                          ident_t *victim = nullptr )
    {
        assert( left.type == right.type ||
                ( left.type->is_pointer() && right.type->is_pointer() &&
                  left.type->remove_pointer()->remove_const()
                      == right.type->remove_pointer()->remove_const() ) ||
                ( left.type->is_pointer() && right.type->is_arithmetic() ) ||
                ( left.type->is_arithmetic() && right.type->is_pointer() ) );

        // Yeah, yeah, uses_ident gets called repeatedly for right branches.
        bool victim_used_in_right = victim && right.uses_ident( victim );

        if ( left.is_constant() )
        {
            if ( !victim_used_in_right )
            {
                kill_victim( victim );
                victim = nullptr;
            }

            bool sgn = left.type->is_signed();
            auto r_dest = gen_expression( right, hint, victim );

            if ( operand_is_neutral( left.value, kind, /* right */ false ) )
                return r_dest;

            auto dest = new_register( hint );

            switch ( kind )
            {
#define _arith_(_kind, _rrru, _rrrs, _rriu, _rris, _unsigned, _signed, ...)\
                case ast::expr::_kind: emit( ( sgn ? as::_signed : as::_unsigned )\
                                               ( dest, r_dest, left.value ) );\
                                       break;
#include "arithmetic.def"
#undef _arith_
                default: UNREACHABLE( "invalid arithmetic expression kind" );
            }

            dbg_ubsan( kind, sgn );

            return dest;
        }

        auto l_dest = gen_expression( left, hint, victim_used_in_right ? nullptr : victim );

        return gen_arithexpr_reg( kind, l_dest, left.type, right, hint,
                                  victim_used_in_right ? victim : nullptr );
    }

    reg_ix gen_arithexpr_reg( ast::expr::kind_t kind, reg_ix l_dest, const type *l_type,
                              const ast::expr &right, reg_ix hint,
                              ident_t *victim = nullptr )
    {
        bool sgn = l_type->is_signed();

        if ( right.is_constant() )
        {
            if ( operand_is_neutral( right.value, kind, /* right */ true ) )
                return l_dest;

            kill_victim( victim );
            auto dest = new_register( hint );

            switch ( kind )
            {
#define _arith_(_kind, _rrru, _rrrs, _unsigned, _signed, ...)\
                case ast::expr::_kind: emit( ( sgn ? as::_signed : as::_unsigned )\
                                               ( dest, l_dest, right.value ) );\
                                       break;
#include "arithmetic.def"
#undef _arith_
                default: UNREACHABLE( "invalid arithmetic expression kind" );
            }

            dbg_ubsan( kind, sgn );

            return dest;
        }

        // temporary ident_t to mark l_dest as live during generation of
        // the right side. May be null if l_dest is in fact used by an
        // already existing ident_t
        auto l_tmp = temp_from_reg( l_dest, l_type );
        auto l_ident = reg( l_dest ).user;

        auto r_victim = victim == l_ident ? nullptr : victim;

        auto r_dest = gen_expression( right, l_dest == hint ? reg_ix::none() : hint, r_victim );

        // The left result might have gotten spilled away
        if ( !l_ident->in_reg )
            load_from_ident( l_ident, hint, used( r_dest ) );

        assert( l_ident->in_reg );
        l_dest = l_ident->in_reg;

        // Kill the temporary (if any) so that its register is available
        // for storing the result
        l_tmp.reset();
        l_ident = nullptr;
        kill_victim( victim );

        auto dest = new_register( hint );

        switch ( kind )
        {
#define _arith_(_kind, _unsigned, _signed, ...) \
            case ast::expr::_kind: emit( ( sgn ? as::_signed : as::_unsigned )( dest, l_dest, r_dest ) );\
                                   break;
#include "arithmetic.def"
#undef _arith_
            default: UNREACHABLE( "invalid arithmetic expression kind" );
        }

        dbg_ubsan( kind, sgn );

        return dest;
    }

    reg_ix gen_boolexpr( ast::expr::kind_t kind, const ast::expr &left,
                         const ast::expr &right, reg_ix hint )
    {
        bool is_and = kind == ast::expr::land;

        auto [ label ] = new_labelset( "short" );

        auto dest = gen_expression( left, hint );
        dest = move_to_cow_register( dest, hint );

        spill_temporaries( used( dest ) );
        auto snap = snapshot();

        if ( is_and )
            emit( as::jz( dest, label ) );
        else
            emit( as::jnz( dest, label ) );

        auto r_dest = gen_expression( right, dest );

        reconcile( snap, used( r_dest ) );

        emit_mov( dest, r_dest );

        emit_label( label );
        return dest;
    }

    reg_ix gen_condexpr( const ast::expr &cexpr, const ast::expr &texpr,
                         const ast::expr &fexpr, reg_ix hint, bool unused = false )
    {
        auto [ l_else, l_fi ] = new_labelset( "else", "fi" );

        auto dest = gen_expression( cexpr, hint );
        spill_temporaries( used( dest ) );
        auto snap = snapshot();

        emit( as::jz( dest, l_else ) );

        dest = gen_expression( texpr, hint, /* victim */ nullptr, unused );

        if ( !unused )
            dest = move_to_cow_register( dest, hint );

        auto used_dest = unused ? regs_mask{} : used( dest );

        reconcile( snap, used_dest );

        emit( as::jmp( l_fi ) );

        emit_label( l_else );
        auto fdest = gen_expression( fexpr, dest, /* victim */ nullptr, unused );

        if ( !unused )
            emit_mov( dest, fdest );

        reconcile( snap, used_dest );

        emit_label( l_fi );
        return dest;
    }

    static const ast::expr *get_rvo_eligible_call( const ast::expr &e )
    {
        assert( e.type->is_struct() );

        if ( e.kind == ast::expr::cast )
        {
            assert( e.type->remove_const() == e.subexpr().type->remove_const() );
            return get_rvo_eligible_call( e.subexpr() );
        }

        return e.kind == ast::expr::call ? &e : nullptr;
    }

    reg_ix gen_assignment( const ast::expr &lexpr, const ast::expr &rexpr,
                           ast::expr::kind_t compound, reg_ix hint, bool unused,
                           bool is_initializer = false )
    {
        assert( compound || lexpr.type->remove_const() == rexpr.type->remove_const() );
        assert( lexpr.is_lvalue() );

        auto lref = gen_lvalue( lexpr );

        auto obj_tmp = preserve_lvalue_ref( lref, hint );

        return gen_assignment_to_ref( lref, rexpr, compound, hint, unused,
                                      is_initializer );
    }
    reg_ix gen_assignment_to_ref( lvalue_ref lref, const ast::expr &rexpr,
                                  ast::expr::kind_t compound,
                                  reg_ix hint, bool unused, bool is_initializer = false )
    {
        assert( !lref.type->is_void() );

        if ( lref.type->is_scalar() )
        {
            reg_ix l_reg;
            if ( compound )
                l_reg = gen_load( lref, hint );

            auto *ident = lref.ident; // null for non-trivial lvalue exprs

            auto force_dest = ident ? ident->pref_reg : reg_ix::none();

            if ( force_dest )
                hint = force_dest;

            if ( !hint && l_reg )
                hint = l_reg;

            if ( !hint && ident )
                hint = ident->in_reg;

            if ( ident && !compound && !rexpr.uses_ident( ident ) )
                kill_victim( ident );

            if ( compound )
            {
                assert( l_reg );
                assert( !lref.type->is_pointer() || rexpr.type->is_arithmetic() );
            }
            else
                assert( lref.type->remove_const() == rexpr.type->remove_const() );

            auto dest = compound
                      ? gen_arithexpr_reg( compound, l_reg, lref.type, rexpr, hint, ident )
                      : gen_expression( rexpr, hint, ident );

            kill_victim( ident );

            // Narrowing assignment in compound (other cases are handled by
            // adding an implicit cast into AST.
            // TODO: should all assignment-like casts be handled here?
            if ( compound )
                dest = _gen_cast( dest, ctx.types.get_int( rexpr.type->is_signed() ),
                                  lref.type, hint );

            if ( force_dest && !reg( force_dest ).is_live() )
            {
                emit_mov( force_dest, dest );
                dest = force_dest;
            }

            gen_store( lref, dest, /* with_ubsan */ !is_initializer );

            // As a rather special hack, address-taken constants are to be
            // stashed on initialisation, because no stashing will be generated
            // for them on register spill.
            if ( lref.is_scalar_ident() && ident->permanent_stash && lref.type->is_const() )
            {
                assert( is_initializer );
                store_to_stash( ident, {}, dest );
            }

            return dest;
        }
        else
        {
            assert( !compound );
            assert( lref.type->is_struct() );
            assert( lref.type->remove_const() == rexpr.type->remove_const() );

            // Return value optimisation
            if ( auto *call = get_rvo_eligible_call( rexpr ) )
            {
                auto dest = gen_call( call->subs, lref );

                if ( !is_initializer )
                    dbg_ubsan( "rvo", call->type->size() );

                return dest;
            }

            auto t1 = as::reg::t1().ix(),
                 t2 = as::reg::t2().ix(),
                 t3 = as::reg::t3().ix();

            spill_register( t1 );
            auto r_reg = gen_expression( rexpr, t1 );
            emit_mov( t1, r_reg );

            spill_register( t2 );

            auto [ obj_reg, offset ] = gen_obj_address( std::move( lref ), t2, used( t1 ) );

            if ( offset || obj_reg != t2 )
                emit_add_ri_signed( t2, obj_reg, offset );

            spill_register( t3 );
            emit( as::put( t3, lref.type->size() ) );

            spill_register( as::reg::t4().ix() );

            for ( auto r : { t1, t2 } )
                if ( reg( r ).is_live() )
                    kill_register( r );

            emit_memcpy();

            if ( !is_initializer )
                dbg_ubsan( "memcpy" );

            if ( unused )
                return {};

            if ( offset == 0 && reg( obj_reg ).is_live() )
                return obj_reg;

            auto dest = hint && reg_is_writable( hint ) ? hint : t2;
            emit( as::sub_ri( dest, t2, lref.type->size() ) );

            return dest;
        }
    }

    reg_ix gen_postop( const ast::expr &expr, int step, reg_ix hint, bool unused )
    {
        assert( expr.is_lvalue() );
        auto lref = gen_lvalue( expr );

        auto obj_tmp = preserve_lvalue_ref( lref, {} );

        auto l_reg = gen_load( lref, hint );

        // TODO: when the lvalue has pref_reg and was not in the register,
        // we could load it to the temporary right away and avoid the move
        auto dest = l_reg;
        if ( !unused )
        {
            regs_mask nospill = used( l_reg );
            if ( lref.obj_ident )
                nospill |= used( lref.obj_ident->in_reg );

            dest = new_register( hint, true, nospill );

            if ( reg_is_writable( l_reg ) )
                std::swap( dest, l_reg );
            else
                emit_mov( dest, l_reg );
        }

        assert( step != 0 );

        bool is_signed = expr.type->is_signed();
        emit_add_ri_signed( l_reg, dest, step );
        dbg_ubsan( ast::expr::add, is_signed );

        kill_victim( reg( l_reg ).user );

        auto cast_dest = _gen_cast( l_reg, ctx.types.get_int( is_signed ),
                                    expr.type, l_reg );
        assert( cast_dest == l_reg );

        gen_store( lref, l_reg );

        return dest;
    }

    reg_ix gen_load( lvalue_ref lref, reg_ix hint = {}, regs_mask used = {} )
    {
        assert( lref.type->is_scalar() );
        assert( !lref.type->is_booleoid() );

        if ( lref.is_scalar_ident() )
            return load_from_ident( lref.ident, hint, used );

        auto [ obj_reg, offset ] = gen_obj_address( lref, hint );

        auto dest = get_cow_register( obj_reg, hint );
        emit_load( dest, obj_reg, offset, lref.type );

        return dest;
    }

    void gen_store( lvalue_ref lref, reg_ix val_reg, bool with_ubsan = true )
    {
        assert( val_reg );
        assert( lref.type->is_scalar() );
        assert( !lref.type->is_booleoid() );

        if ( lref.is_scalar_ident() )
        {
            assert( !lref.type->is_const() || !with_ubsan );
            return store_to_ident( lref.ident, val_reg );
        }

        auto [ obj_reg, offset ] = gen_obj_address( lref, {}, used( val_reg ) );

        emit_store( val_reg, obj_reg, offset, lref.type, with_ubsan );
    }

    void emit_load( reg_ix dest, reg_ix obj_reg, uint16_t offset, const type *t,
                    bool with_ubsan = true )
    {
        assert( t->is_scalar() );

        if ( t->size() == 1 )
        {
            emit( as::ldb_r( dest, obj_reg, offset ) );
            if ( t->is_signed() )
                emit( as::sext( dest, dest ) );
        }
        else
            emit( as::ld_r( dest, obj_reg, offset ) );

        if ( with_ubsan )
            dbg_ubsan( "mem" );
    }

    void emit_store( reg_ix src, reg_ix obj_reg, uint16_t offset, const type *t,
                     bool with_ubsan = true )
    {
        assert( t->is_scalar() );

        if ( t->size() == 1 )
            emit( as::stb_r( obj_reg, src, offset ) );
        else
            emit( as::st_r( obj_reg, src, offset ) );

        if ( with_ubsan )
            dbg_ubsan( "mem" );
    }

    reg_ix load_from_ident( ident_t *ident, reg_ix hint = {}, regs_mask used = {} )
    {
        assert( ident->in_reg || ident->in_stash );

        reg_ix dest = ident->in_reg;
        bool was_in_reg = dest.valid();

        if ( !dest )
        {
            if ( ident->pref_reg && !reg( ident->pref_reg ).is_live()
                    && !used.test( ident->pref_reg.zero_based() ) )
                dest = ident->pref_reg;
            else
                dest = new_register( hint, true, used );

            if ( ident->pref_reg == dest || !ident->is_local )
                link_register( dest, ident );
        }

        if ( ident->in_stash && ( !was_in_reg
                    || ( ident->address_taken && !ident->type->is_const() ) ) )
            load_from_stash( ident, dest );

        return dest;
    }

    void store_to_ident( ident_t *ident, reg_ix val_reg )
    {
        assert( val_reg );

        if ( !ident->in_reg && !reg( val_reg ).is_live() &&
                ( ident->pref_reg == val_reg || !ident->is_local ) )
            link_register( val_reg, ident );

        if ( ident->in_reg )
            emit_mov( ident->in_reg, val_reg );

        if ( ident->in_stash )
        {
            if ( ident->in_reg && !ident->permanent_stash )
                kill_stash( ident->in_stash );
            else if ( !ident->type->is_const() )
                store_to_stash( ident, {}, val_reg );
        }

        assert( ident->in_reg || ident->in_stash );
    }

    void load_from_stash( ident_t *ident, reg_ix dest = {}, stash_ix src = {} )
    {
        if ( !src )
        {
            assert( ident->in_stash );
            src = ident->in_stash;
        }
        if ( !dest )
        {
            assert( ident->in_reg );
            dest = ident->in_reg;
        }

        emit_load( dest, as::reg::bp().ix(), -stash( src ).offset, ident->type );
    }

    void store_to_stash( ident_t *ident, stash_ix dest = {}, reg_ix src = {} )
    {
        if ( !src )
        {
            assert( ident->in_reg );
            src = ident->in_reg;
        }
        if ( !dest )
        {
            assert( ident->in_stash );
            dest = ident->in_stash;
        }

        emit_store( src, as::reg::bp().ix(), -stash( dest ).offset, ident->type,
                    /* with ubsan */ false );
    }

};

} /* anonymous namespace */


void generate( context &ctx, as::assembly &as, codegen_config conf )
{
    global_codegen cg{ ctx, as, conf };
    std::set< std::string > done;

    for ( std::string name : ctx.funcs_ordered )
    {
        if ( done.contains( name ) )
            continue;

        const auto &fun = ctx.funcs.at( name );
        assert( fun.ident );
        done.insert( name );

        function_codegen fcg{ cg, fun };
        fcg.generate();
    }

    if ( cg.need_memcpy )
        cg.emit_memcpy_implementation();
    if ( cg.need_bzero )
        cg.emit_bzero_implementation();
}

} /* tiny::cc */
