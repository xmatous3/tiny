#pragma once

#include "common/document.hpp"
#include "token.hpp"

namespace tiny::cc
{

struct document : tiny::document
{
    using tiny::document::document;
    using tiny::document::ref;

    enum class file_type
    {
        unknown,
        c,
        assembler,
    } type = file_type::unknown;

    bool is_internal = false;

    span ref( const token &t ) const { return ref( t.lit ); }
    span ref( const token &from, const token &to ) const
    {
        return ref( std::string_view( from.lit.begin(),
                                      to.lit.end() - from.lit.begin() ) );
    }

    bool deduce_type()
    {
        auto deduce = [this]( auto ft, auto... exts )
        {
            for ( auto ext : { exts... } )
            {
                if ( filename.ends_with( ext ) )
                {
                    type = ft;
                    return true;
                }
            }
            return false;
        };

        return deduce( file_type::c, ".c", ".C", ".h", ".H" )
            || deduce( file_type::assembler, ".asm", ".ASM", ".s", ".S" );
    }
};

}
