#pragma once

#include <set>
#include <string>
#include <vector>
#include "document.hpp"

namespace tiny::cc
{

static const std::set< std::string_view > keywords = {
    "if", "else", "for", "do", "while", "switch",
    "break", "continue", "case", "default", "return",
    "void", "int", "char", "bool", "unsigned", "signed",
    "const", "sizeof", "struct", "enum", "__override",

    // Not used in tinyc, but should not be allowed as a identifier
    "asm", "goto", "short", "long", "float", "double", "typedef", "union",
    "auto", "static", "volatile", "register", "extern", "restrict", "inline",
    "alignas", "alignof"
};

static const std::set< std::string_view > punct = {
    ";", ",", ".", "?", ":", "(", ")", "[", "]", "{", "}",
    "+", "-", "*", "/", "%", "|", "&", "^", "~", "=", "!", "<", ">",
    "++", "--", "&&", "||", "==", "!=", "<=", ">=", "<<", ">>", "->",
    "+=", "-=", "*=", "/=", "%=", "|=", "&=", "^=",
    "<<=", ">>=",
};

static const std::set< std::string_view > integral_suffices = { "", "u", "U" };

std::vector< token > lex( const document &doc );

}
