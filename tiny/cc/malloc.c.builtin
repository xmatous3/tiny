void __mark_rw( void *begin, void *end );
void __mark_no( void *begin, void *end );
void *__malloc( int p );
void __free( void *p );

struct _Chunk
{
    // Sizes include the chunk header
    unsigned prev_size; // part of the previous chunk, valid if !( meta & _Prev_used )
    unsigned meta;

    // Only valid if !( meta & _This_used ). Otherwise, next is the pointer
    // returned by malloc
    struct _Chunk *next,
                  *prev;
    // ...
    // At the end of a free chunk, ( meta & _Size ) is stored for the next chunk
};

enum
{
    _Prev_used = 0x8000u,
    _This_used = 0x4000u,
    _Size      = 0x3fffu,

    _Header_size = sizeof( unsigned ),
    _Footer_size = sizeof( unsigned ),
    _Data_offset = _Footer_size + _Header_size,
    _Min_allocd_size = sizeof( struct _Chunk ) - _Header_size,

    _Heap_begin = 0x8000u,
    _Heap_end   = 0xc000u,
};

void __init_heap()
{
    struct _Chunk *begin     = ( struct _Chunk * ) _Heap_begin,
                  *sentinel  = ( struct _Chunk * ) _Heap_end - 1;

    begin->prev_size = ( unsigned ) begin; // This is actually the head of the freelist

    int size = ( char * ) sentinel - ( char * ) begin;
    assert( size > sizeof( struct _Chunk ) );
    assert( !( size & ~_Size ) );

    begin->meta = _Prev_used | size;
    begin->next = NULL;
    begin->prev = NULL;

    sentinel->prev_size = size;
    sentinel->meta = _This_used;
    sentinel->next = NULL;
    sentinel->prev = begin;
}

struct _Chunk **__freelist()
{
    return ( struct _Chunk ** ) _Heap_begin;
}

struct _Chunk *__find_smallest( int csize )
{
    struct _Chunk *c = *__freelist();
    while ( c && ( c->meta & _Size ) < csize )
        c = c->next;

    assert( !c || !( c->meta & _This_used ) );
    return c;
}

void __freelist_yank( struct _Chunk *c )
{
    assert( !( c->meta & _This_used ) );

    if ( c->prev )
    {
        assert( !( c->prev->meta & _This_used ) );
        assert( c->prev->next == c );
        c->prev->next = c->next;
    }
    else
    {
        struct _Chunk **fl = __freelist();
        assert( *fl == c );
        *fl = c->next;
    }

    if ( c->next )
    {
        assert( c->next->prev == c );
        c->next->prev = c->prev;
    }
}

void __freelist_insert( struct _Chunk *c )
{
    assert( c );

    int size = c->meta & _Size;
    assert( size >= sizeof( struct _Chunk ) );

    struct _Chunk **fl = __freelist();
    struct _Chunk *prev = *fl;

    if ( !prev || ( prev->meta & _Size ) >= size )
    {
        *fl = c;
        if (( c->next = prev ))
            prev->prev = c;
        c->prev = NULL;
        return;
    }

    while ( prev->next && ( prev->next->meta & _Size ) < size )
    {
        prev = prev->next;
    }

    c->prev = prev;
    if (( c->next = prev->next ));
        prev->next->prev = c;
    prev->next = c;
}

struct _Chunk *__adjacent_next( struct _Chunk *c )
{
    return ( struct _Chunk * ) ( ( char * ) c + ( c->meta & _Size ) );
}

struct _Chunk *__adjacent_prev( struct _Chunk *c )
{
    assert( !( c->meta & _Prev_used ) );
    return ( struct _Chunk * ) ( ( char * ) c - ( c->prev_size ) );
}

void *__data_of( struct _Chunk *c )
{
    return ( char * ) c + _Data_offset;
}

void *__malloc_impl( int size )
{
    assert( size >= 0 );

    if ( size < _Min_allocd_size )
        size = _Min_allocd_size;

    size += _Header_size;

    struct _Chunk *c = __find_smallest( size );
    if ( !c )
        return NULL;

    struct _Chunk *n = __adjacent_next( c );
    assert( !( n->meta & _Prev_used ) );

    __freelist_yank( c );

    int csize = c->meta & _Size;
    assert( csize >= size );

    int rsize = csize - size;
    if ( rsize >= sizeof( struct _Chunk ) )
    {
        c->meta &= ~_Size;
        c->meta |= size;

        struct _Chunk *r = __adjacent_next( c );
        r->meta = rsize;

        assert( __adjacent_next( r ) == n );
        n->prev_size = rsize;

        __freelist_insert( r );

        n = r;
    }

    c->meta |= _This_used;
    n->meta |= _Prev_used;

    void *data = __data_of( c );
    __mark_rw( data, ( char * ) n + _Footer_size );

    return data;
}

void __free_impl( void *ptr )
{
    if ( !ptr )
        return;

    struct _Chunk *c = ( struct _Chunk *) ( ( char * ) ptr - _Data_offset );
    assert( c->meta & _This_used );
    assert( c->meta & _Size );

    struct _Chunk *n = __adjacent_next( c );

    __mark_no( ptr, ( char * ) n + _Footer_size );

    // coalesce forward
    if ( !( n->meta & _This_used ) )
    {
        int n_size = n->meta & _Size;
        __freelist_yank( n );

        // assert size does not overflow outside its mask
        assert( !( ( ( c->meta & _Size ) + n_size ) & ~_Size ) );
        c->meta += n_size;

        n = __adjacent_next( c );
    }
    else
        n->meta &= ~_Prev_used;

    // coalesce back
    if ( !( c->meta & _Prev_used ) )
    {
        struct _Chunk *p = __adjacent_prev( c );
        assert( !( p->meta & _This_used ) );

        // assert size does not overflow outside its mask
        assert( !( ( ( p->meta & _Size ) + ( c->meta & _Size ) ) & ~_Size ) );
        p->meta += c->meta & _Size;

        __freelist_yank( p );
        c = p;
    }
    else
        c->meta &= ~_This_used;

    assert( n->meta & _This_used );
    assert( !( c->meta & _This_used ) );
    assert( c->meta & _Prev_used );

    n->prev_size = c->meta & _Size;
    n->meta &= ~_Prev_used;

    __freelist_insert( c );
}

void *__realloc( void *p, int size )
{
    void *np = __malloc( size );

    if ( np )
    {
        if ( p && p != np )
        {
            for ( char *s = p, *d = np; size; --size )
                *d++ = *s++;
        }

        __free( p );
    }

    return np;
}

void *malloc( int size )
{
    return __malloc( size );
}

void free( void *p )
{
    __free( p );
}

void *realloc( void *p, int size )
{
    return __realloc( p, size );
}
