#include <stdexcept>

#include "common/assert.hpp"
#include "lexer.hpp"
#include "diagnostic.hpp"
#include "common/lexer_base.hpp"
#include "common/limits.hpp"

namespace tiny::cc
{

std::vector< token > lex( const document &doc )
{
    lexer_base l( doc );
    auto &sv = l.sv;
    std::vector< token > out;

    using cat = token::category;

    while ( l.drop_blanks(), l )
    {
        auto begin = sv.begin();

        /* Comments */
        if ( l.try_drop( "//" ) )
        {
            auto nl = 0;
            do
            {
                nl = sv.find( '\n', nl );
                if ( nl == sv.npos )
                    throw lex_error( doc.ref_char( sv ), "no newline to end single-line comment" );
                ++nl;
            }
            while ( nl > 1 && sv[nl - 2] == '\\'
                 || nl > 2 && sv.substr( nl - 3, 2 ) == "\\\r" );
            sv.remove_prefix( nl );
            continue;
        }
        if ( l.try_drop( "/*" ) )
        {
            auto end = sv.find( "*/" );
            if ( end == sv.npos )
                throw lex_error( doc.ref_char( sv ), "unterminated multi-line comment" );
            sv.remove_prefix( end + 2 );
            continue;
        }

        /* Identifiers and keywords */
        if ( auto w = l.shift_word(); !w.empty() )
        {
            if ( w == "true" || w == "false" || w == "NULL" )
                out.emplace_back( w, cat::number, false, w == "true" );
            else
                out.emplace_back( w, keywords.contains( w ) ? cat::keyword : cat::ident );
            continue;
        }

        /* Number literals */
        if ( auto n = l.try_unsigned() )
        {
            auto suffix = l.shift_word();
            if ( !suffix.empty() && !( suffix == "u" || suffix == "U" ) )
                throw lex_error( doc.ref( suffix ), "invalid integral suffix" );

            auto lit = std::string_view( begin, sv.begin() - begin );
            if ( !within_u16( n.value() ) )
                throw lex_error( doc.ref( lit ), "integer literal too large" );

            out.emplace_back( lit, cat::number, !suffix.empty(), n.value() );
            continue;
        }

        /* Punctuation (operators, parentheses etc.) */
        bool was_punct = false;
        for ( int l : { 3, 2, 1 } )
        {
            if ( sv.size() < l )
                continue;

            auto op = sv.substr( 0, l );
            if ( punct.contains( op ) )
            {
                sv.remove_prefix( l );
                out.emplace_back( op, cat::punct );
                was_punct = true;
                break;
            }
        }
        if ( was_punct )
            continue;

        /* Lexing error, try to inform of tinyc's limitations */
        auto ch = sv.front();
        if ( ch == '\"' )
            throw lex_error( doc.ref_char( sv ), "string literals are not supported" );
        if ( ch == '\'' )
            throw lex_error( doc.ref_char( sv ), "character literals are not supported" );
        if ( ch == '#' )
            throw lex_error( doc.ref_char( sv ), "preprocessor directives are not supported" );
        throw lex_error( doc.ref_char( sv ), "unexpected character" );
    }
    assert( l.ok );
    return out;
}

}
