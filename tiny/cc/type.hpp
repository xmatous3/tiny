#pragma once

#include <compare>
#include <string>
#include <variant>
#include <vector>

#include "common/assert.hpp"

#ifndef __cpp_lib_three_way_comparison
inline std::strong_ordering operator<=>( std::string_view a, std::string_view b )
{
    if ( a.size() != b.size() )
        return a.size() <=> b.size();

    int cmp = std::strncmp( a.data(), b.data(), a.size() );

    if ( cmp == 0 )
        return std::strong_ordering::equal;

    if ( cmp < 0 )
        return std::strong_ordering::less;
    else
        return std::strong_ordering::greater;
}

inline std::strong_ordering operator<=>( const std::string &a, const std::string &b )
{
    return std::string_view( a ) <=> std::string_view( b );
}
#endif

namespace tiny::cc
{

const uint16_t pointer_size = 2;
const uint16_t word_size = 2;

template< typename Self >
struct type_base
{
    const std::string name;

    explicit type_base( std::string name ) : name( std::move( name ) ) {}

    const Self& self() const { return static_cast< const Self& >( *this ); }
    std::strong_ordering operator<=>( const type_base & ) const = default;
    std::strong_ordering operator<=>( const std::string &tname ) const
    {
        return name <=> tname;
    }
};

template< typename type >
struct _const_type : type_base< _const_type< type > >
{
    const type &underlying;

    explicit _const_type( const type &t )
        : type_base< _const_type >( std::string( t.name() ) + " const" ), underlying( t )
    {
        assert( !t.is_const() );
    }

    uint16_t size() const { return underlying.size(); }
};

template< typename type >
struct _pointer_type : type_base< _pointer_type< type > >
{
    const type &to;

    explicit _pointer_type( const type &t )
        : type_base< _pointer_type >( std::string( t.name() ) + " *" ), to( t ) {};

    uint16_t size() const { return pointer_size; }
};

template< typename type >
struct _builtin_type : type_base< _builtin_type< type > >
{
    using type_base< _builtin_type >::type_base;

    uint16_t _size;
    bool is_signed = false;
    bool is_void = false;
    bool is_bool = false;
    uint16_t size() const { return _size; }
    bool is_arithmetic() const { return !is_void; }
};

template< typename type_t >
struct _struct_type : type_base< _struct_type< type_t > >
{
    bool complete = false;
    bool has_array = false;
    bool is_readonly = false;

    struct field_t
    {
        std::string name;
        const type_t& type;
        uint16_t offset;
    };
    std::vector< field_t > fields;

    explicit _struct_type( const std::string &name )
        : type_base< _struct_type >( "struct " + name ) {}

    uint16_t _realsize() const { return fields.empty() ? 0 : fields.back().offset + fields.back().type.size(); }
    uint16_t size() const
    {
        assert( complete );
        return fields.empty() ? 1 : _realsize();
    }
    void finish()
    {
        assert( !complete );
        // TODO: something with alignment maybe
        complete = true;
    }
    void add_field( std::string_view fname, const type_t& t )
    {
        assert( !complete );
        uint16_t offset = _realsize();
        fields.emplace_back( std::string{ fname }, t, offset );

        is_readonly = is_readonly || t.is_readonly();

        if ( auto *st = t.remove_const()->template get_if< _struct_type >() )
            has_array = has_array || st->has_array;
        else
            has_array = has_array || t.remove_const()->is_array();

        // TODO: something with alignment maybe
    }
    bool has_field( std::string_view fname ) const { return field_index( fname ) >= 0; }
    int field_index( std::string_view fname ) const
    {
        // structs are probably going to be short enough that linear scan is ok
        int ix = 0;
        for ( auto &f : fields )
        {
            if ( f.name == fname )
                return ix;
            ++ix;
        }
        return -1;
    }
    const field_t* field( std::string_view fname ) const
    {
        auto ix = field_index( fname );
        if ( ix < 0 )
            return nullptr;
        return &fields[ ix ];
    }
};

template< typename type >
struct _function_type : type_base< _function_type< type > >
{
    const type &return_type;
    std::vector< const type* > argument_types;

    _function_type( const type &ret, std::vector< const type* > args )
        : type_base< _function_type >( build_name( ret, args ) ),
          return_type( ret ), argument_types( std::move( args ) )
    {
        for ( auto *t : argument_types )
            assert( t );
    }

    int arity() const { return argument_types.size(); }
    uint16_t size() const { return pointer_size; }
    bool is_complete() const
    {
        for ( auto *t : argument_types )
            if ( !t->is_complete() )
                return false;
        return return_type.is_complete() || return_type.is_void();
    }

    static std::string build_name( const type &ret, const std::vector< const type* > args )
    {
        std::string name( ret.name() );
        name += " (";
        bool first = true;
        for ( auto *t : args )
        {
            if ( !first )
                name += ", ";
            name += t->name();
            first = false;
        }
        name += ")";
        return name;
    }
};

template< typename type >
struct _array_type : type_base< _array_type< type > >
{
    const type &of;
    int length; /* 0 for unknown size (used while parsing initializers) */

    explicit _array_type( const type &t, int len )
        : type_base< _array_type >( std::string( t.name() )
                + "[" + ( len > 0 ? std::to_string( len ) : "" ) + "]" ),
          of( t ), length( len ) {};

    uint16_t size() const { return length * of.size(); }
    bool is_complete() const
    {
        return length > 0 && of.is_complete();
    }
};


struct type
{
    using const_type = _const_type< type >;
    using pointer_type = _pointer_type< type >;
    using builtin_type = _builtin_type< type >;
    using struct_type = _struct_type< type >;
    using function_type = _function_type< type >;
    using array_type = _array_type< type >;

    std::variant< const_type, pointer_type, builtin_type,
                  struct_type, function_type, array_type > _rep;

    template< typename Rep >
    type( Rep &&rep ) : _rep( std::forward< Rep >( rep ) ) {}
    type( const type& ) = delete;
    type( type&& ) = delete;

    std::strong_ordering operator<=>( const std::string_view tname ) const
    {
        return name() <=> tname;
    }
    std::strong_ordering operator<=>( const type &o ) const { return name() <=> o.name(); }

    std::string_view name() const { return std::visit( []( auto &t ) -> auto & { return t.name; }, _rep ); }
    uint16_t size() const { return std::visit( []( auto &t ){ return t.size(); }, _rep ); }

    template< typename Rep >
    const Rep* get_if() const { return std::get_if< Rep >( &_rep ); }

    bool is_scalar() const
    {
        auto unq = remove_const();
        return unq->is_pointer() || unq->is_arithmetic();
    }

    bool is_const() const { return std::holds_alternative< const_type >( _rep ); }
    bool is_readonly() const
    {
        if ( auto st = std::get_if< struct_type >( &_rep ) )
            return st->is_readonly;
        if ( auto at = std::get_if< array_type >( &_rep ) )
            return at->of.is_readonly();
        return is_const();
    }
    bool is_function() const { return std::holds_alternative< function_type >( _rep ); }
    bool is_pointer() const
    {
        if ( std::holds_alternative< pointer_type >( _rep ) )
            return true;
        if ( auto c = std::get_if< const_type >( &_rep ) )
            return c->underlying.is_pointer();
        return false;
    }
    bool is_array() const { return std::holds_alternative< array_type >( _rep ); }
    bool is_struct() const
    {
        if ( std::holds_alternative< struct_type >( _rep ) )
            return true;
        if ( auto c = std::get_if< const_type >( &_rep ) )
            return c->underlying.is_struct();
        return false;
    }
    bool is_void() const
    {
        if ( auto t = remove_const()->get_if< builtin_type >() )
            return t->is_void;
        return false;
    }
    bool is_complete() const
    {
        if ( auto s = remove_const()->get_if< struct_type >() )
            return s->complete;
        if ( auto a = remove_const()->get_if< array_type >() )
            return a->is_complete();
        if ( auto f = remove_const()->get_if< function_type >() )
            return f->is_complete();
        return !is_void();
    }
    bool is_arithmetic() const
    {
        if ( auto s = remove_const()->get_if< builtin_type >() )
            return s->is_arithmetic();
        return false;
    }
    bool is_signed() const
    {
        auto t = remove_const();

        assert( t->is_arithmetic() || t->is_pointer() );
        if ( t->is_pointer() )
            return false;

        return t->get_if< builtin_type >()->is_signed;
    }

    bool is_u16() const
    {
        return is_arithmetic() && !is_signed() && size() == 2 && !is_boollike();
    }
    bool is_i16() const
    {
        return is_arithmetic() && is_signed() && size() == 2 && !is_boollike();
    }
    bool is_u8() const
    {
        return is_arithmetic() && !is_signed() && size() == 1 && !is_boollike();
    }
    bool is_i8() const
    {
        return is_arithmetic() && is_signed() && size() == 1 && !is_boollike();
    }
    bool is_bool() const
    {
        return size() == 1 && is_boollike();
    }
    bool is_booleoid() const
    {
        return size() == 2 && is_boollike();
    }
    bool is_boollike() const
    {
        if ( auto t = get_if< builtin_type >() )
            return t->is_bool;
        return false;
    }

    const type* remove_const() const
    {
        if ( auto c = std::get_if< const_type >( &_rep ) )
            return &c->underlying;
        return this;
    }
    const type* remove_pointer() const
    {
        // non-const pointer
        if ( auto p = std::get_if< pointer_type >( &_rep ) )
            return &p->to;
        // const pointer
        if ( auto c = std::get_if< const_type >( &_rep ) )
            if ( c->underlying.is_pointer() )
                return c->underlying.remove_pointer();
        // non-pointer
        return this;
    }
    const type* try_remove_array() const
    {
        if ( auto a = std::get_if< array_type >( &_rep ) )
            return &a->of;
        return nullptr;
    }
    const type* remove_array() const
    {
        if ( auto t = try_remove_array() )
            return t;
        return this;
    }
    const type* return_type() const
    {
        auto f = std::get_if< function_type >( &_rep );
        assert( f );
        return &f->return_type;
    }
};

inline bool types_are_convertible( const type *from, const type *to, bool implicit = true )
{
    assert( from );
    assert( to );

    from = from->remove_const();
    to   = to->remove_const();

    if ( from == to )
        return true;

    if ( from->is_arithmetic() && to->is_arithmetic() )
        return true;

    if ( to->is_void() )
        return true;

    if ( from->is_pointer() && to->is_boollike() )
        return true;

    if ( from->is_pointer() && to->is_pointer() )
    {
        const type *pfrom = from->remove_pointer();
        const type *pto = to->remove_pointer();

        return !implicit || pfrom == pto->remove_const()
            || ( ( !pfrom->is_const() || pto->is_const() )
                    && ( pfrom->is_void() || pto->is_void() ) );
    }
    if ( ( from->is_pointer() && to->is_arithmetic() ) ||
            ( from->is_arithmetic() && to->is_pointer() ) )
        return !implicit;

    if ( from->is_array() && to->is_pointer() )
    {
        const type *afrom = from->remove_array();
        const type *pto = to->remove_pointer();

        return afrom->remove_const() == pto->remove_const()
            && ( !afrom->is_const() || pto->is_const() );
    }

    return false;
}

}
