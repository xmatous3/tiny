#include "diagnostic.hpp"

namespace tiny::cc
{

brq::string_builder &parse_diagnostic::print( brq::string_builder &out, kind_t kind, config_t conf ) const
{
    std::string_view blue = conf.colourful ? "\033[34m" : "",
                     bold = conf.colourful ? "\033[1m" : "",
                     endc = conf.colourful ? "\033[m" : "";

    auto &doc = where.doc();

    if ( !in_function.empty() )
    {
        format_location( out, doc.ref_file(), conf )
            << ": In function '" << bold << in_function << endc << "':\n";
    }

    for ( auto it = st.open_constituents.begin(); it != st.open_constituents.end(); )
    {
        auto &con = *it;
        auto hili = std::string_view( con.span.begin(), where.sv.begin() - con.span.begin() );
        auto diag = source_diagnostic( doc.ref( hili ), "while parsing this ", blue, con.name, endc );

        while ( ++it < st.open_constituents.end() && con.span.begin() == it->span.begin() )
            diag << " > " << blue << it->name << endc;

        diag.print( out, diagnostic::context, conf );
    }
    for ( auto &con : st.closed_constituents )
    {
        auto diag = source_diagnostic( doc.ref( con.span ), "having parsed this ", blue, con.name, endc );
        diag.print( out, diagnostic::context, conf );
    }

    noted_diagnostic::print( out, kind, conf );
    return out;
}

}
