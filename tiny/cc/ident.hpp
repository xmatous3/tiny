#pragma once

#include <variant>
#include <string>
#include <string_view>
#include <optional>
#include <cstdint>

#include "common/tagged_index.hpp"
#include "type.hpp"
#include "document.hpp"

namespace tiny::as
{
// used as a tag for register indices
struct reg;
}

namespace tiny::cc
{

inline bool is_reserved_identifier( std::string_view name )
{
    return name.length() >= 2 && name[ 0 ] == '_'
        && ( name[ 1 ] == '_' || isupper( name[ 1 ] ) );
}

struct ident_t;

struct reg_t
{
    using index_t = tagged_index< as::reg, int8_t >;

    ident_t *user = nullptr;
    ident_t *preferred_by = nullptr; // for allocated local registers

    bool is_live() const { return user != nullptr; }

    bool operator==( const reg_t & ) const = default;
};

struct stash_t
{
    using index_t = tagged_index< stash_t, int16_t >;

    short offset;
    short size;
    ident_t *user;

    bool is_live() const { return user != nullptr; }

    bool operator==( const stash_t & ) const = default;
};

struct ident_t
{
    std::string name;
    const cc::type *type = nullptr;

    stash_t::index_t in_stash;
    reg_t::index_t in_reg;
    reg_t::index_t pref_reg;

    bool is_local = false;
    bool is_argument = false;
    bool address_taken = false;
    bool permanent_stash = false;

    std::optional< int > enum_value;
    document::span declaration_site;

    explicit ident_t( std::string name, const cc::type *t )
        : name( std::move( name ) ), type( t ) { assert( t ); }
    ident_t( std::string_view name, const cc::type *t, document::span decl_site )
        : name( name ), type( t ), declaration_site( decl_site ) { assert( t ); }

    ~ident_t() { assert( !is_live() ); }
    ident_t( const ident_t & ) = delete;
    ident_t( ident_t && ) = delete;
    ident_t &operator=( ident_t ) = delete;

    bool lives_in_reg() const { return in_reg.valid(); }
    bool is_live() const { return in_reg.valid() || in_stash.valid(); }
    bool is_reserved() const { return is_reserved_identifier( name ); }
};

}
