#pragma once

#include <vector>
#include "context.hpp"
#include "token.hpp"

namespace tiny::cc
{

struct parser_config
{
    bool has_calls = true, /* calls from main and to assert are always allowed */
         has_pointers = true,
         has_structs = true,
         has_arrays = true;

    bool is_internal = false; /* allows __reserved_identifiers */
                              /* note that functions with reserved names are
                               * handled specially in codegen w.r.t. ubsan */
};

void parse( context &, const document &, const std::vector< token > &, parser_config );

}
