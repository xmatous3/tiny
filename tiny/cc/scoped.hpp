#pragma once

#include <vector>
#include <map>
#include <string>

namespace tiny::cc
{

/* non-owning */
template< typename Value >
struct scoped_view
{
    // stack of scopes and kv-pairs declared within each
    std::vector< std::map< std::string, Value * > > s2kv;
    // keys and their values in each scope they are (re)declared in
    std::map< std::string_view,
              std::vector< std::pair< unsigned, Value * > > > k2sv;

    const auto & current_scope() const { return s2kv.back(); }

    struct add_result
    {
        Value *here = nullptr;
        Value *previous = nullptr;

        bool is_shadowing() const { return previous; }
        bool is_redeclaration() const { return here == previous; }

        bool has_previous_site() const
        {
            assert( previous );
            return previous->declaration_site.valid();
        }
        const auto& previous_site() const
        {
            assert( has_previous_site() );
            return previous->declaration_site;
        }
    };

    add_result add( std::string_view name, Value *v )
    {
        assert( !s2kv.empty() );
        auto [ it_s2kv, new_in_scope ] = s2kv.back().try_emplace( std::string( name ), v );
        auto *ident = it_s2kv->second;

        auto [ it_k2sv, new_in_fn ] = k2sv.try_emplace( ident->name );
        auto &scopes = it_k2sv->second;

        add_result r;

        if ( !new_in_fn )
            r.previous = scopes.back().second;

        if ( new_in_scope )
            scopes.emplace_back( s2kv.size() - 1, ident );

        r.here = scopes.back().second;

        assert( ident == r.here );
        assert( new_in_scope == !r.is_redeclaration() );
        assert( new_in_fn == !r.is_shadowing() );

        return r;
    }

    Value* find( std::string_view name ) const
    {
        auto in_scopes = k2sv.find( name );
        if ( in_scopes == k2sv.end() )
            return nullptr;

        assert( !in_scopes->second.empty() );
        return in_scopes->second.back().second;
    }

    struct scope_guard
    {
        scoped_view &parent;
        unsigned guardee_index;
        scope_guard( scoped_view &parent, unsigned ix )
            : parent( parent ), guardee_index( ix ) {}
        scope_guard( const scope_guard & ) = delete;
        ~scope_guard()
        {
            while ( guardee_index < parent.s2kv.size() )
                parent.close_scope();
        }
    };
    friend struct scope_guard;

    [[nodiscard]]
    scope_guard open_scope()
    {
        s2kv.emplace_back();
        return scope_guard{ *this, unsigned( s2kv.size() ) - 1 };
    }

private:
    void close_scope()
    {
        assert( !s2kv.empty() );
        auto &scope = s2kv.back();

        for ( auto &[ name, ident ] : scope )
        {
            auto in_scopes = k2sv.find( name );

            assert( in_scopes != k2sv.end() );
            assert( !in_scopes->second.empty() );
            assert( in_scopes->second.back().first == s2kv.size() - 1 );

            in_scopes->second.pop_back();
            if ( in_scopes->second.empty() )
                k2sv.erase( in_scopes );
        }
        s2kv.pop_back();
    }
};

}
