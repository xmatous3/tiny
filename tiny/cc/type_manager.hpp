#pragma once

#include <set>

#include "type.hpp"

namespace tiny::cc
{

class type_manager
{
    std::set< type, std::less<> > types;

public:
    struct error
    {
        std::string wh;
        const char * what() const { return wh.c_str(); }
    };

    type_manager()
    {
        type::builtin_type uint_t{ "unsigned int" };
        uint_t._size = 2;
        add( uint_t );

        type::builtin_type int_t{ "int" };
        int_t._size = 2;
        int_t.is_signed = true;
        add( int_t );

        type::builtin_type char_t{ "char" };
        char_t._size = 1;
        char_t.is_signed = true;
        add( char_t );

        type::builtin_type schar_t{ "signed char" };
        schar_t._size = 1;
        schar_t.is_signed = true;
        add( schar_t );

        type::builtin_type uchar_t{ "unsigned char" };
        uchar_t._size = 1;
        add( uchar_t );

        // Bytes with values of 1 or 0
        type::builtin_type bool_t{ "bool" };
        bool_t._size = 1;
        bool_t.is_bool = true;
        add( bool_t );

        // Ints that undergo narrowing to 0/1 when their number value is requested
        type::builtin_type booleoid_t{ "bool-like" };
        booleoid_t._size = 2;
        booleoid_t.is_bool = true;
        add( booleoid_t );

        type::builtin_type void_t{ "void" };
        void_t._size = 1;
        void_t.is_void = true;
        add( void_t );
    }

    const type* get_signed() const { return get( "int" ); }
    const type* get_unsigned() const { return get( "unsigned int" ); }
    const type* get_int( bool sgn = true ) const
    {
        return sgn ? get_signed() : get_unsigned();
    }
    const type* get_void() const { return get( "void" ); }
    const type* get_bool() const { return get( "bool" ); }
    const type* get_booleoid() const { return get( "bool-like" ); }
    const type* get_voidptr() { return add_pointer( get_void() ); }
    const type* get_cvoidptr() { return add_pointer( add_const( get_void() ) ); }

    const type* get( const std::string_view tname ) const
    {
        auto t = find( tname );
        assert( t );
        return t;
    }
    const type* find( const std::string_view tname ) const
    {
        auto it = types.find( tname );
        if ( it == types.end() )
            return nullptr;
        return &*it;
    }
    const type* find_struct( const std::string_view tname ) const
    {
        std::string n = "struct ";
        n += tname;
        return find( n );
    }

    const type* promoted( const type *t )
    {
        return t->is_signed() ? get_int() : get_unsigned();
    }
    const type* common_arithmetic_type( const type *t1, const type *t2 )
    {
        if ( t1->is_pointer() && t2->is_arithmetic() )
            return t1;
        if ( t2->is_pointer() && t1->is_arithmetic() )
            return t2;
        if ( t1->is_pointer() && t2->is_pointer() )
            return t1->remove_pointer()->is_const() || t2->remove_pointer()->is_const()
                ? get_cvoidptr() : get_voidptr();

        return t1->is_u16() || t2->is_u16() ? get_unsigned()
                                            : get_int();
    }
    const type* common_boollike_type( const type *t1, const type *t2 )
    {
        if ( t1->remove_const()->is_bool() && t2->remove_const()->is_bool() )
            return get_bool();
        return get_booleoid();
    }


    template< typename Rep >
    const type* add( Rep &&rep )
    {
        auto [it, is_new] = types.emplace( std::forward< Rep >( rep ) );
        return &*it;
    }

    const type* add_const( const type *t )
    {
        assert( t );

        if ( t->is_function() )
            throw error( "no const functions" );

        if ( auto *at = t->get_if< type::array_type >() )
            return array_of( add_const( &at->of ), at->length );

        if ( t->is_const() )
            return t;

        return add( type::const_type{ *t } );
    }
    const type* add_pointer( const type *t )
    {
        assert( t );

        if ( t->is_array() )
            throw error( "no pointers to arrays" );
        if ( t->is_function() )
            throw error( "no pointers to functions" );

        return add( type::pointer_type{ *t } );
    }
    const type* array_of( const type *t, int len )
    {
        assert( t );

        if ( t->is_array() )
            throw error( "no multidimensional arrays" );

        return add( type::array_type{ *t, len } );
    }
    const type* function( const type *rt, const std::vector< const type* > &args )
    {
        assert( rt );
        if ( rt->is_array() )
            throw error( "no returning arrays" );
        if ( rt->is_function() )
            throw error( "no returning functions" );

        for ( auto *arg : args )
        {
            assert( arg );

            if ( arg->is_array() )
                throw error( "no array arguments" );
            if ( arg->is_function() )
                throw error( "no function arguments" );
        }

        return add( type::function_type{ *rt, args } );
    }
    type::struct_type& add_struct( const std::string &name )
    {
        auto *t = add( type::struct_type{ name } );
        auto &s = std::get< type::struct_type >( t->_rep );
        // note that the actual key (type name) is const within the structure
        // and thus cannot be changed even through a non-const reference
        // TODO: check whether that's UB
        return const_cast< type::struct_type& >( s );
    }

    auto begin() const { return types.begin(); }
    auto end() const { return types.end(); }
};

}
