#include <iostream>

#include "context.hpp"

namespace tiny::cc
{

context::context()
    : _toplevel( idents.open_scope() )
{
    // Declare the built-in 'assert' function
    auto *t = types.function( types.get_void(), std::vector{ types.get_booleoid() } );
    auto &ident = globals.emplace_back( "assert", t );
    idents.add( ident.name, &ident );
}

void context::dump_types() const
{
    std::cerr << "Types in existence (--dump-types):\n";
    for ( const auto &t : types )
    {
        std::cerr << "  - " << t.name() << "; ";

        if ( t.is_complete() )
            std::cerr << t.size();
        else
            std::cerr << "incomplete";

        std::cerr << "\n";
    }
}

void context::dump_ast() const
{
    for ( const auto &[ name, f ] : funcs )
        f.dump_ast();
}

void function::dump_ast() const
{
    brq::string_builder sb;
    sb << "AST for function " << ident->name << " : " << ident->type->name()
       << ":\n" << body << "\n";
    std::cerr << sb.data();
}

}
