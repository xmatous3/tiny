#pragma once

namespace tiny
{

/* A nullable index with type tag to prevent mixing indices to different things */

template< typename Tag, typename T >
struct tagged_index
{
    T raw = 0;

    static constexpr tagged_index none() { return {}; }
    static constexpr tagged_index from_zero_based( T i ) { return { T( i + 1 ) }; }
    static constexpr tagged_index from_one_based( T ix ) { return { T( ix ) }; }

    constexpr T zero_based() const { return raw - 1; }
    constexpr T one_based() const { return raw; }

    constexpr bool valid() const { return raw != 0; }
    constexpr explicit operator bool() const { return valid(); }

    constexpr void clear() { raw = 0; }

    constexpr bool operator==( const tagged_index & ) const = default;
};

}
