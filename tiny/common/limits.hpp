#pragma once

namespace tiny
{

inline bool within_i16( int64_t v )
{
    return v >= -( 1 << 15 ) && v < ( 1 << 15 );
}

inline bool within_u16( int64_t v )
{
    return v >= 0 && v < ( 1 << 16 );
}

}
