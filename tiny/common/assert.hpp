#pragma once

/* assert is ASSERT from brick-assert */

#include <brick-assert>

#ifdef assert
#error "Will not override already defined 'assert' macro. Stray <cassert> somewhere?"
#endif

#define assert ASSERT
