#pragma once

#include <brick-cmd>
#include <brick-assert>

namespace tiny
{

/* An option expecting either an existing and readable regular file, or "-" */
struct cmd_file : brq::cmd_file
{
    static brq::parse_result from_string( std::string_view s, cmd_file &f )
    {
        if ( s == "-" )
        {
            f.name = s;
            return {};
        }
        return brq::cmd_file::from_string( s, f );
    }
};

template< typename Command >
inline int parse_and_run_single( int argc, char** argv )
{
    brq::singleton< brq::assert_config >().quiet = true;
    brq::cmd_option_parser p( argc, argv, "" );
    std::unique_ptr< brq::cmd_base > cmd;

    auto move = [&cmd]( auto &&c )
    {
        using type = std::remove_cvref_t< decltype( c ) >;
        cmd.reset( new type( std::move( c ) ) );
    };

    try
    {
        p.opt_parse< Command >().match( move );
        cmd->run();
    }
    catch ( std::exception &e )
    {
        std::cerr << "\033[1;31mexception:\033[m " << e.what() << "\n";
        return 1;
    }

    return 0;
}

}
