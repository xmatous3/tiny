#pragma once

#include <vector>
#include <memory>
#include <brick-fs>
#include <iostream>

#include "document.hpp"

namespace tiny
{

template< typename Document = tiny::document >
struct document_store
{
    using document = Document;
    std::vector< std::unique_ptr< document > > docs;

    document& load_from_stdin()
    {
        auto text = brq::read_file( std::cin );
        return add_document( document::stdin_name, text );
    }

    document& load_from_file( std::string_view path )
    {
        if ( path == "-" )
           return load_from_stdin();

        auto text = brq::read_file( path );
        return add_document( path, text );
    }

    document& add_document( std::string_view name, std::string_view text )
    {
        return *docs.emplace_back( std::make_unique< document >( name, text ) );
    }
};

}
