#pragma once

#include "document.hpp"

namespace tiny
{

struct diagnostic
{
    struct config_t
    {
        bool colourful = true;
    };

    enum kind_t
    {
        context,
        note,
        warning,
        error,
    };

    virtual ~diagnostic() = default;
    virtual brq::string_builder &print( brq::string_builder &, kind_t, config_t ) const = 0;
};

struct source_diagnostic : diagnostic
{
    document::span where; // optional, also may only point to a line or a file
    brq::string_builder sb;

    source_diagnostic( document::span where ) : where( where ) {}

    template< typename... Ts >
    source_diagnostic( document::span where, Ts&&... args )
        : where( where )
    {
        ( ( sb << args ),  ... );
    }

    source_diagnostic( const source_diagnostic &o ) : where( o.where )
    {
        sb << o.sb;
    }
    source_diagnostic( source_diagnostic && ) = default;

    brq::string_builder &print( brq::string_builder &, kind_t, config_t ) const override;

    template< typename T >
    brq::string_builder &operator<<( T &&arg ) { return sb << std::forward< T >( arg ); }
};

struct noted_diagnostic : source_diagnostic
{
    std::vector< source_diagnostic > notes;

    using source_diagnostic::source_diagnostic;

    brq::string_builder &print( brq::string_builder &, kind_t, config_t ) const override;

    template< typename... Ts >
    noted_diagnostic &append_note( Ts &&... args )
    {
        notes.emplace_back( std::forward< Ts >( args )... );
        return *this;
    }
};

brq::string_builder &format_location( brq::string_builder &, document::span, diagnostic::config_t );
brq::string_builder &format_highlight( brq::string_builder &, document::span, int colour, diagnostic::config_t );

void print_diagnostic( diagnostic::kind_t, const diagnostic & );
inline void print_error( const diagnostic &d ) { print_diagnostic( diagnostic::error, d ); }
inline void print_warning( const diagnostic &d ) { print_diagnostic( diagnostic::warning, d ); }

}
