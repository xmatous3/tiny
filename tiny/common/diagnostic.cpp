#include <iostream>
#include <brick-string>

#include "diagnostic.hpp"

namespace tiny
{

line_index::line_index( std::string_view sv )
{
    long last = -1;

    do
    {
        index.push_back( last );
        last = sv.find( '\n', last + 1 );
    }
    while ( last != sv.npos );

    // final line without \n
    if ( index.empty() || index.back() + 1 != sv.length() )
        index.push_back( sv.length() );
}

namespace
{
std::pair< std::string_view, int >  get_diagnostic_style( diagnostic::kind_t kind )
{
    switch ( kind )
    {
        case diagnostic::context: return { "", 4 };
        case diagnostic::note:    return { "note", 6 };
        case diagnostic::warning: return { "warning", 5 };
        case diagnostic::error:   return { "error", 1 };
    }
    UNREACHABLE( "invalid diagnostic kind" );
}
}

brq::string_builder &source_diagnostic::print(
        brq::string_builder &out, kind_t kind, config_t conf ) const
{
    auto [ label, colour ] = get_diagnostic_style( kind );

    format_location( out, where, conf ) << ": ";

    if ( !label.empty() )
    {
        if ( conf.colourful )
            out << "\033[1;" << ( 30 + colour ) << "m";

        out << label << ": ";

        if ( conf.colourful )
            out << "\033[m";
    }

    out << sb << "\n";

    return format_highlight( out, where, colour, conf );
}

brq::string_builder &noted_diagnostic::print(
        brq::string_builder &out, kind_t kind, config_t conf ) const
{
    source_diagnostic::print( out, kind, conf );

    for ( auto &n : notes )
        n.print( out, diagnostic::note, conf );

    return out;
}

void print_diagnostic( diagnostic::kind_t kind, const diagnostic &d )
{
    brq::string_builder out;
    diagnostic::config_t conf;
#ifndef _WIN32
    conf.colourful = ::isatty( STDERR_FILENO );
#endif
    d.print( out, kind, conf );
    std::cerr << out.data();
}

brq::string_builder &format_location( brq::string_builder &out,
        document::span span, diagnostic::config_t conf )
{
    if ( !span )
        return out << "<no location info>";

    const document &doc = span.doc();

    std::string_view endc = "";
    if ( conf.colourful )
    {
        out << "\033[1m";
        endc = "\033[m";
    }

    out << doc.filename;
    if ( span.only_file() )
        return out << endc;

    auto [ i_from, i_to ] = doc.view_to_indices( span.sv );
    auto l_from = doc.translate( i_from );

    out << ":" << l_from.line;

    if ( span.zero_width() )
        return out << endc;

    return out << ":" << 1 + doc.pcolumn( l_from.byte ) << endc;
}

brq::string_builder &format_highlight( brq::string_builder &out,
        document::span span, int colour, diagnostic::config_t conf )
{
    if ( span.zero_width() )
        return out;

    if ( !conf.colourful )
        colour = 0;

    const document &doc = span.doc();

    auto [ i_from, i_to ] = doc.view_to_indices( span.sv );
    location from = doc.translate( i_from ),
             to   = doc.translate( i_to );

    for ( auto l = from.line; l <= to.line; ++l )
    {
        auto l_str = doc.line( l );

        long c_from = l > from.line ? 1 : from.column;
        while ( c_from < l_str.length() && std::isspace( l_str[ c_from - 1 ] ) )
            ++c_from;

        long c_to = l < to.line ? l_str.length() + 1 : to.column;
        while ( c_to > c_from && c_to > 2 && std::isspace( l_str[ c_to - 2 ] ) )
            --c_to;

        long p_from = doc.pcolumn( l_str.substr( 0, c_from ) ),
             p_to   = 1 + doc.pcolumn( l_str.substr( 0, c_to - 1 ) );

        out << brq::pad( 6 ) << l << brq::mark << " | ";

        out << l_str.substr( 0, c_from - 1 );
        if ( colour ) out << "\033[1;" << ( 30 + colour ) << "m";
        out << l_str.substr( c_from - 1, c_to - c_from );
        if ( colour ) out << "\033[m";
        out << l_str.substr( c_to - 1 );

        out << "\n" << brq::pad( 6 ) << "" << brq::mark << " | "
            << brq::pad( p_from - 1 ) << "" << brq::mark;
        if ( colour ) out << "\033[1;" << ( 30 + colour ) << "m";
        out << brq::mark << ( l == from.line ? "^" : "" ) << brq::pad( p_to - p_from, '~' );
        if ( colour ) out << "\033[m";
        out << "\n";
    }

    return out;
}


}
