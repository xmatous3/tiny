#pragma once

#include <array>
#include <memory>

namespace tiny::vm
{

struct memory
{
    static constexpr uint32_t size = 1 << 16;
    using data_t = std::array< uint8_t, size >;

    std::unique_ptr< data_t > _data;

    memory() : _data( new data_t )
    {
        _data->fill( 0 );
    }

    uint8_t get( uint16_t a ) const
    {
        return _data->data()[ a ];
    }

    void set( uint16_t a, uint8_t v )
    {
        _data->data()[ a ] = v;
    }
};

// TODO
struct striped_memory
{
    static constexpr const int size = 1 << 16;
    static constexpr const int stripe_count = 128;
    static constexpr const int stripe_size = size / stripe_count;
    static_assert( stripe_count * stripe_size == size );
    using stripe_t = std::array< uint8_t, stripe_size >;

    std::array< std::unique_ptr< stripe_t >, stripe_count > stripes{};

    constexpr static std::pair< int, int > addr( uint16_t a )
    {
        return { a / stripe_size, a % stripe_size };
    }
    uint8_t get( uint16_t a ) const
    {
        auto [stripe, off] = addr( a );
        if ( stripes[stripe] )
            return (*stripes[stripe])[off];
        return 0;
    }
    void set( uint16_t a, uint8_t v )
    {
        auto [stripe, off] = addr( a );
        if ( stripes[stripe] )
            stripes[stripe] = std::make_unique< stripe_t >();
        (*stripes[stripe])[off] = v;
    }
};

}
