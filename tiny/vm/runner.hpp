#pragma once

#include <brick-min>
#include <brick-cmd>

#include "state.hpp"
#include "debug.hpp"
#include "instr_visitor.hpp"
#include "as/assembly.hpp"

namespace tiny::vm
{

struct runner_config
{
    int max_steps = 1 << 16;
    brq::cmd_flag trace_steps,
                  trace_regs,
                  trace_triggers = true;

    void options( brq::cmd_options &c )
    {
        c.opt( "--[no-]trace-triggers", trace_triggers )
            << "print out trigger-based traces";
        c.opt( "--trace-steps", trace_steps )
            << "print out every performed instruction";
        c.opt( "--trace-registers", trace_regs )
            << "also trace full content of registers (with diff)";
        c.opt( "--steps-limit", max_steps )
            << "maximal number of operations before giving up (default 2¹⁶)";
    }
};

struct runner : state
{
    runner_config config;
    as::triggers_t triggers;
    as::symtab_t syms;
    dbg_state dbg{ *this };
    int steps = 0;

    bool trace_comma = false;
    brq::string_builder _trace;
    std::bitset< 17 > regs_accessed;
    std::bitset< 17 > regs_modified;

    runner() = default;
    explicit runner( runner_config conf ) : config( std::move( conf ) ) {}
    explicit runner( const as::program &prog, runner_config conf = {} )
        : config( std::move( conf ) )
    {
        load( prog );

        if ( config.trace_regs )
            config.trace_steps = true;
    }

    runner( const runner & ) = delete;
    runner( runner && ) = delete;

    void load( const as::program &prog )
    {
        int i = 0;

        for ( int j = 0; j < 65536; ++j )
            mem.set( j, 0 );

        for ( auto b : prog.bytes )
            mem.set( i++, b );

        triggers = prog.triggers;
        syms = prog.syms;
    }

    uint32_t read_rep() { return read32( getpc() ); }

    bool step()
    {
        uint32_t rep = read_rep();
        uint16_t oldpc = getpc();

        as::instruction instr;
        try
        {
            instr = as::instruction::from_rep( rep );
        }
        catch ( std::exception &e )
        {
            stop( state::decoding_failed );
            return false;
        }

        std::string_view startc = "",
                         endc   = "";
        if ( config.trace_regs && regs_modified.test( 16 ) )
        {
            startc = "\033[7m";
            endc = "\033[m";
        }
        trace( startc, oldpc, endc, " | ", instr, " |" );

        setpc( oldpc + 4 );

        using visit = instr_visitor< runner >;

        try
        {
            visit()( instr, *this );
        }
        catch ( std::exception &e )
        {
            stop( state::decoding_failed );
            return false;
        }

        dbg.ub_clear();

        if ( auto it = triggers.find( oldpc ); it != triggers.end() )
            for ( const auto &t : it->second )
                dbg.trigger( t, instr );

        ++steps;

        if ( config.trace_steps || dbg.ub_detected() )
        {
            if ( config.trace_regs )
                gen_reg_trace();

            std::cout << _trace.data() << std::endl;
            _trace.clear();
            trace_comma = false;
            regs_accessed.reset();
            regs_modified.reset();

            if ( getpc() != oldpc + 4 )
                regs_modified.set( 16 );
        }

        if ( is_running() && dbg.ub_detected() )
            stop( state::ub_detected );

        return is_running() && steps < config.max_steps;
    }

    void run()
    {
        dbg.do_print = config.trace_triggers;
        print_trace_header();

        while ( step() )
            ;

        if ( hs == state::halted && dbg.vals.contains( "_tc_expect_" ) )
            if ( dbg.vals[ "_tc_" ] != dbg.vals[ "_tc_expect_" ] )
                hs = state::tc_mismatch;
    }

    brq::string_builder& print_final( brq::string_builder &out )
    {
        auto stepback = [ this ]() { setpc( getpc() - 4 ); };

        out << "after " << std::dec << steps << " steps, the machine ";
        switch ( hs )
        {
            case state::running:
                out << "is still running";
                break;
            case state::halted:
                stepback();
                out << "halted normally";
                break;
            case state::decoding_failed:
                out << "failed to decode instruction\n  representation: 0x"
                    << std::hex << brq::pad( 8, '0' ) << read_rep() << brq::mark
                    << std::dec;
                break;
            case state::assert_failed:
                stepback();
                out << "failed an assert";
                // TODO: file and line;
                break;
            case state::division_by_zero:
            case state::ub_detected:
                stepback();
                out << "detected undefined behaviour: "
                    << ( hs == state::division_by_zero ? "division by zero"
                                                       : dbg.ub_kind );
                break;
            case state::tc_mismatch:
                out << "halted unexpectedly after " << dbg.vals[ "_tc_" ] << " testcases, out of "
                    << dbg.vals[ "_tc_expect_" ];
                break;
            default:
                UNREACHABLE( "invalid halting state" );
        }

        out << "\n  pc: " << std::hex << brq::pad( 4, '0' ) << getpc() << brq::mark << "\n";
        return out;
    }


    void trace( auto &&... args )
    {
        if ( !config.trace_steps )
            return;

        if ( !_trace.data().empty() )
        {
            _trace << ( trace_comma ? ", " : " " );
            trace_comma = true;
        }

        ( ..., trace_one( std::forward< decltype( args ) >( args ) ) );
    }
    void trace_one( as::reg r )
    {
        _trace << r.to_string();
    }
    void trace_one( uint16_t n )
    {
        _trace << std::hex << brq::pad( 4, '0' ) << n << brq::mark;
    }
    void trace_one( std::string_view sv )
    {
        _trace << sv;
    }
    void trace_one( const as::instruction &instr )
    {
        instr.print_fixed( _trace );
    }

    void print_trace_header()
    {
        if ( !config.trace_steps )
            return;

        std::cout << "  pc | inst op_1 op_2  out |";

        if ( !config.trace_regs )
        {
            std::cout << " notes and effects\n";
            return;
        }

        for ( int i = 0; i < 16; ++i )
        {
            std::cout << "   " << as::reg( i ).to_string();
        }
        std::cout << "\n";
    }

    void gen_reg_trace()
    {
        for ( int i = 0; i < 16; ++i )
        {
            std::string_view endc = "\033[m";
            _trace << ' ';
            if ( regs_modified.test( i ) )
                _trace << "\033[7m";
            if ( regs_accessed.test( i ) )
                _trace << "\033[4m";

            _trace << std::hex << brq::pad( 4 ) << state::getreg( as::reg( i ) )
                   << brq::mark << endc;
        }
    }

    /* Tracing of micro-operations */
    uint16_t getreg( as::reg r )
    {
        regs_accessed.set( r.raw );

        auto v = state::getreg( r );

        if ( !config.trace_regs )
            trace( r, " = ", v );

        return v;
    }
    void setreg( as::reg r, uint16_t v )
    {
        regs_modified.set( r.raw );

        if ( !config.trace_regs )
            trace( r, " ← ", v );

        state::setreg( r, v );
    }
    void stop( state::halt_status h )
    {
        if ( !config.trace_regs )
            trace( "stop" );

        state::stop( h );
    }

    uint16_t sym( std::string_view a ) const
    {
        if ( auto it = syms.find( a ); it != syms.end() )
            return it->second.address;
        else
            brq::raise() << "symbol " << a << " not found";

        __builtin_trap();
    }

    using state::read8;
    using state::read16;
    using state::write8;
    using state::write16;

    uint16_t read16( std::string_view a, int off = 0 ) const { return read16( sym( a ) + off ); }
    uint16_t read8( std::string_view a, int off = 0 ) const { return read8( sym( a ) + off ); }
    void write16( std::string_view a, int off, uint16_t val ) { write16( sym( a ) + off, val ); }
    void write8( std::string_view a, int off, uint16_t val ) { write8( sym( a ) + off, val ); }
};

}
