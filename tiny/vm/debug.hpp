#pragma once
#include <iostream>
#include <bitset>
#include <brick-string>
#include <set>

#include "as/trigger.hpp"
#include "as/instruction.hpp"
#include "state.hpp"

namespace tiny::vm
{
    struct dbg_state
    {
        std::map< std::string, uint16_t > vals;
        std::map< char, int > regnum;
        const vm::state &s;
        bool do_print = true;

        std::string_view ub_kind;

        using memmask_t = std::bitset< memory::size >;
        std::unique_ptr< memmask_t > _rmask,
                                     _wxmask;

        std::set< uint16_t > allocations;

        static constexpr uint16_t data_start = 0x8000;
        static constexpr uint16_t stack_start = 0xc000;

        dbg_state( const vm::state &s )
            : s( s ), _rmask( new memmask_t ), _wxmask( new memmask_t )
        {
            for ( int i = 0; i <= 9; ++i )
                regnum[ '0' + i ] = i;
            for ( int i = 0; i < 6; ++i )
                regnum[ 'a' + i ] = 10 + i;
        }

        void trigger( const as::trigger &t, const as::instruction &instr )
        {
            switch ( t.t )
            {
                case as::trigger::trace:
                    return trace( t );
                case as::trigger::inc:
                    vals[ t.arg ] ++;
                    break;
                case as::trigger::dec:
                    vals[ t.arg ] --;
                    break;
                case as::trigger::set:
                    vals[ t.arg ] = t.val;
                    break;
                case as::trigger::ubsan:
                    check_ub( t.arg, t.val, instr );
                    break;
                case as::trigger::mark:
                    mark( t.memmark );
                    break;
            }
        }

        void check_ub( std::string_view type, uint16_t val, const as::instruction &instr )
        {
            auto fault = [&]( auto set_type, auto when )
            {
                if ( when )
                    ub_kind = set_type;
                return when;
            };

            auto fault_if = [&]( auto if_type, auto when )
            {
                if ( type != if_type )
                    return false;
                fault( if_type, when );
                return true;
            };

            if ( type == "mem" )
            {
                if ( fault( "nullptr-deref", s.last_accessed_address == 0 ) )
                    return;

                if ( instr.is_control_flow() )
                {
                    for ( int i = 0; i < 4; ++i )
                        if ( fault( "invalid-jump", !is_code( s.getpc() + i ) ) )
                            return;
                }
                else if ( instr.is_load() )
                {
                    for ( int i = 0; i <= !instr.is_narrow(); ++i )
                        if ( fault( "invalid-load", !is_data( s.last_accessed_address + i ) ) )
                            return;
                }
                else if ( instr.is_store() )
                {
                    for ( int i = 0; i <= !instr.is_narrow(); ++i )
                        if ( fault( "invalid-store", !is_writable( s.last_accessed_address + i ) ) )
                            return;
                }

                return;
            }

            if ( type == "rvo" || type == "memcpy" )
            {
                auto dest = s.getreg( type == "rvo" ? as::reg::rv() : as::reg::t2() );
                auto size = type == "rvo" ? val : s.getreg( as::reg::t3() );
                for ( int i = 0; i < size; ++i )
                    if ( fault( "invalid-memcpy", !is_writable( dest + i ) ) )
                        return;
                return;
            }

            if ( type == "malloc" )
            {
                auto addr = s.getreg( as::reg::rv() );
                fault( "BUG-malloc", addr && !allocations.insert( addr ).second );
                return;
            }

            if ( type == "free" )
            {
                auto addr = s.getreg( as::reg::l1() );
                fault( "invalid-free", addr && !allocations.erase( addr ) );
                return;
            }

            fault_if( "overflow", s.overflowed ) ||
                fault_if( "no-return", true ) ||
                fault_if( "stack-overflow", s.getreg( as::reg::sp() ) < stack_start ) ||
                fault_if( "shl-neg", s.shl_negative ) ||
                fault_if( "overshift", s.overshifted );
        }

        bool ub_detected() const { return !ub_kind.empty(); }
        void ub_clear() { ub_kind = ""; }

        void trace( const as::trigger &t )
        {
            if ( !do_print )
                return;

            brq::string_builder out;
            bool escape = false;

            for ( int i = 0; i < vals[ "_indent_" ]; ++i )
                out << "  ";

            for ( char c : t.arg )
                if ( c == '$' )
                    escape = true;
                else if ( escape )
                {
                    out << std::hex << s.getreg( as::reg( regnum[ c ] ) );
                    escape = false;
                }
                else
                    out << c;

            INFO( out.data() );
        }

        bool is_data( uint16_t a ) const { return _rmask->test( a ); }
        bool is_code( uint16_t a ) const { return _wxmask->test( a ) && !is_data( a ); }
        bool is_writable( uint16_t a ) const { return is_data( a ) && _wxmask->test( a ); }

        void mark_ro( uint16_t a, uint16_t sz ) { mark( a, sz, true, false ); }
        void mark_rw( uint16_t a, uint16_t sz ) { mark( a, sz, true, true ); }
        void mark_code( uint16_t a, uint16_t sz ) { mark( a, sz, false, true ); }
        void unmark( uint16_t a, uint16_t sz ) { mark( a, sz, false, false ); }

        void mark( as::memory_mark mm )
        {
            using as::memory_mark;

            for ( auto p : { &memory_mark::begin, &memory_mark::end } )
            {
                auto &part = mm.*p;
                if ( part.reg )
                    part.offset += s.getreg( part.reg );
            }

            mark( mm.begin.offset, mm.end.offset - mm.begin.offset,
                    mm.kind & memory_mark::r, mm.kind & memory_mark::wx );
        }

        void mark( uint16_t a, uint16_t sz, bool r, bool wx )
        {
            while ( sz --> 0 )
            {
                _rmask->set( a, r );
                _wxmask->set( a, wx );
                ++ a;
            }
        }
    };
}
