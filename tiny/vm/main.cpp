#include <iostream>
#include <fstream>
#include <brick-cmd>

#include "runner.hpp"
#include "common/cli.hpp"
#include "as/parser.hpp"

namespace tiny::vm
{

int load_and_run( const char *, const runner_config & );

struct command : brq::cmd_base
{
    cmd_file _in_file;

    runner_config runconf;

    void options( brq::cmd_options &c ) override
    {
        brq::cmd_base::options( c );
        c.pos( _in_file );
        runconf.options( c );
    }

    std::string_view help() override
    {
        return "‹tiny vm› is a virtual machine executing the result of the ‹tiny› assembler";
    }

    void run() override
    {
        exit( load_and_run( _in_file.name.c_str(), runconf ) );
    }

};

std::string cmd_name( command & ) { return ""; }

int load_and_run( const char *filename, const runner_config &runconf )
{
    runner r( runconf );
    std::ifstream ifs( filename );
    ifs >> std::hex;

    uint32_t opcode;
    int i = 0;

    while ( ifs )
    {
        std::istream::sentry _s( ifs );

        if ( ifs.peek() == ';' )
            break;

        ifs >> opcode;
        r.write32( i, opcode );
        i += 4;
    }

    std::string delim;
    std::getline( ifs, delim );

    while ( true )
    {
        uint16_t pc;
        int t;
        std::string arg;
        ifs >> pc >> t;
        if ( !ifs )
            break;
        ifs.get();
        std::getline( ifs, arg );
        if ( t == as::trigger::mark )
        {
            as::line_parser p{ arg };
            auto mm = p.memory_mark();
            if ( !p.ok )
                throw std::runtime_error( "invalid memory mark format" );
            r.triggers[ pc ].emplace_back( mm );
        }
        else
            r.triggers[ pc ].emplace_back( as::trigger::type( t ), arg );
    }

    r.run();

    brq::string_builder sb;
    std::cout << r.print_final( sb ).data();

    return r.hs != state::halted;
}

}

int main( int argc, char **argv )
{
    return tiny::parse_and_run_single< tiny::vm::command >( argc, argv );
}
