#pragma once

#include <array>

#include "as/instr_arg.hpp"
#include "memory.hpp"

namespace tiny::vm
{

struct state
{
    memory mem;
    std::array< uint16_t, 16 > regs = {};
    uint16_t pc = 0;

    uint16_t last_accessed_address = 0;
    bool overshifted = false;
    bool overflowed = false;
    bool shl_negative = false;

    enum halt_status
    {
        running,
        halted,
        assert_failed,
        decoding_failed,
        division_by_zero,
        ub_detected,
        tc_mismatch,
    } hs = running;

    void setreg( as::reg r, uint16_t v ) { regs[ r.raw ] = v; }
    uint16_t getreg( as::reg r ) const { return regs[ r.raw ]; }
    void setpc( uint16_t v ) { pc = v; }
    uint16_t getpc() const { return pc; }

    bool is_running() const { return hs == running; }
    void stop( halt_status h ) { assert( is_running() ); hs = h; }

    void write8( uint16_t a, uint8_t v ) { mem.set( a, v ); }
    uint8_t read8( uint16_t a ) const { return mem.get( a ); }
    void write16( uint16_t a, uint16_t v ) { write8( a + 1, v ); write8( a, v >> 8 ); }
    uint16_t read16( uint16_t a ) const { return read8( a + 1 ) | ( read8( a ) << 8 ); }
    void write32( uint16_t a, uint32_t v ) { write16( a + 2, v ); write16( a, v >> 16 ); }
    uint32_t read32( uint16_t a ) const { return read16( a + 2 ) | ( read16( a ) << 16 ); }
};

auto &operator<<( brq::string_builder &b, state::halt_status s )
{
    switch ( s )
    {
        case state::running: return b << "running";
        case state::halted: return b << "halted";
        case state::assert_failed: return b << "assert_failed";
        case state::decoding_failed: return b << "decoding_failed";
        case state::division_by_zero: return b << "division_by_zero";
        case state::ub_detected: return b << "ub_detected";
        case state::tc_mismatch: return b << "tc_mismatch";
        default: return b << "???";
    }
}

}
