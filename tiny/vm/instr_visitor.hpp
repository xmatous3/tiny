#pragma once
#include "common/limits.hpp"
#include "as/instructions.hpp"

namespace tiny::vm
{

template< typename VM >
struct divide
{
    VM &m;

    divide( VM &m ) : m( m ) {}

    template< typename T > T operator()( T r, T l ) const
    {
        if ( l == 0 )
        {
            m.stop( state::division_by_zero );
            return 0xdead;
        }
        return r / l;
    }
};

template< typename VM >
struct modulus
{
    VM &m;

    modulus( VM &m ) : m( m ) {}

    template< typename T > T operator()( T r, T l ) const
    {
        if ( l == 0 )
        {
            m.stop( state::division_by_zero );
            return 0xdead;
        }
        return r % l;
    }
};

template< typename VM >
struct instr_visitor
{
    static int64_t maybe_signed( std::true_type, auto op, int16_t l, int16_t r )
    {
        return op( int32_t( l ), int32_t( r ) );
    }
    static int64_t maybe_signed( std::false_type, auto op, uint16_t l, uint16_t r )
    {
        return op( uint32_t( l ), uint32_t( r ) );
    }

    template< bool Signed >
    static void binarith( VM &m, auto op, as::reg dst, uint16_t l, uint16_t r )
    {
        int64_t result = maybe_signed( std::bool_constant< Signed >(), op, l, r );
        m.overflowed = !within_i16( result );
        m.setreg( dst, result );
    }

    template< bool Signed = false >
    static void rrr( auto &i, VM &m, auto op )
    {
        auto l = m.getreg( i.reg2 );
        auto r = m.getreg( i.reg3 );
        binarith< Signed >( m, op, i.reg1, l, r );
    }
    template< bool Signed = false >
    static void rri( auto &i, VM &m, auto op )
    {
        auto l = m.getreg( i.reg2 );
        auto r = i.immediate.immediate();
        binarith< Signed >( m, op, i.reg1, l, r );
    }
    template< bool Signed = false >
    static void rir( auto &i, VM &m, auto op )
    {
        auto l = i.immediate.immediate();
        auto r = m.getreg( i.reg2 );
        binarith< Signed >( m, op, i.reg1, l, r );
    }

    static void push( VM &m, uint16_t v )
    {
        auto newsp = m.getreg( as::reg::sp() ) - 2;
        m.setreg( as::reg::sp(), newsp );
        m.write16( newsp, v );
    }
    static uint16_t pop( VM &m )
    {
        auto sp = m.getreg( as::reg::sp() );
        m.setreg( as::reg::sp(), sp + 2 );
        return m.read16( sp );
    }

    void operator()( const as::instruction &i, VM &m )
    {

        auto shift_left = [&]< typename T >( T l, unsigned r ) -> T
        {
            m.shl_negative = l < 0;
            if (( m.overshifted = r >= 16 ))
                return 0;
            return l << r;
        };
        auto shift_right = [&]< typename T >( T l, unsigned r ) -> T
        {
            if (( m.overshifted = r >= 16 ))
                return ( l < 0 ? -1 : 0 );
            return l >> r;
        };

        namespace opcode = as::opcode;

        switch ( i.opcode )
        {
            case opcode::add_rr: { rrr< true >( i, m, std::plus() ); } break;
            case opcode::add_ir:
            case opcode::add_ri: { rri< true >( i, m, std::plus() ); } break;
            case opcode::sub_rr: { rrr< true >( i, m, std::minus() ); } break;
            case opcode::sub_ri: { rri< true >( i, m, std::minus() ); } break;
            case opcode::sub_ir: { rir< true >( i, m, std::minus() ); } break;
            case opcode::mul_rr: { rrr< true >( i, m, std::multiplies() ); } break;
            case opcode::mul_ir:
            case opcode::mul_ri: { rri< true >( i, m, std::multiplies() ); } break;
            case opcode::udiv_rr: { rrr( i, m, divide( m ) ); } break;
            case opcode::udiv_ri: { rri( i, m, divide( m ) ); } break;
            case opcode::udiv_ir: { rir( i, m, divide( m ) ); } break;
            case opcode::sdiv_rr: { rrr< true >( i, m, divide( m ) ); } break;
            case opcode::sdiv_ri: { rri< true >( i, m, divide( m ) ); } break;
            case opcode::sdiv_ir: { rir< true >( i, m, divide( m ) ); } break;
            case opcode::umod_rr: { rrr( i, m, modulus( m ) ); } break;
            case opcode::umod_ri: { rri( i, m, modulus( m ) ); } break;
            case opcode::umod_ir: { rir( i, m, modulus( m ) ); } break;
            case opcode::smod_rr: { rrr< true >( i, m, modulus( m ) ); } break;
            case opcode::smod_ri: { rri< true >( i, m, modulus( m ) ); } break;
            case opcode::smod_ir: { rir< true >( i, m, modulus( m ) ); } break;
            case opcode::and_rr: { rrr( i, m, std::bit_and() ); } break;
            case opcode::and_ri:
            case opcode::and_ir: { rri( i, m, std::bit_and() ); } break;
            case opcode::or_rr: { rrr( i, m, std::bit_or() ); } break;
            case opcode::or_ir:
            case opcode::or_ri: { rri( i, m, std::bit_or() ); } break;
            case opcode::xor_rr: { rrr( i, m, std::bit_xor() ); } break;
            case opcode::xor_ir:
            case opcode::xor_ri: { rri( i, m, std::bit_xor() ); } break;
            case opcode::shl_rr: { rrr< true >( i, m, shift_left ); } break;
            case opcode::shl_ri: { rri< true >( i, m, shift_left ); } break;
            case opcode::shl_ir: { rir< true >( i, m, shift_left ); } break;
            case opcode::shr_rr: { rrr< false >( i, m, shift_right ); } break;
            case opcode::shr_ri: { rri< false >( i, m, shift_right ); } break;
            case opcode::shr_ir: { rir< false >( i, m, shift_right ); } break;
            case opcode::sar_rr: { rrr< true >( i, m, shift_right ); } break;
            case opcode::sar_ri: { rri< true >( i, m, shift_right ); } break;
            case opcode::sar_ir: { rir< true >( i, m, shift_right ); } break;

            case opcode::eq_rr: { rrr( i, m, std::equal_to() ); } break;
            case opcode::eq_ri: { rri( i, m, std::equal_to() ); } break;
            case opcode::ne_rr: { rrr( i, m, std::not_equal_to() ); } break;
            case opcode::ne_ri: { rri( i, m, std::not_equal_to() ); } break;

            case opcode::ult_rr: { rrr( i, m, std::less() ); } break;
            case opcode::ult_ri: { rri( i, m, std::less() ); } break;
            case opcode::uge_rr: { rrr( i, m, std::greater_equal() ); } break;
            case opcode::uge_ri: { rri( i, m, std::greater_equal ()); } break;
            case opcode::ugt_rr: { rrr( i, m, std::greater() ); } break;
            case opcode::ugt_ri: { rri( i, m, std::greater ()); } break;
            case opcode::ule_rr: { rrr( i, m, std::less_equal() ); } break;
            case opcode::ule_ri: { rri( i, m, std::less_equal() ); } break;
            case opcode::slt_rr: { rrr< true >( i, m, std::less() ); } break;
            case opcode::slt_ri: { rri< true >( i, m, std::less() ); } break;
            case opcode::sge_rr: { rrr< true >( i, m, std::greater_equal() ); } break;
            case opcode::sge_ri: { rri< true >( i, m, std::greater_equal() ); } break;
            case opcode::sgt_rr: { rrr< true >( i, m, std::greater() ); } break;
            case opcode::sgt_ri: { rri< true >( i, m, std::greater() ); } break;
            case opcode::sle_rr: { rrr< true >( i, m, std::less_equal() ); } break;
            case opcode::sle_ri: { rri< true >( i, m, std::less_equal() ); } break;
            case opcode::sext: { m.setreg( i.reg1, int16_t( int8_t( m.getreg( i.reg2 ) ) ) ); } break;
            case opcode::copy: { m.setreg( i.reg1, m.getreg( i.reg2 ) ); } break;
            case opcode::put: { m.setreg( i.reg1, i.immediate.immediate() ); } break;
            case opcode::jmp: { m.setpc( i.address.address() ); } break;
            case opcode::jmpi: { m.setpc( m.getreg( i.reg1 ) ); } break;
            case opcode::jz: { if ( m.getreg( i.reg2 ) == 0 ) m.setpc( i.address.address() ); } break;
            case opcode::jzi: { if ( m.getreg( i.reg1 ) == 0 ) m.setpc( m.getreg( i.reg2 ) ); } break;
            case opcode::jnz: { if ( m.getreg( i.reg2 ) != 0 ) m.setpc( i.address.address() ); } break;
            case opcode::jnzi: { if ( m.getreg( i.reg1 ) != 0 ) m.setpc( m.getreg( i.reg2 ) ); } break;
            case opcode::call: { push( m, m.getpc() ); m.setpc( i.address.address() ); } break;
            case opcode::calli: { push( m, m.getpc() ); m.setpc( m.getreg( i.reg1 ) ); } break;
            case opcode::ret: { m.setpc( pop( m ) ); } break;
            case opcode::ld_r:
            case opcode::ldb_r:
            {
                auto addr = m.getreg( i.reg2 ) + i.address.address();
                m.last_accessed_address = addr;
                m.setreg( i.reg1, i.opcode == opcode::ldb_r ? m.read8( addr ) : m.read16( addr ) );
                break;
            }

            case opcode::st_r:
            case opcode::stb_r:
            {
                auto addr = m.getreg( i.reg1 ) + i.address.address();
                m.last_accessed_address = addr;
                auto val  = m.getreg( i.reg2 );
                if ( i.opcode == opcode::stb_r )
                    m.write8( addr, val );
                else
                    m.write16( addr, val );
                break;
            }

            case opcode::pop: { m.setreg( i.reg1, pop( m ) ); } break;
            case opcode::push_r: { push( m, m.getreg( i.reg2 ) ); } break;
            case opcode::asrt: { if ( m.getreg( i.reg2 ) == 0 ) m.stop( VM::assert_failed ); } break;
            case opcode::halt: { m.stop( VM::halted ); } break;
            default: assert( false );
        }
    }
};

}
