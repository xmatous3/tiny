#pragma once

#include <algorithm>
#include <map>
#include <vector>
#include <stack>
#include <optional>
#include <brick-string>

#include "common/document.hpp"
#include "common/diagnostic.hpp"
#include "instruction.hpp"
#include "trigger.hpp"

namespace tiny::as
{

struct symbol_info
{
    uint16_t address;
    document::span declaration_site;
};

using symtab_t = std::map< std::string, symbol_info, std::less<> >;

/* Representation of the assembler output/VM input */
struct program
{
    std::vector< uint8_t > bytes;
    triggers_t triggers;
    symtab_t syms;

    uint16_t pc() const { return bytes.size(); }

    void add_instr( const instruction &i );
    void add_trigger( auto... args )
    {
        triggers[ pc() == 0 ? 0 : pc() - 4 ].emplace_back( args... );
    }

    void write_hex( auto &stream ) const
    {
        int offset = 0;
        std::string_view delim = "";

        for ( auto b : bytes )
        {
            stream << delim << brq::hex_digit[ b / 16 ] << brq::hex_digit[ b % 16 ];
            ++ offset;
            delim = offset % 32 == 0 ? "\n" : offset % 4 == 0 ? " " : "";
        }

        stream << "\n";
    }

    void write_triggers( auto &stream ) const
    {
        for ( const auto &[ pc, ts ] : triggers )
            for ( const auto &t : ts )
            {
                stream << std::hex << std::setw( 4 ) << std::setfill( '0' )
                       << pc << " " << int( t.t ) << " " << t.arg;

                if ( t.t == trigger::set )
                    stream << " " << t.val;

                if ( t.t == trigger::mark )
                    stream << t.memmark;

                stream << "\n";
            }
    }
};

struct filter_t
{
    bool triggers    = false,
         locations   = false,
         allocations = false,
         src_lines   = false,
         comments    = false;
};

/* Representation of the compiler output/assembler input.
 *
 * The goal here is to have no other state than the stream of assembly items, so
 * that it is safe to freely add and delete instructions anywhere in the
 * existing code. */
struct assembly
{
    struct item
    {
        enum kind_t
        {
            instruction,
            label,
            trigger,
            register_allocation,
            stash_allocation,
            source_line,
            comment,
        } kind;

        std::string name;      // filename, label or identifier name
        as::instruction instr;
        as::trigger trig;

        document::span lit;

        int16_t placement = 0; // register index or offset from bp

        uint16_t size = 0;     // size of stash (any number) or the raw data (1 or 2)

        uint16_t length() const
        {
            if ( kind == instruction )
                return size;
            return 0;
        }

        bool matches( filter_t filter ) const
        {
            switch ( kind )
            {
                case trigger:
                    return filter.triggers;
                case register_allocation:
                case stash_allocation:
                    return filter.allocations;
                case source_line:
                    return filter.src_lines;
                case comment:
                    return filter.comments;
                case instruction:
                case label:
                    return true;
            }
            return false;
        }

        friend brq::string_builder &operator<<( brq::string_builder &, const assembly::item & );
    };

    std::vector< item > items;
    std::stack< int > checkpoints;

    void checkpoint()
    {
        checkpoints.push( items.size() );
    }

    void rollback()
    {
        items.resize( checkpoints.top() );
        checkpoints.pop();
    }

    auto &add( item &&i )
    {
        DEBUG( i );

        // remove newlines
        auto ix = 0;
        while ( ( ix = i.name.find( '\n', ix ) ) != i.name.npos )
            i.name[ ix ] = ' ';

        items.push_back( std::move( i ) );
        return items.back();
    }

    auto &add_instr( instruction i, document::span lit = {} )
    {
        return add({ .kind = item::instruction, .instr = std::move( i ),
                     .lit = lit, .size = uint16_t( i.size() ) });
    }
    auto &add_label( std::string_view l, document::span lit = {} )
    {
        return add({ .kind = item::label, .name = std::string{ l }, .lit = lit });
    }
    auto &add_trigger( auto... args )
    {
        return add({ .kind = item::trigger, .trig = { args... } });
    }
    auto &add_source_line( document::span lit )
    {
        return add({ .kind = item::source_line, .lit = lit });
    }
    auto &add_comment( auto... args )
    {
        return add({ .kind = item::comment, .name = brq::format( args... ).str() });
    }

    void add_line( document::span );
    void add_document( const document & );

    auto &add_word( uint16_t val )
    {
        instruction i{ 0, instruction::_i_ };
        i.immediate.raw = val;
        return add_instr( i );
    }

    symtab_t collect_symbols() const;
    program assemble() const;

    void rename_label( std::string_view from, std::string_view to )
    {
        for ( auto &i : items )
            if ( i.kind == item::label && i.name == from )
                i.name = to;
    }

    std::string print( filter_t to_print = {} ) const;
};

}
