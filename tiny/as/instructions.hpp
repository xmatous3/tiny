#pragma once

#include "instruction.hpp"

namespace tiny::as
{

#define _instr_( _opcode, _tag, ... ) \
    _tag = _opcode,

namespace opcode
{
enum
{
#include "instructions.def"
};
}

#undef _instr_

#define _filter_initialiser_order 1
#define _sgn_ instruction::_sgn_

namespace placeholders
{
    inline reg r1;
    inline reg r2;
    inline reg r3;
    inline imm immediate;
    inline addr address;
}

#define _instr_( _opcode, tag, name, _form, _flags ) \
    inline instruction tag( _form ## args ) { \
        using namespace placeholders; \
        return instruction{ \
                .opcode = opcode::tag, \
                .flags = instruction::_form | _flags, \
                .reg1 = r1, \
                .reg2 = r2, \
                .reg3 = r3, \
                .immediate = immediate, \
                .address = address \
        }; }

#include "instructions.def"

#undef _instr_
#undef _filter_initialiser_order
#undef _sgn_

inline bool instruction::is_jump() const
{
    return opcode == opcode::jmp
        || opcode == opcode::jz
        || opcode == opcode::jnz;
}

inline bool instruction::is_control_flow() const
{
    return is_jump()
        || opcode == opcode::call
        || opcode == opcode::calli
        || opcode == opcode::ret;
}

inline bool instruction::is_store( uint8_t opcode )
{
    return opcode == opcode::st_r
        || opcode == opcode::stb_r;
}

inline bool instruction::is_store() const
{
    return is_store( opcode );
}

inline bool instruction::is_load() const
{
    return opcode == opcode::ld_r
        || opcode == opcode::ldb_r;
}

inline bool instruction::is_narrow() const
{
    return opcode == opcode::stb_r
        || opcode == opcode::ldb_r;
}

}
