#pragma once

#include "common/tagged_index.hpp"
#include "common/assert.hpp"

#include <algorithm>
#include <string>
#include <cstdint>

namespace tiny::as
{

struct arg_parser;

struct reg
{
    static constexpr std::string_view aliases[ 16 ] =
    {
        "rv", "l1", "l2", "l3", "l4", "l5", "l6", "l7",
        "t1", "t2", "t3", "t4", "t5", "t6", "bp", "sp"
    };

    template< int8_t Raw >
    static constexpr reg mkreg()
    {
        static_assert( Raw >= 0 );
        static_assert( Raw < 16 );
        reg r;
        r.raw = Raw;
        return r;
    }

    static constexpr reg rv() { return mkreg< 0 >(); }
    static constexpr reg l1() { return mkreg< 1 >(); }
    static constexpr reg l2() { return mkreg< 2 >(); }
    static constexpr reg l3() { return mkreg< 3 >(); }
    static constexpr reg l4() { return mkreg< 4 >(); }
    static constexpr reg l5() { return mkreg< 5 >(); }
    static constexpr reg l6() { return mkreg< 6 >(); }
    static constexpr reg t1() { return mkreg< 8 >(); }
    static constexpr reg t2() { return mkreg< 9 >(); }
    static constexpr reg t3() { return mkreg< 10 >(); }
    static constexpr reg t4() { return mkreg< 11 >(); }
    static constexpr reg t5() { return mkreg< 12 >(); }
    static constexpr reg t6() { return mkreg< 13 >(); }
    static constexpr reg bp() { return mkreg< 14 >(); }
    static constexpr reg sp() { return mkreg< 15 >(); }

    int8_t raw = 0;

    constexpr reg() = default;
    explicit reg( int8_t r ) : raw( r ) { assert( r >= 0 ); assert( r < 16 ); }

    template< typename Tag >
    reg( tagged_index< Tag, int8_t > ix ) : raw( ix.zero_based() ) { assert( ix ); }

    constexpr auto ix() const { return tagged_index< reg, int8_t >::from_zero_based( raw ); }

    constexpr std::string_view to_string() const
    {
        return aliases[raw];
    }
    bool parse_with( arg_parser &p );

    static char to_hex( int i ) {
        if ( i < 10 )
            return '0' + i;
        return 'a' + i - 10;
    }
};

static_assert( reg::sp().to_string() == "sp" );
static_assert( reg::bp().to_string() == "bp" );
static_assert( reg::t1().to_string() == "t1" );
static_assert( reg::t2().to_string() == "t2" );
static_assert( reg::t3().to_string() == "t3" );
static_assert( reg::t4().to_string() == "t4" );

struct imm
{
    uint16_t raw = 0;

    imm() = default;
    imm( uint16_t r ) : raw( r ) {};

    std::string to_string() const { return std::to_string( raw ); }
    uint16_t immediate() const { return raw; }
    int16_t as_signed() const { return raw; }
    uint16_t as_unsigned() const { return raw; }
    bool parse_with( arg_parser &p );
};

struct addr
{
    std::string sym;
    int16_t off = 0;

    addr() = default;
    addr( int a ) : off( a ) {}
    addr( const char *s ) : sym( s ) {}
    addr( std::string_view s ) : sym( s ) {}
    addr( std::string s ) : sym( s ) {}
    addr( std::string_view s, int16_t o ) : sym( s ), off( o ) {}

    brq::string_builder &print( brq::string_builder &out, bool as_offset = false,
                                bool fixed = false ) const;
    friend brq::string_builder &operator<<( brq::string_builder &out, const addr &a )
    {
        return a.print( out );
    }

    uint16_t address() const { return off; }
    bool parse_with( arg_parser &p );
    bool resolved() const { return sym.empty(); }
    bool resolve( const auto &table )
    {
        if ( resolved() )
            return true;

        auto it = table.find( sym );
        if ( it == table.end() )
            return false;

        off += it->second.address;
        sym.clear();
        return true;
    }
    bool deresolve( const auto &table )
    {
        if ( !resolved() )
            return true;
        if ( table.empty() )
            return false;

        auto after = std::lower_bound( table.begin(), table.end(), off,
                []( auto &a, auto &b ) { return a.second.address < b; } );
        auto before = after;
        if ( before != table.begin() )
            --before;
        if ( after == table.end() )
            after = before;

        auto closest = after->second.address - off >= off - before->second.address
                     ? before : after;
        sym = closest->first;
        off -= closest->second.address;

        return true;
    }
};

}
