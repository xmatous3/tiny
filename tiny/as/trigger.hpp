#pragma once

#include <string_view>
#include <string>
#include <vector>
#include <map>
#include <brick-except>

#include "memory_mark.hpp"

namespace tiny::as
{

struct trigger
{
    enum type { trace, set, inc, dec, ubsan, mark } t;
    std::string arg;
    uint16_t val = 0;
    memory_mark memmark;

    trigger() = default;
    trigger( enum type t, std::string_view s, uint16_t v = 0 )
        : t( t ), arg( s ), val( v )
    {
        if ( t == set || t == ubsan )
        {
            auto [ n, v ] = brq::split( s, ' ' );
            arg = std::string( n );

            if ( auto res = brq::from_string( v, val ); t == set && !res )
                brq::raise() << "bad set trigger: " << s << "; " << res.error();
        }
    }

    trigger( memory_mark mm ) : t( mark ), memmark( mm ) {}
};

using triggers_t = std::map< uint16_t, std::vector< trigger > >;

}
