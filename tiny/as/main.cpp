#include <iostream>
#include <fstream>

#include "common/document_store.hpp"
#include "common/diagnostic.hpp"
#include "assembly.hpp"
#include "parser.hpp"

int main( int argc, const char **argv )
{
    --argc; ++argv; // shift $0

    using namespace tiny;
    using namespace std::literals;

    document_store docstore;
    as::assembly assm;

    const document *current_doc = nullptr;
    const char *out_name = "tiny.out";

    try
    {
        if ( argc == 0 )
            current_doc = &docstore.load_from_stdin();

        while ( current_doc || argc > 0 )
        {
            if ( argc > 1 && *argv == "-o"sv )
            {
                out_name = argv[ 1 ];
                argc -= 2;
                argv += 2;
            }

            if ( !current_doc )
                current_doc = &docstore.load_from_file( *argv++ ), --argc;

            assm.add_document( *current_doc );

            current_doc = nullptr;
        }

        auto prog = assm.assemble();
        std::ofstream out{ out_name, std::ios::binary };
        prog.write_hex( out );
        out << "; triggers follow\n";
        prog.write_triggers( out );
        std::cout << assm.print();
    }
    catch ( diagnostic &d )
    {
        print_diagnostic( diagnostic::error, d );
        return 1;
    }
    return 0;
}
