#pragma once

#include <brick-min>

namespace tiny::as
{

struct reg;

using reg_ix = tagged_index< reg, int8_t >;

struct memory_mark
{
    enum kind_t
    {
        r = 0x1,
        wx = 0x2,

        inaccessible = 0,
        readonly = r,
        readwrite = r | wx,
        executable = wx,
    } kind;

    struct addr_t
    {
        reg_ix reg = {};
        uint16_t offset = 0;

        friend auto &operator<<( auto &sb, addr_t a )
        {
            sb << std::hex;
            if ( a.reg )
                sb << '$' << +a.reg.zero_based();
            if ( a.offset )
            {
                if ( a.reg )
                    sb << ' ';
                sb << "0x" << a.offset;
            }
            return sb;
        }
    } begin, end;

    friend auto &operator<<( auto &sb, const memory_mark &mm )
    {
        switch ( mm.kind )
        {
            case readonly:     sb << "ro"; break;
            case readwrite:    sb << "rw"; break;
            case executable:   sb << "xo"; break;
            case inaccessible: sb << "no"; break;
        }
        return sb << ' ' << mm.begin << ' ' << mm.end;
    }
};

}
