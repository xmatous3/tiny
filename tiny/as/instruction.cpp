#include <stdexcept>
#include <ios>
#include <brick-string>

#include "common/diagnostic.hpp"
#include "instructions.hpp"
#include "parser.hpp"

namespace tiny::as
{
namespace
{

auto invalid_opcode( uint8_t opcode )
{
    return std::logic_error( "invalid opcode: " + std::to_string( opcode ) );
}

uint8_t opcode_to_flags( uint8_t opcode )
{
    switch ( opcode )
    {
#define _sgn_ instruction::_sgn_
#define _instr_( _opcode, _tag, _name, _form, _flags ) \
        case _opcode: return instruction::_form | _flags;

#include "instructions.def"

#undef _instr_
#undef _sgn_
    }
    throw invalid_opcode( opcode );
}

}


std::string_view instruction::name() const
{
    switch ( opcode )
    {
#define _instr_( _opcode, tag, name, ... ) \
    case _opcode: return #name;
#include "instructions.def"
#undef _instr_
    }
    throw invalid_opcode( opcode );
}

std::string_view instruction::tag() const
{
    switch ( opcode )
    {
#define _instr_( _opcode, tag, ... ) \
    case _opcode: return #tag;
#include "instructions.def"
#undef _instr_
    }
    throw invalid_opcode( opcode );
}

instruction instruction::from_rep( uint32_t rep )
{
    uint8_t opcode = rep >> 24;
    instruction i{ .opcode = opcode, .flags = opcode_to_flags( opcode ) };

    assert( i.has_reg3() + i.has_imm() + i.has_addr() <= 1 );

    if ( i.has_reg1() ) i.reg1 = reg( ( rep >> 20 ) & 0x0f );
    if ( i.has_reg2() ) i.reg2 = reg( ( rep >> 16 ) & 0x0f );
    if ( i.has_reg3() ) i.reg3 = reg( ( rep >> 12 ) & 0x0f );
    if ( i.has_imm() )  i.immediate = imm( rep & 0xffff );

    if ( i.has_addr() )
        i.address = addr( rep & 0xffff );

    return i;
}

uint32_t instruction::to_rep() const
{
    uint32_t rep = opcode << 24;

    if ( has_reg1() ) rep |= reg1.raw << 20;
    if ( has_reg2() ) rep |= reg2.raw << 16;
    if ( has_reg3() ) rep |= reg3.raw << 12;
    if ( has_imm() )  rep |= immediate.raw;

    if ( has_addr() )
    {
        assert( address.resolved() );
        rep |= uint16_t( address.off );
    }

    assert( has_reg3() + has_imm() + has_addr() <= 1 );

    return rep;
}

brq::string_builder &instruction::print( brq::string_builder &out ) const
{
    out << brq::mark << ( opcode ? name() : ".word" ) << brq::pad( 6 );
    int n = 0;
    auto append = [&]( const auto &arg, const char *sep = ", " )
    {
        if ( n++ )
            out << sep;
        if constexpr ( std::is_same_v< decltype( arg ), const addr & > )
            arg.print( out, ( flags & _rra_ ) == _rra_ );
        else
            out << std::dec << arg;
    };

    auto arrow = " → ";

    if ( has_reg2() && !is_flipped() ) append( reg2.to_string() );
    if ( has_reg3() ) append( reg3.to_string() );
    if ( !is_store() )
    {
        if ( has_addr() ) append( address );
        if ( has_imm() )
        {
            int val = is_signed() ? immediate.as_signed()
                                  : immediate.as_unsigned();
            append( std::to_string( val ) );
        }
    }
    if ( has_reg2() && is_flipped() ) append( reg2.to_string() );
    if ( has_reg1() ) append( reg1.to_string(), arrow );
    if ( is_store() )
    {
        if ( has_addr() ) append( address );
        if ( has_imm() ) append( std::to_string( immediate.as_signed() ) );
    }

    return out;
}

brq::string_builder &instruction::print_fixed( brq::string_builder &out ) const
{
    assert( opcode );

    out << brq::mark << name() << brq::pad( 4 ) << std::hex;

    auto append = [&]( auto arg, bool arrow = false )
    {
        out << brq::pad( 5 ) << ( arrow ? " → " : "" ) << arg << brq::mark;
    };

    if ( has_reg2() && !is_flipped() )
        append( reg2.to_string() );
    else if ( !has_reg2() )
        append( "" );

    if ( has_reg3() )
        append( reg3.to_string() );
    else if ( is_store() && has_reg1() )
        append( reg1.to_string(), true );
    else if ( has_addr() )
        address.print( out << " ", false, true );
    else if ( has_imm() )
        append( immediate.as_unsigned() );
    else
        append( "" );

    if ( has_reg2() && is_flipped() )
        append( reg2.to_string() );

    if ( is_store() && has_addr() )
        address.print( out << " ", false, true );
    else if ( has_reg1() )
        append( reg1.to_string(), true );
    else
        append( "" );

    return out;
}

instruction instruction::from_string( const std::string_view sv )
{
    arg_parser _p{ sv };
    auto name = _p.shift_word();
    _p.drop_blanks();

    instruction i;

    auto try_rrr_ = [&]
    {
        auto p = _p;
        return p( i.reg2 )
            && p( i.reg3 )
            && p( i.reg1, true )
            && p.end();
    };

    auto try_rri_ = [&]
    {
        auto p = _p;
        return p( i.reg2 )
            && p( i.immediate )
            && p( i.reg1, true )
            && p.end();
    };

    auto try_rir_ = [&]
    {
        auto p = _p;
        return p( i.immediate )
            && p( i.reg2 )
            && p( i.reg1, true )
            && p.end();
    };

    auto try_rra_ = [&]
    {
        auto p = _p;
        if ( instruction::is_store( i.opcode ) )
            return p( i.reg2 )
                && p( i.reg1, true )
                && p( i.address )
                && p.end();

        return p( i.reg2 )
            && p( i.address )
            && p( i.reg1, true )
            && p.end();
    };

    auto try_rrx_ = [&]
    {
        auto p = _p;
        return p( i.reg2 )
            && p( i.reg1, true )
            && p.end();
    };

    auto try_xrr_ = [&]
    {
        auto p = _p;
        return p( i.reg2 )
            && p( i.reg3 )
            && p.end();
    };

    auto try_rxi_ = [&]
    {
        auto p = _p;
        return p( i.immediate )
            && p( i.reg1, true )
            && p.end();
    };

    auto try_rxa_ = [&]
    {
        auto p = _p;
        return p( i.address )
            && p( i.reg1, true )
            && p.end();
    };

    auto try_xra_ = [&]
    {
        auto p = _p;
        return p( i.reg2 )
            && p( i.address, is_store( i.opcode ) )
            && p.end();
    };

    auto try_rxx_ = [&]
    {
        auto p = _p;
        return p( i.reg1, true )
            && p.end();
    };

    auto try_xrx_ = [&]
    {
        auto p = _p;
        return p( i.reg2 )
            && p.end();
    };

    auto try_xxa_ = [&]
    {
        auto p = _p;
        return p( i.address )
            && p.end();
    };

    auto try_xxx_ = [&]
    {
        auto p = _p;
        return p.end();
    };

    bool name_exists = false;

#define _instr_( _opcode, _tag, _name, _form, _flags ) \
    if ( name == #_name ) \
    { \
        name_exists = true; \
        i.opcode = _opcode; \
        i.flags = instruction::_form | _flags; \
        if ( try##_form() ) \
            return i; \
    }
#include "instructions.def"
#undef _instr_

    throw name_exists ? source_diagnostic( { .sv = _p.sv }, "instruction has wrong form" )
                      : source_diagnostic( { .sv = name }, "no such instruction" );
}
brq::string_builder &addr::print( brq::string_builder &out,
                                  bool as_offset, bool fixed ) const
{
    assert( sym.empty() || !fixed );

    out << sym;
    int abs_off = off;

    if ( !sym.empty() && off != 0 )
    {
        out << ( off < 0 ? " - " : " + " );
        abs_off = std::abs( off );
        as_offset = true;
    }
    if ( sym.empty() || off != 0 )
    {
        if ( fixed )
        {
            abs_off = uint16_t( abs_off );
            out << std::hex << brq::pad( 4, ' ' ) << abs_off << brq::mark;
        }
        else if ( as_offset )
        {
            out << std::dec << abs_off;
        }
        else
        {
            abs_off = uint16_t( abs_off );
            out << "0x" << std::hex << abs_off;
        }
    }
    return out;
}

bool reg::parse_with( arg_parser &p )
{
    auto w = p.shift_word();
    if ( w.length() == 2 && w[0] == 'r' && isxdigit( w[1] ) )
    {
        raw = from_hex( w[1] );
        return true;
    }
    for ( int i = 0; i < 16; ++i )
    {
        if ( w == aliases[i] ) {
            raw = i;
            return true;
        }
    }
    return false;
}

bool imm::parse_with( arg_parser &p )
{
    raw = p.shift_signed();
    return p.ok;
}

bool addr::parse_with( arg_parser &p )
{
    sym = p.shift_label();
    int sgn = 0;
    if ( !sym.empty() )
    {
        p.drop_blanks();
        if ( p.try_drop( "+" ) )
            sgn = 1;
    }

    p.drop_blanks();
    if ( !sgn && ! p.sv.starts_with( "->" ) && p.try_drop( "-" ) )
        sgn = -1;

    auto n = p.try_unsigned();

    if ( ( sym.empty() && n.has_value() ) ||
            ( !sym.empty() && ( !!sgn == n.has_value() ) ) )
    {
        off = ( sgn ? sgn : 1 ) * n.value_or( 0 );
        return true;
    }
    return false;
}

}
