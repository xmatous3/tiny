#include <iostream>
#include <stdexcept>
#include <brick-trace>

#include "common/assert.hpp"
#include "common/diagnostic.hpp"
#include "assembly.hpp"
#include "parser.hpp"

namespace tiny::as
{

void program::add_instr( const instruction &i )
{
    i.write( [ this ]( auto rep )
             {
                for ( auto byte : rep )
                    bytes.push_back( byte );
             } );
}

symtab_t assembly::collect_symbols() const
{
    symtab_t syms;
    uint16_t pc = 0;

    for ( auto &i : items )
    {
        if ( i.kind == item::label )
        {
            if ( auto prev = syms.find( i.name ); prev != syms.end() )
            {
                auto e = noted_diagnostic( i.lit,
                        "multiple definitions of symbol '", prev->first, "'" );
                if ( auto &site = prev->second.declaration_site )
                    e.append_note( site, "first defined here" );
                throw e;
            }

            syms.emplace( i.name, symbol_info{ pc, i.lit } );
        }
        pc += i.length();
    }

    return syms;
}

program assembly::assemble() const
{
    program prog;
    prog.syms = collect_symbols();

    for ( auto &i : items )
    {
        switch ( i.kind )
        {
            case item::instruction:
            {
                auto instr = i.instr; // copy

                if ( !instr.resolve( prog.syms ) )
                {
                    auto e = source_diagnostic( i.lit,
                            "undefined symbol '", instr.address.sym, "'" );
                    throw e;
                }

                prog.add_instr( std::move( instr ) );
                break;
            }
            case item::trigger:
                prog.add_trigger( i.trig );
                break;
        }
    }
    return prog;
}

void assembly::add_line( document::span span )
{
    auto line = span.sv;
    line_parser p{ line };

    p.drop_blanks();

    // Comment characters (; and #) may appear in the trigger
    if ( !p.sv.starts_with( ".trigger" ) )
        p.trim_comment();

    p.trim_blanks();

    span.sv = p.sv;

    if ( p.sv.empty() )
        return;

    // label
    if ( p.try_trim( ":" ) )
    {
        p.trim_blanks();
        auto label = p.shift_label();
        auto sym = std::string{ label };

        if ( !p.sv.empty() )
            throw source_diagnostic( span, "invalid label" );

        span.sv = label;

        add_label( label, span );
        return;
    }

    if ( p.try_drop( ".word" ) )
    {
        do
        {
            p.drop_blanks();
            instruction i{ 0, instruction::_i_ };
            i.immediate.raw = p.shift_signed();

            if ( !p.ok )
                throw source_diagnostic( span, "invalid .word" );

            add_instr( i, span );
        } while ( p.try_drop( "," ) );

        return;
    }

    if ( p.try_drop( ".trigger" ) )
    {
        p.drop_blanks();
        auto what = p.shift_word();
        p.drop_blanks();
        auto str  = p.sv;

        if ( p.ok && what == "inc" )
            add_trigger( trigger::inc, str );
        else if ( p.ok && what == "dec" )
            add_trigger( trigger::dec, str );
        else if ( p.ok && what == "set" )
            add_trigger( trigger::set, str );
        else if ( p.ok && what == "trace" )
            add_trigger( trigger::trace, str );
        else if ( p.ok && what == "ubsan" )
            add_trigger( trigger::ubsan, str );
        else if ( p.ok && what == "mark" )
        {
            if ( auto mm = p.memory_mark(); p.ok )
                add_trigger( mm );
            else
                throw source_diagnostic( span, "invalid memory mark" );
        }
        else
            throw source_diagnostic( span, "invalid .trigger" );

        return;
    }

    // instruction
    try
    {
        add_instr( instruction::from_string( p.sv ), span );
    }
    catch ( source_diagnostic &e )
    {
        // The instruction parser does not know about documents, but sets the
        // string_view part of document::span anyway, so we just add the doc
        e.where = span.doc().ref( e.where.sv );
        throw;
    }
}

void assembly::add_document( const document &doc )
{
    for ( int i = 1; i <= doc.line_count(); ++i )
        add_line( doc.ref( doc.line( i ) ) );
}

brq::string_builder &operator<<( brq::string_builder &out, const assembly::item &i )
{
    auto print_user = [&]
    {
        if ( i.name.empty() )
            out << " unused";
        else
            out << " ↔ " << i.name;
    };

    using item = assembly::item;

    switch ( i.kind )
    {
        case item::instruction:
        {
            out << "    ";
            brq::string_builder sb;
            i.instr.print( sb );
            out << sb.data();
            break;
        }
        case item::label:
            out << i.name << ":";
            break;
        case item::trigger:
            out << "    .trigger ";
            switch ( i.trig.t )
            {
                case trigger::trace: out << "trace "; break;
                case trigger::inc: out << "inc "; break;
                case trigger::dec: out << "dec "; break;
                case trigger::set: out << "set "; break;
                case trigger::ubsan: out << "ubsan "; break;
                case trigger::mark: out << "mark " << i.trig.memmark; break;
                default: UNREACHABLE( "invalid trigger type" );
            }
            out << i.trig.arg;

            if ( i.trig.val )
                out << ' ' << i.trig.val;

            break;
        case item::register_allocation:
            out << ";; "
                << reg( i.placement ).to_string();
            print_user();
            break;
        case item::stash_allocation:
            out << ";; bp[ "
                << i.placement << " ] + " << i.size;
            print_user();
            break;
        case item::source_line:
        case item::comment:
            out << "; " << i.name;
    }

    return out;
}

std::string assembly::print( filter_t to_print ) const
{
    const item *waiting_line = nullptr;
    std::string_view last_line;

    struct docinfo_t { size_t id; bool emitted = false; };
    using docmap_t = std::map< const document*, docinfo_t >;
    docmap_t docmap = { { nullptr, { .id = 0 } } };

    brq::string_builder sb;

    auto flush_waiting = [&]( bool standalone = true )
    {
        if ( !waiting_line || ( !to_print.locations && !to_print.src_lines ) )
            return;

        auto &lit = waiting_line->lit;

        location loc;
        std::string_view line;
        if ( lit )
        {
            auto [ i_from, i_to ] = lit.doc().view_to_indices( lit.sv );
            loc = lit.doc().translate( i_from );
            line = lit ? lit.doc().line( loc.line ) : "";
        }


        if ( lit.valid() == last_line.empty()
                || line.begin() != last_line.begin() )
        {
            auto &docinfo = docmap.emplace( lit._doc,
                    docinfo_t{ .id = docmap.size() } ).first->second;

            if ( to_print.locations )
            {
                if ( !docinfo.emitted )
                {
                    auto fname = lit ? std::string_view( lit.doc().filename )
                                     : "<nowhere>";
                    sb << "\n.file " << docinfo.id << ' ';
                    sb << brq::printable( fname ) << '\n';
                    docinfo.emitted = true;
                }

                if ( !standalone )
                    sb << '\n';

                sb << ".loc " << docinfo.id << ' ' << loc.line;
            }

            if ( standalone || to_print.locations )
                sb << "\t";

            if ( to_print.src_lines && !line.empty() )
                sb  << "; " << loc.line << "\t| " << line;

            if ( standalone || to_print.locations )
                sb << "\n";
        }

        last_line = line;
        waiting_line = nullptr;
    };

    for ( auto &i : items )
    {
        if ( i.kind == item::source_line )
        {
            flush_waiting();
            waiting_line = &i;
            continue;
        }

        if ( !i.matches( to_print ) )
            continue;

        if ( i.kind == item::register_allocation ||
                i.kind == item::stash_allocation ||
                i.kind == item::trigger )
        {
            flush_waiting();
            sb << "\t " << i << "\n";
        }
        else
        {
            if ( to_print.locations )
                flush_waiting();

            sb << i << "\t";

            if ( !to_print.locations )
                flush_waiting( /* standalone */ false );

            sb << "\n";
        }
    }

    // Convert to u32string before tabulating, because instructions contain a
    // unicode arrow and the tabulation algorithm understands it as two glyphs
    std::u32string str32;
    brq::from_string( sb.data(), str32 );
    str32 = brq::tabulate( str32 );
    sb.clear();
    sb << str32;
    return sb.str();
}

}
