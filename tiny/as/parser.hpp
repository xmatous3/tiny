#pragma once

#include "common/lexer_base.hpp"
#include "memory_mark.hpp"

namespace tiny::as
{

struct arg_parser : lexer_base
{
    bool first = true;

    using lexer_base::lexer_base;

    bool operator()( auto &e, bool arrow = false )
    {
        drop_blanks();

        if ( arrow )
            try_drop( "->" ) || try_drop( "→" ) || first || drop( "," );
        else if ( !first )
            drop( "," );

        drop_blanks();
        if (( ok = ok && e.parse_with( *this ) ))
        {
            drop_blanks();
            first = false;
        }
        return ok;
    }

    bool end() const { return sv.empty(); }
};

struct line_parser : lexer_base
{
    using lexer_base::lexer_base;

    void trim_comment()
    {
        auto ix = sv.find_first_of( ";#" );
        if ( ix != sv.npos )
            sv.remove_suffix( sv.length() - ix );
    }

    as::memory_mark memory_mark()
    {
        as::memory_mark res;

        if ( try_drop( "ro" ) )
            res.kind = memory_mark::readonly;
        else if ( try_drop( "rw" ) )
            res.kind = memory_mark::readwrite;
        else if ( try_drop( "xo" ) )
            res.kind = memory_mark::executable;
        else if ( try_drop( "no" ) )
            res.kind = memory_mark::inaccessible;
        else
            return ok = false, res;

        res.begin = memmark_address();
        res.end = memmark_address();

        return res;
    }

    as::memory_mark::addr_t memmark_address()
    {
        as::memory_mark::addr_t res;
        bool eaten = false;

        drop_blanks();
        if ( try_drop( "$" ) )
        {
            if ( !isxdigit( sv[ 0 ] ) )
                return ok = false, res;

            res.reg = reg_ix::from_zero_based( from_hex( sv[ 0 ] ) );
            sv.remove_prefix( 1 );
            drop_blanks();
            eaten = true;
        }

        if ( auto n = try_unsigned() )
        {
            res.offset = *n;
            eaten = true;
        }

        if ( !eaten )
            ok = false;

        return res;
    }
};

}
