#pragma once

#include <array>
#include <bitset>
#include <string_view>
#include <vector>
#include <map>

#include "instr_arg.hpp"

namespace tiny::as
{

struct instruction
{
    uint8_t opcode;
    uint8_t flags;
    reg reg1,
        reg2,
        reg3;
    imm immediate;
    addr address;

    std::string_view name() const;
    std::string_view tag() const;

    bool has_reg1() const { return flags & _r1_; }
    bool has_reg2() const { return flags & _r2_; }
    bool has_reg3() const { return flags & _r3_; }
    bool has_imm() const  { return flags & _i_; }
    bool has_addr() const { return flags & _a_; }

    bool is_signed() const { return flags & _sgn_; }
    bool is_flipped() const { return flags & _flip_; }
    bool is_jump() const;
    bool is_control_flow() const;
    static bool is_store( uint8_t );
    bool is_store() const;
    bool is_load() const;
    bool is_narrow() const;

    static instruction from_rep( uint32_t );
    static instruction from_string( const std::string_view );

    uint32_t to_rep() const;
    brq::string_builder &print( brq::string_builder &out ) const;
    brq::string_builder &print_fixed( brq::string_builder &out ) const;

    bool resolve( const auto &syms )
    {
        return !has_addr() || address.resolve( syms );
    }
    bool deresolve( const auto &syms )
    {
        return !has_addr() || address.deresolve( syms );
    }

    std::array< uint8_t, 4 > to_bytes() const
    {
        std::array< uint8_t, 4 > out;
        auto rep = to_rep();
        int i = 4;
        while ( i --> 0 )
        {
            out[i] = rep & 0xFF;
            rep >>= 8;
        }
        return out;
    }

    int size() const
    {
        return opcode == 0 ? 2 : 4; // XXX
    }

    auto write( auto do_write ) const
    {
        if ( opcode == 0 )
        {
            std::array< uint8_t, 2 > bytes;
            bytes[ 0 ] = immediate.raw >> 8 & 0xff;
            bytes[ 1 ] = immediate.raw      & 0xff;
            return do_write( bytes );
        }
        else
            return do_write( to_bytes() );
    }

    static instruction from_bytes( std::array< char, 4 > in )
    {
        uint32_t rep;
        for ( int i = 0; i < 4; ++i )
        {
            rep <<= 8;
            rep |= uint8_t( in[i] );
        }
        return from_rep( rep );
    }

    enum flags_t
    {
        _r1_  = 0b000001,
        _r2_  = 0b000010,
        _r3_  = 0b000100,
        _i_   = 0b001000,
        _a_   = 0b010000,

        _sgn_  = 0b0100000,
        _flip_ = 0b1000000,

        _xxx_ = 0,
        _rxx_ = _r1_,
        _xrx_ = _r2_,
        _xrr_ = _r2_ | _r3_,
        _rrx_ = _r1_ | _r2_,
        _rrr_ = _r1_ | _r2_ | _r3_,
        _xra_ = _r2_ | _a_,
        _rxa_ = _r1_ | _a_,
        _xxa_ = _a_,
        _rxi_ = _rxx_  | _i_,
        _rri_ = _rrx_ | _i_,
        _rir_ = _rrx_ | _i_ | _flip_,
        _rra_ = _rrx_ | _a_,
    };
};

}
