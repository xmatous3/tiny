int foo( int );

int main()
{
    foo( 1 );
    return 0;
}

int foo( int n ) { return n; }
