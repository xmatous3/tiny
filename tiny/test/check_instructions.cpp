#include "as/instructions.hpp"
#include <cstdio>
#include <cassert>

int main()
{
    auto test = []( unsigned opcode, const char *tag )
    {
        printf( "0x%02x %-10s ", opcode, tag );
        fflush( stdout );

        using namespace tiny::as;
        instruction instr = instruction::from_rep( opcode << 24 );
        instr.reg1 = reg( 0 );
        instr.reg2 = reg( 1 );
        instr.reg3 = reg( 2 );
        instr.immediate = -42;
        instr.address = 0xbeef;

        printf( "%08x ", instr.to_rep() );
        fflush( stdout );

        brq::string_builder sb, sb_fixed;
        instr.print( sb );
        instr.print_fixed( sb_fixed );

        printf( "| %s | %s\n", sb_fixed.str().c_str(), sb.str().c_str() );
        fflush( stdout );

        auto reinstr = instruction::from_string( sb.data() );
        assert( reinstr.to_rep() == instr.to_rep() );
    };

#define _instr_( _opcode, _tag, ... ) test( _opcode, #_tag );
#include "as/instructions.def"
#undef _instr_

    return 0;
}
