struct s
{
    const int x;
};

int main()
{
    struct s s;

    s.x = 4;

    return 0;
}
