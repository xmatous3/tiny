void foo() {}

int main()
{
    bool b = true;
    b ? foo() : 42;
    return 0;
}
