int foo( unsigned x );

int main()
{
    foo( 1 );
    return 0;
}

int foo( int x ) { return x; }
