int main()
{
    unsigned x = 21;
    x *= 2;
    assert( x == 42 );
    return 0;
}
