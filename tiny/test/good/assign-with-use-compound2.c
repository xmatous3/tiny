int main()
{
    unsigned x = 10;
    x *= x + 1;
    assert( x == 110 );
    return 0;
}
