int main()
{
    unsigned x = 10;
    x *= x;
    assert( x == 100 );
    return 0;
}
