int main()
{
    assert( sizeof( char ) == 1 );
    assert( ( sizeof( char ) + 1 ) == 2 );
    return 0;
}
