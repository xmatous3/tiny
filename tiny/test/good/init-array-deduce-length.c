int main()
{
    int a[] = { 1, 2, 3 };

    assert( a[ 0 ] == 1 );
    assert( a[ 1 ] == 2 );
    assert( a[ 2 ] == 3 );

    return 0;
}
